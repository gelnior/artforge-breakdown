package fr.hd3d.breakdown.ui.test.mock;

import java.util.List;

import fr.hd3d.breakdown.ui.client.view.IBreakdownView;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class BreakdownViewMock implements IBreakdownView
{

    private boolean idleShotView;
    private String cookieTabName;
    private boolean idleConstituentView;
    private List<Hd3dModelData> refreshTagEditor;
    private boolean displayOdsUploader;
    private boolean showOdsLongLoading;
    private boolean displayTaskTypeEditor;

    public void displayOdsUploader()
    {
        this.displayOdsUploader = true;
    }

    public void displayTaskTypeEditor()
    {
        this.displayTaskTypeEditor = true;
    }

    public void hideLoading()
    {
        // TODO Auto-generated method stub

    }

    public void idleConstituentView()
    {
        this.idleConstituentView = true;
    }

    public void idleShotView()
    {
        this.idleShotView = true;
    }

    public void init()
    {
        // TODO Auto-generated method stub

    }

    public void initWidgets()
    {
        // TODO Auto-generated method stub

    }

    public void rebuildLastEnvironment()
    {
        // TODO Auto-generated method stub

    }

    public void refreshTagEditor(List<Hd3dModelData> selection)
    {
        this.refreshTagEditor = selection;
    }

    public void saveTabToCookie(String tabName)
    {
        this.cookieTabName = tabName;
    }

    public void selectLastProject()
    {
        // TODO Auto-generated method stub

    }

    public void setControllers()
    {
        // TODO Auto-generated method stub

    }

    public void showAssetLongLoading()
    {
        // TODO Auto-generated method stub

    }

    public void showOdsImportLongLoading()
    {
        this.showOdsLongLoading = true;
    }

    public void showTaskLongLoading()
    {
        // TODO Auto-generated method stub

    }

    public void toggleAutoSave()
    {
        // TODO Auto-generated method stub

    }

    public void unidleConstituentView()
    {
        this.idleConstituentView = false;
    }

    public void unidleShotView()
    {
        this.idleShotView = false;
    }

    public void displayError(Integer error)
    {
        // TODO Auto-generated method stub

    }

    public void displayError(Integer error, String userMsg, String stack)
    {
        // TODO Auto-generated method stub

    }

    public void hideStartPanel()
    {
        // TODO Auto-generated method stub

    }

    public void showStartPanel()
    {
        // TODO Auto-generated method stub

    }

    /************************** To Test ***********************************/
    public boolean getIdleShotView()
    {
        return idleShotView;
    }

    public boolean getIdleConstituentView()
    {
        return idleConstituentView;
    }

    public String getCookieTabName()
    {
        return cookieTabName;
    }

    public List<Hd3dModelData> getRefreshTagEditor()
    {
        return refreshTagEditor;
    }

    public boolean getDisplayOdsUploader()
    {
        return displayOdsUploader;
    }

    public boolean getShowOdsLongLoading()
    {
        return showOdsLongLoading;
    }

    public boolean getDisplayTaskTypeEditor()
    {
        return displayTaskTypeEditor;
    }

    public void showScriptRunningPanel()
    {
        // TODO Auto-generated method stub

    }

    public void hideScriptRunningPanel()
    {
        // TODO Auto-generated method stub

    }

	public void idleSequenceView() {
		// TODO Auto-generated method stub
		
	}

	public void unidleSequenceView() {
		// TODO Auto-generated method stub
		
	}

}
