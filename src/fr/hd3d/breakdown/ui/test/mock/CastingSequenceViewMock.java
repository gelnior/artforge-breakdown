package fr.hd3d.breakdown.ui.test.mock;

import java.util.List;

import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel.Cell;

import fr.hd3d.breakdown.ui.client.model.CastingModelData;
import fr.hd3d.breakdown.ui.client.view.ICastingSequenceView;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


public class CastingSequenceViewMock implements ICastingSequenceView
{
    public ColumnModel getCastingColumnModel()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<CompositionModelData> getCompositionSelectedGrid()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public MaskableController getController()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<Cell> getCellsSelected()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void maskLoading()
    {
        // TODO Auto-generated method stub

    }

    public void reconfigureGrid()
    {
        // TODO Auto-generated method stub

    }

    // @Override
    // public void selectGrid(int rowIndex, int colIndex)
    // {
    // // TODO Auto-generated method stub
    //
    // }
    public void showMessageCopyError()
    {
        // TODO Auto-generated method stub

    }

    public void showMessageDeleteError()
    {
        // TODO Auto-generated method stub

    }

    public void unmaskLoading()
    {
        // TODO Auto-generated method stub

    }

    public void updateRow(int index)
    {
        // TODO Auto-generated method stub

    }

    public void deselectedRows()
    {
        // TODO Auto-generated method stub

    }

    public void infoDisplay(String title, String message)
    {
        // TODO Auto-generated method stub

    }

    public boolean isMultiSelectionGrid()
    {
        // TODO Auto-generated method stub
        return false;
    }

    public void changeProgress(Double value)
    {
        // TODO Auto-generated method stub

    }

    public void infoProgress(String title, String message)
    {
        // TODO Auto-generated method stub

    }

    public String getColumnIdSelected()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public List<CastingModelData> getCastingSelected()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void openCsvExport(String path)
    {
        // TODO Auto-generated method stub

    }

}
