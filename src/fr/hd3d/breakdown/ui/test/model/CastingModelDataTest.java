//package fr.hd3d.breakdown.ui.test.model;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.extjs.gxt.ui.client.core.FastMap;
//
//import fr.hd3d.breakdown.ui.client.model.CastingModelData;
//import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
//import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
//import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
//import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
//import fr.hd3d.common.ui.test.UITestCase;
//
//
//public class CastingModelDataTest extends UITestCase
//{
//    FastMap<CategoryModelData> categoryMap;
//    ArrayList<CompositionModelData> compositionList;
//    ShotModelData shot;
//
//    private CategoryModelData createCategory(Long id, Long parent)
//    {
//        CategoryModelData category = new CategoryModelData();
//        category.setId(id);
//        category.setName("category" + id);
//        category.setParent(parent);
//        category.setProject(1L);
//        return category;
//    }
//
//    // creation d'une arborescence :
//    // categoryRoot(-1)=> category1(1)
//    // * * * * * * * * * * * * * * * * - constituent(0)
//    // * * * * * * * * * * * * * * * * => category2(2)
//    // * * * * * * * * * * * * * * * * * * * * * * * - constituent(1)
//    // * * * * * * * * * * * * * * * * * * * * * * * - constituent(2)
//    //    
//    // * * * * * * * * => category3(3)
//    // * * * * * * * * * * * * * * * - constituent(3)
//    // * * * * * * * * * * * * * * * - constituent(4)
//    // * * * * * * * * * * * * * * * => category4(4)
//    // * * * * * * * * * * * * * * * * * * * * * - constituent(5)
//
//    @Override
//    public void setUp()
//    {
//        super.setUp();
//        shot = new ShotModelData();
//        shot.setId(1L);
//        shot.setDescription("test description");
//        shot.setName("shot1");
//        shot.setNbFrame(4);
//
//        categoryMap = new FastMap<CategoryModelData>();
//
//        CategoryModelData categoryRoot = createCategory(-1L, null);
//        CategoryModelData category1 = createCategory(1L, -1L);
//        CategoryModelData category2 = createCategory(2L, 1L);
//        CategoryModelData category3 = createCategory(3L, -1L);
//        CategoryModelData category4 = createCategory(4L, 3L);
//        categoryMap.put(categoryRoot.getId().toString(), categoryRoot);
//        categoryMap.put(category1.getId().toString(), category1);
//        categoryMap.put(category2.getId().toString(), category2);
//        categoryMap.put(category3.getId().toString(), category3);
//        categoryMap.put(category4.getId().toString(), category4);
//
//        compositionList = new ArrayList<CompositionModelData>();
//        for (Long i = 0L; i < 2L; i++)
//        {
//            ConstituentModelData constituent = new ConstituentModelData();
//            constituent.setId(i);
//            constituent.setName("constituent" + i);
//            constituent.setCategory(0L);
//        }
//
//        for (Long i = 2L; i < 4L; i++)
//        {
//            ConstituentModelData constituent = new ConstituentModelData();
//            constituent.setId(i);
//            constituent.setName("constituent" + i);
//            constituent.setCategory(1L);
//        }
//        for (Long i = 4L; i < 6L; i++)
//        {
//            ConstituentModelData constituent = new ConstituentModelData();
//            constituent.setId(i);
//            constituent.setName("constituent" + i);
//            constituent.setCategory(2L);
//        }
//
//        for (Long i = 0L; i < 6L; i++)
//        {
//            CompositionModelData composition = new CompositionModelData();
//            composition.setId(i);
//            composition.setName("composition" + i);
//            composition.setConstituent(i);
//            composition.setShot(shot.getId());
//            composition.setCategoryId(((i + 1) / 2) + 1);
//            compositionList.add(composition);
//        }
//
//    }
//
//    public void testConstructor()
//    {
//        try
//        {
//            new CastingModelData(categoryMap);
//        }
//        catch (Exception e)
//        {
//            fail();
//        }
//    }
//
//    public void testSetShot()
//    {
//        CastingModelData casting = new CastingModelData(categoryMap);
//        casting.setShot(shot);
//        assertEquals(shot.getId(), casting.getShotId());
//        assertEquals(shot.getDescription(), casting.getShotDescription());
//        assertEquals(shot.getName(), casting.getShotName());
//        assertEquals(shot.getNbFrame(), casting.getShotNbFrame());
//    }
//
//    public void testInitialize()
//    {
//        CastingModelData casting = new CastingModelData(categoryMap);
//
//        casting.setShot(shot);
//
//        casting.initialize(compositionList);
//
//        List<CompositionModelData> compositions = casting.getCompositions("1");
//        assertNotNull(compositions);
//        assertEquals("getCompositions() return compositions of under categories.", 3, compositions.size());
//
//        compositions = casting.getCompositions("2");
//        assertNull("the category id have to a category of first level.", compositions);
//
//        compositions = casting.getCompositions("3");
//        assertNotNull(compositions);
//        assertEquals("getCompositions() return compositions of under categories.", 3, compositions.size());
//
//        compositions = casting.getCompositions("4");
//        assertNull("the category id have to a category of first level.", compositions);
//
//    }
//}
