//package fr.hd3d.breakdown.ui.test.model;
//
//import java.util.ArrayList;
//
//import com.extjs.gxt.ui.client.event.EventType;
//
//import fr.hd3d.breakdown.ui.client.controller.CastingSequenceController;
//import fr.hd3d.breakdown.ui.client.model.CastingSequenceModel;
//import fr.hd3d.breakdown.ui.client.view.ICastingSequenceView;
//import fr.hd3d.breakdown.ui.test.mock.CastingSequenceViewMock;
//import fr.hd3d.common.ui.client.event.EventDispatcher;
//import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
//import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
//import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
//import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
//import fr.hd3d.common.ui.test.UITestCase;
//import fr.hd3d.common.ui.test.util.TestUtils;
//import fr.hd3d.common.ui.test.util.modeldata.reader.BaseModelReaderMock;
//
//
//public class CastingSequenceModelTest extends UITestCase
//{
//
//    CastingSequenceModel model;
//    ICastingSequenceView view;
//    CastingSequenceController controller;
//
//    @Override
//    public void setUp()
//    {
//        super.setUp();
//
//        model = new CastingSequenceModel();
//        view = new CastingSequenceViewMock();
//        controller = new CastingSequenceController(view, model);
//        Hd3dModelDataReaderSingleton.setInstanceAsMock(new BaseModelReaderMock());
//        EventDispatcher dispatcher = EventDispatcher.get();
//        dispatcher.getControllers().clear();
//        dispatcher.addController(controller);
//        dispatcher.addController(errorController);
//    }
//
//    public void testInitialize()
//    {
//        // preparing data
//        ProjectModelData project = TestUtils.getDefaultProject();
//        SequenceModelData sequence1 = new SequenceModelData();
//        sequence1.setName("seq");
//        sequence1.setProject(project.getId());
//        sequence1.save();
//        sequence1.refreshByName((EventType) null);
//
//        ShotModelData shot1 = new ShotModelData("shot1", sequence1);
//        shot1.save();
//        shot1.refreshByName((EventType) null);
//
//        ShotModelData shot2 = new ShotModelData("shot2", sequence1);
//        shot2.save();
//        shot2.refreshByName((EventType) null);
//
//        ArrayList<ShotModelData> shotList = new ArrayList<ShotModelData>();
//        shotList.add(shot1);
//        shotList.add(shot2);
//
//        // test
//        try
//        {
//            model.initialize(null);
//        }
//        catch (Exception e)
//        {
//            fail();
//        }
//
//        model.initialize(project);
//        assertEquals(project.getId(), model.getSelectedProject().getId());
//        // model.setSelectedSequences(sequence1);
//        model.loadShots();
//        // TODO test de chargement des données (bloqué par le allLoadListener)
//        // assertEquals(shotList.size(), model.getShotStore().getCount());
//    }
//}
