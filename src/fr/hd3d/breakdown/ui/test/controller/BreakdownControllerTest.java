//package fr.hd3d.breakdown.ui.test.controller;
//
//import org.junit.Test;
//
//import com.extjs.gxt.ui.client.mvc.AppEvent;
//
//import fr.hd3d.breakdown.ui.client.controller.BreakdownController;
//import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
//import fr.hd3d.breakdown.ui.client.model.BreakdownModel;
//import fr.hd3d.breakdown.ui.client.view.CastingView;
//import fr.hd3d.breakdown.ui.client.view.CastingSequenceView;
//import fr.hd3d.breakdown.ui.client.view.IConstituentView;
//import fr.hd3d.breakdown.ui.client.view.ShotView;
//import fr.hd3d.breakdown.ui.test.mock.BreakdownViewMock;
//import fr.hd3d.common.ui.client.event.CommonEvents;
//import fr.hd3d.common.ui.client.event.EventDispatcher;
//import fr.hd3d.common.ui.client.modeldata.RecordModelData;
//import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
//import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
//import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
//import fr.hd3d.common.ui.test.UITestCase;
//
//
//public class BreakdownControllerTest extends UITestCase
//{
//
//    BreakdownModel model = new BreakdownModel();
//    BreakdownViewMock view = new BreakdownViewMock();
//    BreakdownController controller = new BreakdownController(view, model);
//
//    @Override
//    public void setUp()
//    {
//        super.setUp();
//        model = new BreakdownModel();
//        view = new BreakdownViewMock();
//        controller = new BreakdownController(view, model);
//        EventDispatcher dispatcher = EventDispatcher.get();
//        dispatcher.getControllers().clear();
//        dispatcher.addController(controller);
//    }
//
//    @Test
//    public void testShowNewSheetWindow()
//    {}
//
//    public void testProjectChangedEvent()
//    {
//        EventDispatcher.forwardEvent(CommonEvents.PROJECT_CHANGED);
//        assertEquals(Boolean.valueOf(true), this.model.isProjectChanged());
//    }
//
//    public void testConstituentTabClicked()
//    {
//        EventDispatcher.forwardEvent(BreakdownEvents.CONSTITUENT_TAB_CLICKED);
//        assertEquals(IConstituentView.NAME, this.view.getCookieTabName());
//        assertTrue(this.view.getIdleShotView());
//        assertFalse(this.view.getIdleConstituentView());
//        assertFalse(this.model.isProjectChanged());
//        assertNull(this.view.getRefreshTagEditor());
//        assertEquals(ConstituentModelData.SIMPLE_CLASS_NAME, model.getCurrentEntity());
//    }
//
//    public void testShotTabClicked()
//    {
//        EventDispatcher.forwardEvent(BreakdownEvents.SHOT_TAB_CLICKED);
//        assertEquals(ShotView.NAME, this.view.getCookieTabName());
//        assertTrue(this.view.getIdleConstituentView());
//        assertFalse(this.view.getIdleShotView());
//        assertFalse(this.model.isProjectChanged());
//        assertNull(this.view.getRefreshTagEditor());
//        assertEquals(ShotModelData.SIMPLE_CLASS_NAME, model.getCurrentEntity());
//    }
//
//    public void testCastingTabClicked()
//    {
//        EventDispatcher.forwardEvent(BreakdownEvents.CASTING_TAB_CLICKED);
//        assertEquals(CastingView.NAME, this.view.getCookieTabName());
//    }
//
//    public void testCastingTabClicked2()
//    {
//        EventDispatcher.forwardEvent(BreakdownEvents.CASTING_SEQUENCE_TAB_CLICKED);
//        assertEquals(CastingSequenceView.NAME, this.view.getCookieTabName());
//    }
//
//    public void testSelectChangeEvent()
//    {
//        AppEvent event = new AppEvent(ExplorerEvents.SELECT_CHANGE);
//        RecordModelData selectedModel = new RecordModelData();
//        event.setData(ExplorerEvents.EVENT_VAR_MODELDATA, selectedModel);
//
//        this.model.setCurrentEntity(ShotModelData.SIMPLE_CLASS_NAME);
//        EventDispatcher.forwardEvent(event);
//
//        assertEquals(ShotModelData.SIMPLE_CLASS_NAME, selectedModel.getSimpleClassName());
//        assertEquals(ShotModelData.CLASS_NAME, selectedModel.getClassName());
//
//        event = new AppEvent(ExplorerEvents.SELECT_CHANGE);
//        selectedModel = new RecordModelData();
//        event.setData(ExplorerEvents.EVENT_VAR_MODELDATA, selectedModel);
//
//        this.model.setCurrentEntity(ConstituentModelData.SIMPLE_CLASS_NAME);
//        EventDispatcher.forwardEvent(event);
//
//        assertEquals(ConstituentModelData.SIMPLE_CLASS_NAME, selectedModel.getSimpleClassName());
//        assertEquals(ConstituentModelData.CLASS_NAME, selectedModel.getClassName());
//
//        event = new AppEvent(ExplorerEvents.SELECT_CHANGE);
//        event.setData(ExplorerEvents.EVENT_VAR_MODELDATA, null);
//        try
//        {
//            EventDispatcher.forwardEvent(event);
//        }
//        catch (Exception e)
//        {
//            fail();
//        }
//
//    }
//
//    public void testOdsUploaderEvent()
//    {
//        EventDispatcher.forwardEvent(BreakdownEvents.ODS_IMPORT_CLICKED);
//        assertTrue(this.view.getDisplayOdsUploader());
//    }
//
//    // non tester => lancement de script :(
//    public void testOdsFileSubmittedEvent()
//    {
//    // TODO
//    }
//
//    public void testTaskTypeEditorClickedEvent()
//    {
//        EventDispatcher.forwardEvent(CommonEvents.TASK_TYPE_BUTTON_CLICKED);
//        assertTrue(this.view.getDisplayTaskTypeEditor());
//    }
//
//}
