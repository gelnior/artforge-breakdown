package fr.hd3d.breakdown.ui.client.controller;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.model.CreationData;
import fr.hd3d.breakdown.ui.client.view.SequenceView;
import fr.hd3d.breakdown.ui.client.view.ShotView;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.model.SequenceModel;
import fr.hd3d.common.ui.client.model.ShotModel;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.workobject.browser.controller.WorkObjectController;


/**
 * Controller handling events raised by or for shots view.
 * 
 * @author HD3D
 */
public class SequenceController extends WorkObjectController
{
    /** Model handling constituent view data (explorer and sheet combo stores). */
    private final SequenceModel model;
    /** Constituent view containing tree, explorer and identity widget for constituents. */
    private final SequenceView view;

    /**
     * Default constructor.
     * 
     * @param view
     *            Sequence view containing tree, explorer and identity widget for constituents.
     * @param model
     *            Model handling sequence view data (explorer and sheet combo stores).
     */
    public SequenceController(SequenceModel model, SequenceView view)
    {
        super(view, model);

        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == CommonEvents.SEQUENCES_SELECTED)
        {
            this.onSequenceSelected(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
    }

    /**
     * When project changes, all data are cleared. Then first sheet of combo box is selected. Explorer filter is updated
     * to retrieve only sequences from this project. Project id is save to cookie for reselecting at next launching.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();
        EqConstraint constraint = new EqConstraint("project.id", MainModel.currentProject.getId());
        this.model.getFilter().setLeftMember(constraint);

        if (project != null)
        {
            this.view.setExplorerView(null);
            this.isFirstSheetLoading = true;
            this.view.reloadTree(project);            
            this.view.reloadSheets(project, ShotModelData.SIMPLE_CLASS_NAME.toLowerCase());
        }
    }

    /**
     * When a sequence is selected, explorer filter is updated. If the all sequence is selected (id = -1) the default
     * filter is set (filter on whole project). If another sequence is selected a lucene constraint is set. It will make
     * the explorer to retrieve only shots from the selected sequence and from its children.
     * 
     * @param event
     *            The sequence selected event.
     */
    private void onSequenceSelected(AppEvent event)
    {
        List<SequenceModelData> sequences = event.getData();
        if (CollectionUtils.isNotEmpty(sequences))
        {
            this.model.setConstraintSequence(sequences);
            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    @Override
    protected void onExplorerDataLoaded(AppEvent event) 
    {
    	this.view.enableTree();
	}
    
    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(CommonEvents.SHOTS_SELECTED);
        this.registerEventTypes(CommonEvents.SEQUENCES_SELECTED);

        this.registerEventTypes(CommonEvents.ERROR);
    }
}
