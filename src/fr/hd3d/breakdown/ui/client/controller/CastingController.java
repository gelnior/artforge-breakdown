package fr.hd3d.breakdown.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.enums.ECastingMode;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.model.CastingModel;
import fr.hd3d.breakdown.ui.client.view.CastingView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Controller handling casting events.
 * 
 * @author HD3D
 */
public class CastingController extends MaskableController
{
    /** View displaying casting widgets. */
    protected CastingView view;
    /** Model handling casting data. */
    protected CastingModel model;

    public CastingController(CastingView view, CastingModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (CommonEvents.PROJECT_CHANGED.equals(type))
        {
            this.onProjectChanged();
        }
        else if (BreakdownEvents.CONSTITUENT_MODE_CLICKED.equals(type))
        {
            this.onConstituentModeClicked(event);
        }
        else if (BreakdownEvents.SHOT_MODE_CLICKED.equals(type))
        {
            this.onShotModeClicked(event);
        }
        else if (BreakdownEvents.CASTING_DRAG_START.equals(type))
        {
            this.onDragStart(event);
        }
        else if (BreakdownEvents.CASTING_CONSTITUENT_DROPPED.equals(type))
        {
            this.onCastingConstituentDropped(event);
        }
        else if (BreakdownEvents.CASTING_SHOT_DROPPED.equals(type))
        {
            this.onCastingShotDropped(event);
        }
        else if (BreakdownEvents.CASTING_REMOVE_CLICKED.equals(type))
        {
            this.onRemoveClicked();
        }
        else if (BreakdownEvents.CASTING_REFRESH_CLICKED.equals(type))
        {
            this.onRefreshClicked();
        }
        else if (CommonEvents.SHOTS_SELECTED.equals(type))
        {
            this.onShotSelected(event);
        }
        else if (CommonEvents.CONSTITUENTS_SELECTED.equals(type))
        {
            this.onConstituentSelected(event);
        }
    }

    private void onRemoveClicked()
    {
        if (this.model.getCurrentMode() == ECastingMode.SHOT)
        {
            List<ConstituentModelData> constituents = this.view.getConstituentSelection();
            this.model.getConstituentStore().remove(constituents);
            this.model.deleteConstituentCompositions(constituents);
        }
        else
        {
            List<ShotModelData> shots = this.view.getShotSelection();
            this.model.getShotStore().remove(shots);
            this.model.deleteShotCompositions(shots);
        }
    }

    private void onRefreshClicked()
    {
        if (ECastingMode.SHOT == this.model.getCurrentMode())
        {
            this.model.refreshConstituentStore(this.model.getSelectedShot());
        }
        else if (ECastingMode.CONSTITUENT == this.model.getCurrentMode())
        {
            this.model.refreshShotStore(this.model.getSelectedConstituent());
        }
    }

    /**
     * When project changes, the navigation tree are reloaded and the stores are reconfigured.
     */
    private void onProjectChanged()
    {
        this.model.reloadTrees(MainModel.currentProject);
        this.model.getConstituentStore().removeAll();
        this.model.getShotStore().removeAll();
        this.model.configureStores(MainModel.currentProject);
    }

    /**
     * When a shot is selected, the corresponding constituents are loaded and displayed.
     * 
     * @param event
     *            Shot selected event.
     */
    private void onShotSelected(AppEvent event)
    {
        if (ECastingMode.SHOT == this.model.getCurrentMode())
        {
            List<ShotModelData> shots = event.getData();
            if (shots.isEmpty())
            {
                return;
            }
            ShotModelData shot = shots.get(0);
            this.view.setWorkObjectName(shot.getSequencesNames() + shot.getLabel(), shot.getNbFrame());
            this.model.setSelectedShot(shot);
            this.model.refreshConstituentStore(shot);
        }
    }

    /**
     * When a constituent is selected, the corresponding shots are loaded and displayed.
     * 
     * @param event
     *            Constituent selected event.
     */
    private void onConstituentSelected(AppEvent event)
    {
        if (ECastingMode.CONSTITUENT == this.model.getCurrentMode())
        {
            List<ConstituentModelData> constituents = event.getData();
            if (constituents.isEmpty())
            {
                return;
            }
            ConstituentModelData constituent = constituents.get(0);
            this.view.setWorkObjectName(constituent.getCategoriesNames() + constituent.getLabel(), null);
            this.model.setSelectedConstituent(constituent);
            this.model.refreshShotStore(constituent);
        }
    }

    /**
     * When constituent mode is clicked if button is pressed, the constituent grid is displayed and the shot mode button
     * is unpressed.
     * 
     * @param event
     *            Constituent mode clicked event.
     */
    private void onConstituentModeClicked(AppEvent event)
    {
        Boolean isPressed = event.getData();
        if (isPressed)
        {
            ConstituentModelData constituent = this.model.getSelectedConstituent();
            this.view.disableConstituentModeButton();
            if (constituent != null)
                this.view.setWorkObjectName(constituent.getCategoriesNames() + constituent.getLabel(), null);
            else
                this.view.setWorkObjectName("", null);
            this.view.displayShotList();
            this.model.setCurrentMode(ECastingMode.CONSTITUENT);
        }
    }

    /**
     * When shot mode is clicked if button is pressed, the shot grid is displayed and the constituent mode button is
     * unpressed.
     * 
     * @param event
     *            Shot mode clicked event.
     */
    private void onShotModeClicked(AppEvent event)
    {
        Boolean isPressed = event.getData();
        if (isPressed)
        {
            ShotModelData shot = this.model.getSelectedShot();
            if (shot != null)
                this.view.setWorkObjectName(shot.getSequencesNames() + shot.getLabel(), shot.getNbFrame());
            else
                this.view.setWorkObjectName("", null);
            this.view.disableShotModeButton();
            this.view.displayConstituentList();
            this.model.setCurrentMode(ECastingMode.SHOT);
        }
    }

    /**
     * When drag start from one of casting trees, it checks if the panel corresponds to right mode (shot tree can't be
     * dragged in shot mode, idem for constituent tree and constituent mode).
     * 
     * @param event
     *            The drag start event.
     */
    private void onDragStart(AppEvent event)
    {
        DNDEvent e = event.getData(BreakdownConfig.DNDEVENT_EVENT_VAR_NAME);
        ECastingMode mode = event.getData(BreakdownConfig.FORMODE_EVENT_VAR_NAME);

        if (this.model.getCurrentMode() != mode)
        {
            e.setCancelled(true);
        }
    }

    /**
     * When constituent list is dropped, it creates all composition objects between constituents dropped and current
     * shot.
     * 
     * @param event
     *            The constituent dropped event.
     */
    private void onCastingConstituentDropped(AppEvent event)
    {
        List<ConstituentModelData> constituents = event.getData();

        for (ConstituentModelData constituent : constituents)
        {
            CompositionModelData.getOrCreate(constituent, this.model.getSelectedShot());
        }
    }

    /**
     * When shot list is dropped, it creates all composition objects between shots dropped and current constituent.
     * 
     * @param event
     *            The shot dropped event.
     */
    private void onCastingShotDropped(AppEvent event)
    {
        List<ShotModelData> shots = event.getData();

        for (ShotModelData shot : shots)
        {
            CompositionModelData.getOrCreate(this.model.getSelectedConstituent(), shot);
        }
    }

    /**
     * Register global events the controller handles.
     */
    private void registerEvents()
    {
        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);

        this.registerEventTypes(BreakdownEvents.CONSTITUENT_MODE_CLICKED);
        this.registerEventTypes(BreakdownEvents.SHOT_MODE_CLICKED);
        this.registerEventTypes(BreakdownEvents.CASTING_CONSTITUENT_DROPPED);
        this.registerEventTypes(BreakdownEvents.CASTING_SHOT_DROPPED);
        this.registerEventTypes(BreakdownEvents.CASTING_DRAG_START);
        this.registerEventTypes(BreakdownEvents.CASTING_REMOVE_CLICKED);
        this.registerEventTypes(BreakdownEvents.CASTING_REFRESH_CLICKED);
    }

}
