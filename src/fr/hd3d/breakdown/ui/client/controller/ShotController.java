package fr.hd3d.breakdown.ui.client.controller;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.model.CreationData;
import fr.hd3d.breakdown.ui.client.view.ShotView;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.model.ShotModel;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.workobject.browser.controller.WorkObjectController;


/**
 * Controller handling events raised by or for shots view.
 * 
 * @author HD3D
 */
public class ShotController extends WorkObjectController
{
    /** Model handling constituent view data (explorer and sheet combo stores). */
    private final ShotModel model;
    /** Constituent view containing tree, explorer and identity widget for constituents. */
    private final ShotView view;

    /**
     * Default constructor.
     * 
     * @param view
     *            Shot view containing tree, explorer and identity widget for constituents.
     * @param model
     *            Model handling shot view data (explorer and sheet combo stores).
     */
    public ShotController(ShotModel model, ShotView view)
    {
        super(view, model);

        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == CommonEvents.SEQUENCES_SELECTED)
        {
            this.onSequenceSelected(event);
        }
        else if (type == CommonEvents.SHOTS_SELECTED)
        {
            this.onShotSelected(event);
        }
        else if (type == BreakdownEvents.SHOT_CREATE_CLICKED)
        {
            this.onShotCreateClicked(event);
        }
        else if (type == BreakdownEvents.SHOT_CREATE_SEQUENCE_CLICKED)
        {
            this.onSequenceCreateClicked(event);
        }
        else if (type == BreakdownEvents.WORK_OBJECT_CREATION_REQUESTED)
        {
            this.onWorkObjectCreationRequested(event);
        }
        else if (type == BreakdownEvents.SHOT_MASS_CREATION_SUCCESS)
        {
            this.onShotMassCreationSuccess(event);
        }
        else if (type == BreakdownEvents.SHOT_RENAME_CLICKED)
        {
            this.onShotRenameClicked();
        }
        else if (type == BreakdownEvents.SHOT_RENAMED)
        {
            this.onShotRenamed(event);
        }
        else if (type == BreakdownEvents.SHOT_DELETE_CLICKED)
        {
            this.onShotDeleteClicked(event);
        }
        else if (type == BreakdownEvents.SHOT_DELETE_CONFIRMED)
        {
            this.onShotDeleteConfirmed(event);
        }
        else if (type == BreakdownEvents.SELECTION_DROPPED)
        {
            this.onSelectionDropped(event);
        }
        else if (type == BreakdownEvents.SHOT_SEQUENCE_DELETE_CLICKED)
        {
            this.onShotSequenceDeletedClicked(event);
        }
        else if (type == BreakdownEvents.SHOT_SEQUENCE_DELETE_CONFIRMED)
        {
            this.onShotSequenceDeleteConfirmed(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
    }

    /**
     * When project changes, all data are cleared. Then first sheet of combo box is selected. Explorer filter is updated
     * to retrieve only sequences from this project. Project id is save to cookie for reselecting at next launching.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();
        EqConstraint constraint = new EqConstraint("sequence.project.id", MainModel.currentProject.getId());
        this.model.getFilter().setLeftMember(constraint);
        this.model.removeSequenceConstraint();
        this.model.removeShotConstraint();

        if (project != null)
        {
            this.view.setExplorerView(null);
            this.view.reloadTree(project);

            this.isFirstSheetLoading = true;

            String cookieKey = BreakdownConfig.SHEET_COOKIE_VAR_PREFIX + this.model.getEntityName() + "-"
                    + MainModel.currentProject.getId();
            this.view.setSheetCookie(cookieKey);
            this.view.reloadSheets(project, ShotModelData.SIMPLE_CLASS_NAME.toLowerCase());
        }
    }

    /**
     * When a sequence is selected, explorer filter is updated. If the all sequence is selected (id = -1) the default
     * filter is set (filter on whole project). If another sequence is selected a lucene constraint is set. It will make
     * the explorer to retrieve only shots from the selected sequence and from its children.
     * 
     * @param event
     *            The sequence selected event.
     */
    private void onSequenceSelected(AppEvent event)
    {
        List<SequenceModelData> sequences = event.getData();
        if (CollectionUtils.isNotEmpty(sequences))
        {
            SequenceModelData sequence = sequences.get(0);
            Constraint constraint = new Constraint(EConstraintOperator.eq, "sequence.project.id",
                    MainModel.currentProject.getId());

            this.model.removeSequenceConstraint();
            this.model.removeShotConstraint();
            this.model.getFilter().setLeftMember(constraint);
            if (sequence.getId() != -1)
            {
                this.model.setConstraintSequence(sequences);
            }

            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /**
     * Modify explorer store parameters to display only selected shot.
     * 
     * @param event
     *            The shot selected event.
     */
    private void onShotSelected(AppEvent event)
    {
        List<ShotModelData> shots = event.getData();
        if (CollectionUtils.isNotEmpty(shots))
        {
            this.model.removeSequenceConstraint();
            this.model.removeShotConstraint();            
            this.model.setConstraintShot(shots);

            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /**
     * When work object creation is requested it analyzes if mass creation or simple creation is requested.<br>
     * If it is simple creation, it creates a work object with name given by user. If it is mass creation, it creates a
     * list of constituents or categories depending on parameters given by user (start index, end index and step).
     * 
     * @param event
     *            Work object creation requested event.
     */
    private void onWorkObjectCreationRequested(AppEvent event)
    {
        CreationData data = event.getData();
        final RecordModelData parent = (RecordModelData) CollectionUtils.getFirst(this.view.getTreeSelection());

        if (!data.getMassCreation())
        {
            this.onSimpleCreation(data, parent);
        }
        else
        {
            this.onMassCreation(data, parent);
        }
    }

    /**
     * Creates a list of shots or sequences depending on parameters given by user : start index, end index and step. <br>
     * Ex : If class = Shot, name = myShot, start index = 1, end index = 10 and step = 1. It will create myShot 01,
     * myShot 02, ... myShot 10.
     * 
     * @param data
     *            Data contains parameters : name, simple class name, start index, end index.
     * @param parent
     *            Parent under which instances will be created.
     * 
     */
    private void onMassCreation(CreationData data, RecordModelData parent)
    {
        List<Hd3dModelData> records = new ArrayList<Hd3dModelData>();
        int nbDigits = data.getEndIndex().toString().length();

        if (FieldUtils.isShot(data.getSimpleClassName()))
        {
            for (int i = data.getStartIndex(); i <= data.getEndIndex(); i = i + data.getStep())
            {
                String name = data.getName() + FieldUtils.getDigitNumber(i, nbDigits);
                records.add(this.getNewShot(name, parent));
            }
        }
        else if (FieldUtils.isSequence(data.getSimpleClassName()))
        {
            for (int i = data.getStartIndex(); i <= data.getEndIndex(); i = i + data.getStep())
            {
                String name = data.getName() + FieldUtils.getDigitNumber(i, nbDigits);
                records.add(this.getNewSequence(name, parent));
            }
        }

        AppEvent event = new AppEvent(BreakdownEvents.SHOT_MASS_CREATION_SUCCESS, parent);
        event.setData(BreakdownConfig.PARENT_EVENT_VAR_NAME, parent);
        BulkRequests.bulkRangePost(records, event, 50);
    }

    /**
     * On simple creation, a new constituent or category is created with given name under parent node.
     * 
     * @param data
     *            Data contains name and class name for new instance.
     * @param parent
     *            It is the parent inside tree of the future instance.
     */
    private void onSimpleCreation(CreationData data, final RecordModelData parent)
    {
        if (FieldUtils.isShot(data.getSimpleClassName()))
        {
            final ShotModelData shot = this.getNewShot(data.getName(), parent);

            shot.save(new PostModelDataCallback(shot, null) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    this.setNewId(response);

                    model.getTreeStore().add(parent, shot, false);
                    view.display("Creation succeeds", "Shot " + shot.getLabel() + " created.");
                    view.enableCreationDialog();
                    view.expandParent(parent);
                }
            });
        }
        else if (FieldUtils.isSequence(data.getSimpleClassName()))
        {
            final SequenceModelData sequence = this.getNewSequence(data.getName(), parent);

            sequence.save(new PostModelDataCallback(sequence, null) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    this.setNewId(response);

                    model.getTreeStore().add(parent, sequence, false);
                    view.display("Creation succeeds", "Sequence " + sequence.getName() + " created.");
                    view.enableCreationDialog();
                    view.expandParent(parent);
                }
            });
        }
    }

    /**
     * When create shot is clicked, the creation dialog is displayed for constituent.
     * 
     * @param event
     *            Create Shot Clicked.
     */
    private void onShotCreateClicked(AppEvent event)
    {
        this.view.displayCreationDialog(ShotModelData.SIMPLE_CLASS_NAME, BreakdownEvents.SHOT_CREATED, "Create shots");
    }

    /**
     * When create category is clicked, the creation dialog is displayed for constituent.
     * 
     * @param event
     *            Category Create Clicked.
     */
    private void onSequenceCreateClicked(AppEvent event)
    {
        this.view.displayCreationDialog(SequenceModelData.SIMPLE_CLASS_NAME, BreakdownEvents.SHOT_CREATED,
                "Create sequences");
    }

    /**
     * When mass creation succeeds, It reloads tree node where creation is occurred. Creation dialog is reenabled.
     * 
     * @param event
     *            The mass creations success event.
     */
    private void onShotMassCreationSuccess(AppEvent event)
    {
        RecordModelData parent = event.getData(BreakdownConfig.PARENT_EVENT_VAR_NAME);
        this.model.getTreeStore().getLoader().loadChildren(parent);
        view.display("Creation succeeds", "Mass creation worked correctly");

        this.view.enableCreationDialog();
    }

    /**
     * When constituent rename is clicked, the rename dialog is displayed.
     */
    private void onShotRenameClicked()
    {
        List<RecordModelData> constituents = this.view.getTreeSelection();
        if (CollectionUtils.isNotEmpty(constituents))
        {
            this.view.showRenameDialog(constituents.get(0), BreakdownEvents.SHOT_RENAMED);
        }
    }

    /**
     * When constituent is renamed remotely, the tree is updated to display the new name.
     * 
     * @param event
     *            Constituent renamed event.
     */
    private void onShotRenamed(AppEvent event)
    {
        RecordModelData record = event.getData();
        this.model.getTreeStore().update(record);
    }

    /**
     * When category is deleted it shows a category dialog box.
     * 
     * @param event
     *            Constituent deleted clicked.
     */
    private void onShotSequenceDeletedClicked(AppEvent event)
    {
        this.view.showSequenceDeleteConfirmation();
    }

    /**
     * When user confirm category deletion, the category is remotely deleted and removed from the navigation tree.
     * 
     * @param event
     *            Category deletion confirmed.
     */
    private void onShotSequenceDeleteConfirmed(AppEvent event)
    {
        List<RecordModelData> sequences = this.view.getTreeSelection();
        for (RecordModelData sequence : sequences)
        {
            if (FieldUtils.isSequence(sequence))
            {
                sequence.delete();
                this.model.getTreeStore().remove(sequence);
            }
        }
    }

    /**
     * When constituent is deleted it shows a confirmation dialog box.
     * 
     * @param event
     *            Constituent deleted clicked.
     */
    private void onShotDeleteClicked(AppEvent event)
    {
        this.view.showShotDeleteConfirmation();
    }

    /**
     * When user confirm constituent deletion, the constituent is remotely deleted and removed from the navigation tree.
     * 
     * @param event
     *            Constituent deleted confirmed.
     */
    private void onShotDeleteConfirmed(AppEvent event)
    {
        List<RecordModelData> shots = this.view.getTreeSelection();
        for (RecordModelData shot : shots)
        {
            if (FieldUtils.isShot(shot))
            {
                shot.delete();
                this.model.getTreeStore().remove(shot);
            }
        }
    }

    /**
     * When a selection is dropped, it updates parent and sequence fields with id of sequence on which drop is
     * occurring.
     * 
     * @param event
     *            The selection dropped event.
     */
    private void onSelectionDropped(AppEvent event)
    {
        RecordModelData parent = event.getData(BreakdownConfig.DND_PARENT_EVENT_VAR_NAME);
        List<Hd3dModelData> shots = event.getData(BreakdownConfig.DND_SHOT_EVENT_VAR_NAME);
        List<Hd3dModelData> sequences = event.getData(BreakdownConfig.DND_SEQUENCE_EVENT_VAR_NAME);

        for (Hd3dModelData shot : shots)
        {
            shot.set(ShotModelData.SHOT_SEQUENCE, parent.getId());
        }

        for (Hd3dModelData sequence : sequences)
        {
            Long parentId = parent.getId();
            if (parent.getId() == -1L)
                parentId = MainModel.currentProject.getRootSequenceId();
            sequence.set(SequenceModelData.SEQUENCE_PARENT, parentId);
        }

        if (shots.size() > 0)
            BulkRequests.bulkPut(shots, CommonEvents.MODEL_DATA_UPDATE_SUCCESS);
        if (sequences.size() > 0)
            BulkRequests.bulkPut(sequences, CommonEvents.MODEL_DATA_UPDATE_SUCCESS);
    }

    /**
     * Make a new constituent with correct default path.
     * 
     * @param event
     *            Constituent Create Clicked.
     */
    private ShotModelData getNewShot(String name, RecordModelData parent)
    {
        ShotModelData shot = new ShotModelData();
        shot.setSequence(parent.getId());
        shot.setLabel(name);
        shot.setName(name);
        shot.setHook(name.toLowerCase().replace(" ", "_"));
        shot.setDefaultPath(MainModel.currentProject.getId(), parent.getId());

        return shot;
    }

    /**
     * Make a new sequence with correct default path.
     * 
     * @param name
     *            The sequence name.
     * @param parent
     *            The sequence parent inside tree.
     */
    private SequenceModelData getNewSequence(String name, RecordModelData parent)
    {
        SequenceModelData sequence = new SequenceModelData();
        if (parent != null)
            sequence.setParent(parent.getId());
        sequence.setName(name);
        sequence.setProject(MainModel.currentProject.getId());

        return sequence;
    }

    /**
     * If error occurs, saving indicator is hidden and creation dialog is enabled.
     */
    @Override
    protected void onError(AppEvent event)
    {
        super.onError(event);
        this.view.enableTree();
        this.view.enableCreationDialog();
    }
    
    @Override
    protected void onExplorerDataLoaded(AppEvent event) 
    {
        super.onExplorerDataLoaded(event);
    	this.view.enableTree();
	}


    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(CommonEvents.SHOTS_SELECTED);
        this.registerEventTypes(CommonEvents.SEQUENCES_SELECTED);
        this.registerEventTypes(BreakdownEvents.SHOT_RENAME_CLICKED);
        this.registerEventTypes(BreakdownEvents.SHOT_RENAMED);
        this.registerEventTypes(BreakdownEvents.SHOT_DELETE_CLICKED);
        this.registerEventTypes(BreakdownEvents.SHOT_DELETE_CONFIRMED);
        this.registerEventTypes(BreakdownEvents.SHOT_SEQUENCE_DELETE_CLICKED);
        this.registerEventTypes(BreakdownEvents.SHOT_SEQUENCE_DELETE_CONFIRMED);
        this.registerEventTypes(BreakdownEvents.SHOT_CREATE_CLICKED);
        this.registerEventTypes(BreakdownEvents.SHOT_CREATED);
        this.registerEventTypes(BreakdownEvents.SHOT_CREATE_SEQUENCE_CLICKED);
        this.registerEventTypes(BreakdownEvents.WORK_OBJECT_CREATION_REQUESTED);
        this.registerEventTypes(BreakdownEvents.SHOT_MASS_CREATION_SUCCESS);
        this.registerEventTypes(BreakdownEvents.SELECTION_DROPPED);

        this.registerEventTypes(CommonEvents.ERROR);
    }
}
