package fr.hd3d.breakdown.ui.client.controller;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.TreeModel;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.event.DNDListener;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel.Cell;

import fr.hd3d.breakdown.ui.client.event.CastingEvents;
import fr.hd3d.breakdown.ui.client.model.CastingModelData;
import fr.hd3d.breakdown.ui.client.model.CastingSequenceModel;
import fr.hd3d.breakdown.ui.client.view.ICastingSequenceView;
import fr.hd3d.breakdown.ui.client.view.dnd.CastingSequenceDropTarget;
import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.clipboard.ClipBoard;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Controller handling casting events.
 * 
 * @author HD3D
 */
public class CastingSequenceController extends MaskableController
{
    /** Model handling casting data. */
    private final CastingSequenceModel model;

    /** View displaying casting widgets. */
    private final ICastingSequenceView view;

    private double nbCompositionSaving = 0;
    private double cpt = 0;

    /**
     * Default controller.
     * 
     * @param view
     *            View displaying casting widgets
     * @param model
     *            Model handling casting data
     */
    public CastingSequenceController(ICastingSequenceView view, CastingSequenceModel model)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    /**
     * Return the drag'n drop listener.
     * 
     * @return the drag'n drop listener.
     */
    public DNDListener getDNDListener()
    {
        return new DNDListener() {
            @Override
            public void dragDrop(DNDEvent e)
            {
                EventDispatcher.forwardEvent(CastingEvents.CONSTITUENT_DROPPED, e);
            }
        };
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (CastingEvents.COMPOSITION_LOADED == type)
        {
            this.onCompositionLoaded();
        }
        else if (CommonEvents.PROJECT_CHANGED == type)
        {
            this.onProjectChanged();
        }
        else if (CastingEvents.CATEGORIES_LOADED == type)
        {
            this.onCategoriesLoaded();
        }
        else if (CastingEvents.SHOT_LOADED == type)
        {
            this.onShotLoaded();
        }
        else if (CommonEvents.SEQUENCES_SELECTED == type)
        {
            this.onSequenceSelected(event);
        }
        else if (CastingEvents.COPY_KEYPRESS == type)
        {
            this.onCopy(event);
        }
        else if (CastingEvents.PASTE_KEYPRESS == type)
        {
            this.onPaste(event);
        }
        else if (CastingEvents.CONSTITUENT_DROPPED == type)
        {
            this.onConstituentDropped(event);
        }
        else if (CastingEvents.DELETE_KEYPRESS == type)
        {
            this.onDeletePressed(event);
        }
        else if (CastingEvents.DELETE_COMPOSITION_CLICKED_FROM_CONTEXTMENU == type)
        {
            this.onDeleteClickedFromContextMenu(event);
        }
        else if (CastingEvents.REFRESH_CLICKED == type)
        {
            this.onRefresh(event);
        }
        else if (CastingEvents.REFRESH_CATEGORIE_CLICKED == type)
        {
            this.onRefreshCategorie(event);
        }
        else if (CastingEvents.COMPOSITION_SAVE_SUCCESS == type)
        {
            this.onSaveCompositionSuccess(event);
        }
        else if (CastingEvents.CATEGORIES_ROOT_LOADED == type)
        {
            this.onCategoriesRootLoaded();
        }
        else if (CastingEvents.AFTER_EDIT == type)
        {
            this.onAfterEdit(event);
        }
        else if (CastingEvents.SHOT_SAVE_SUCCESS == type)
        {
            this.onSaveShotSuccess(event);
        }
        else if (ExplorerEvents.COLUMN_HIDDEN_CHANGED == type)
        {
            this.onColumnHiddenChanged(event);
        }
        else if (ExplorerEvents.COLUMN_WIDTH_CHANGED == type)
        {
            this.onColumnWidthChanged(event);
        }
        else if (CastingEvents.EXPORT_CASTING_CSV == type)
        {
            this.onExportCsv(event);
        }
        this.forwardToChild(event);
    }

    private void onExportCsv(AppEvent event)
    {
        String exportcasting = ServicesURI.PROJECTS + '/' + MainModel.getCurrentProject().getId() + '/'
                + ServicesURI.CASTING_EXPORT;
        String path = RestRequestHandlerSingleton.getInstance().getServicesUrl() + exportcasting;
        this.view.openCsvExport(path);
    }

    private void onRefreshCategorie(AppEvent event)
    {
        this.model.loadCategories(this.model.getSelectedProject());
    }

    private void onColumnWidthChanged(AppEvent event)
    {
        if (event.getData() == null || this.model.getSelectedProject() == null)
        {
            return;
        }
        ColumnConfig cc = event.getData();
        String key = "breakdown.";
        key += "casting.";
        key += "project.";
        key += this.model.getSelectedProject().getId();
        key += ".item.";
        key += cc.getId();
        key += ":width";

        String value = (new Integer(cc.getWidth())).toString();

        UserSettings.setSetting(key, value);
    }

    private void onColumnHiddenChanged(AppEvent event)
    {
        if (event.getData() == null || this.model.getSelectedProject() == null)
        {
            return;
        }
        ColumnConfig cc = event.getData();
        String key = "breakdown.";
        key += "casting.";
        key += "project.";
        key += this.model.getSelectedProject().getId();
        key += ".item.";
        key += cc.getId();
        key += ":hidden";

        Boolean value = cc.isHidden();
        UserSettings.setSetting(key, value.toString());
    }

    /**
     * When the save of shot succeed, update the store to commit change.
     * 
     * @param event
     *            the event containing the id of saved shot.
     */
    private void onSaveShotSuccess(AppEvent event)
    {

        List<CastingModelData> castings = this.view.getCastingSelected();

        for (CastingModelData casting : castings)
        {
            this.model.getCastingStore().getRecord(casting).commit(false);
        }
        this.view.unmaskLoading();
    }

    /**
     * When description is edited, save the shot.
     * 
     * @param event
     *            the event containing the modified casting.
     */
    private void onAfterEdit(AppEvent event)
    {
        CastingModelData castingEdit = event.getData();
        if (castingEdit == null)
        {
            return;
        }

        ArrayList<ShotModelData> shots = new ArrayList<ShotModelData>();
        List<CastingModelData> castings = this.view.getCastingSelected();
        for (CastingModelData casting : castings)
        {
            ShotModelData shot = this.model.getShot(casting.getShotId());
            if (shot == null)
            {
                continue;
            }
            shot.setDescription(castingEdit.getShotDescription());
            casting.setShotDescriptin(castingEdit.getShotDescription());
            shot.setNbFrame(castingEdit.getShotNbFrame());
            shot.setStartFrame(castingEdit.getShotStartFrame());
            shot.setEndFrame(castingEdit.getShotEndFrame());
            casting.setShotNbFrame(castingEdit.getShotNbFrame());
            casting.setShotStartFrame(castingEdit.getShotStartFrame());
            casting.setShotEndFrame(castingEdit.getShotEndFrame());
            shots.add(shot);
        }
        this.view.maskLoading();
        BulkRequests.bulkPut(shots, CastingEvents.SHOT_SAVE_SUCCESS);
    }

    /**
     * When the root categories loaded, reconfigure the grid.
     */
    private void onCategoriesRootLoaded()
    {
        this.view.reconfigureGrid();
    }

    /**
     * When the composition save success, reload the data.
     * 
     * @param event
     *            the event.
     */
    private void onSaveCompositionSuccess(AppEvent event)
    {
        CompositionModelData composition = event.getData(CompositionModelData.CLASS_NAME);
        if (composition == null)
        {
            return;
        }
        Integer nb = event.getData(CastingModelData.NB_RECORDS_REMAIN);
        if (nb == null || nb < 1)
        {
            CastingModelData casting = this.model.getCasting(composition);
            this.model.getCastingStore().update(casting);
            if (cpt > 0)
            {
                this.view.changeProgress(1.0 - (cpt - 1.0) / nbCompositionSaving);
                cpt--;
            }
            else
            {
                this.view.changeProgress(1.0);
            }

        }
    }

    /**
     * When the refresh asked, reload the shots from a sequence
     * 
     * @param event
     *            the event.
     */
    private void onRefresh(AppEvent event)
    {
        this.model.loadCategories(MainModel.getCurrentProject());
        if (!this.model.getSelectedSequences().isEmpty())
        {
            this.view.maskLoading();
        }
        this.model.loadShots();
    }

    /**
     * When delete button is clicked from the context menu, remove the composition to casting and deselect the selection
     * (because GXT deselects the grid).
     * 
     * @param event
     *            the event
     */
    private void onDeleteClickedFromContextMenu(AppEvent event)
    {
        List<CompositionModelData> compositions = event.getData();
        if (compositions == null || compositions.isEmpty())
        {
            return;
        }

        for (CompositionModelData composition : compositions)
        {
            CastingModelData casting = this.model.getCasting(composition);
            if (casting == null)
            {
                continue;
            }
            casting.removeComposition(composition);
            this.model.getCastingStore().update(casting);
        }
        this.view.deselectedRows();
    }

    /**
     * When delete button is pressed, remove all compositions to casting and deselect the selection (because GXT
     * deselects the grid).
     * 
     * @param event
     *            the event.
     */
    private void onDeletePressed(AppEvent event)
    {
        String columnId = this.view.getColumnIdSelected();
        if (columnId == null)
        {
            return;
        }

        List<Cell> cellList = this.view.getCellsSelected();
        if (cellList == null || cellList.isEmpty())
        {
            return;
        }

        this.view.infoProgress("Info", "deleting...");
        boolean removed = false;
        for (Cell cell : cellList)
        {
            boolean remove = this.model.remove(columnId, cell.row, CastingEvents.COMPOSITION_SAVE_SUCCESS);
            if (remove)
            {
                removed = true;
                cpt++;
            }
        }
        nbCompositionSaving = cpt;
        // if no composition is deleted also prevent the view to stop
        if (!removed)
        {
            this.view.changeProgress(1.0);
        }

        this.view.deselectedRows();
    }

    /**
     * When a constituent is dropped, add composition to casting.
     * 
     * @param event
     *            the event.
     */
    @SuppressWarnings("unchecked")
    private void onConstituentDropped(AppEvent event)
    {
        DNDEvent e = event.getData();
        CastingSequenceDropTarget target = (CastingSequenceDropTarget) e.getDropTarget();
        int index = target.getActiveItemIndex(e);
        CastingModelData casting = (CastingModelData) target.getGrid().getStore().getAt(index);
        if (casting == null)
        {
            return;
        }

        Object obj = e.getDragSource().getData();
        ArrayList<TreeModel> modelList = (ArrayList<TreeModel>) obj;
        for (TreeModel models : modelList)
        {
            ModelData data = models.get("model");
            if (ConstituentModelData.CLASS_NAME.equals(data.get("class")))
            {
                ConstituentModelData constituent = (ConstituentModelData) data;
                boolean categoryExist = false;
                for (CategoryModelData category : this.model.getCategoriesStore().getModels())
                {
                    if (category.getId().toString().equals(constituent.getCategory().toString()))
                    {
                        categoryExist = true;
                        break;
                    }
                }
                if (categoryExist)
                {
                    casting.addComposition(constituent, null);
                }
                else
                {
                    Logger.warn("Category is not present in the grid");
                }
            }
        }
        target.getGrid().getStore().update(casting);
    }

    /**
     * When paste button is pressed, copy the clipboard to casting.
     * 
     * @param event
     *            the event.
     */
    @SuppressWarnings("unchecked")
    private void onPaste(AppEvent event)
    {

        List<Cell> cellList = this.view.getCellsSelected();
        if (cellList == null || cellList.isEmpty())
        {
            return;
        }

        Object objToPaste = ClipBoard.get().paste();
        if (objToPaste == null)
        {
            return;
        }

        if (objToPaste instanceof List)
        {
            List<? extends Hd3dModelData> list = (List<? extends Hd3dModelData>) objToPaste;

            if (list.isEmpty())
            {
                return;
            }

            if (list.get(0).getClassName().equals(CompositionModelData.CLASS_NAME))
            {
                List<CompositionModelData> compositionList = (List<CompositionModelData>) list;
                this.view.infoProgress("Info", "Saving...");
                nbCompositionSaving = cellList.size();
                cpt = nbCompositionSaving;
                for (int i = 0; i < cellList.size(); i++)
                {
                    Cell cell = cellList.get(i);
                    this.model.insert(cell.row, compositionList, CastingEvents.COMPOSITION_SAVE_SUCCESS);
                }
                this.view.deselectedRows();
            }
        }
    }

    /**
     * When copy button is pressed, copy data to the clipboard.
     * 
     * @param event
     *            the event.
     */
    private void onCopy(AppEvent event)
    {
        if (!this.view.isMultiSelectionGrid())
        {
            ClipBoard.get().copy(this.view.getCompositionSelectedGrid());
        }
        else
        {
            this.view.infoDisplay("Warning", "Cannot copy on multi selection.");
        }
    }

    /**
     * When a sequence is selected, the shot is filtering by the selected sequence.
     * 
     * @param event
     *            the sequence selected.
     */
    private void onSequenceSelected(AppEvent event)
    {
        List<SequenceModelData> selectedSequences = event.getData();
        if (selectedSequences.isEmpty())
        {
            return;
        }

        this.model.setSelectedSequences(selectedSequences);
        this.model.loadShots();

        this.view.maskLoading();

    }

    /**
     * When the composition data is loaded, update the composition map.
     */
    private void onCompositionLoaded()
    {
        this.model.updateCompositionMap();
        this.model.setCastingStore();
        this.view.unmaskLoading();
    }

    /**
     * When the shot data is loaded, reload the composition store.
     */
    private void onShotLoaded()
    {
        if (this.model.getShotStore().getCount() != 0)
        {
            this.model.reloadCompositionStore();
        }
        else
        {
            this.model.setCastingStore();
            this.view.unmaskLoading();
        }
    }

    /**
     * When the categories is loaded, update the map.
     */
    private void onCategoriesLoaded()
    {
        this.model.updateCategoriesMap();
    }

    /**
     * When the project is changed, the data is reinitialized.
     */
    private void onProjectChanged()
    {
        if (MainModel.currentProject != null)
        {
            this.model.initialize(MainModel.currentProject);
        }
    }

    /**
     * Register global events the controller handles.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(CastingEvents.COMPOSITION_LOADED);
        this.registerEventTypes(CastingEvents.CATEGORIES_LOADED);
        this.registerEventTypes(CastingEvents.SHOT_LOADED);
        this.registerEventTypes(CastingEvents.CASTING_LOADED);
        this.registerEventTypes(CastingEvents.CONSTITUENT_DROPPED);
        this.registerEventTypes(CastingEvents.COPY_KEYPRESS);
        this.registerEventTypes(CastingEvents.PASTE_KEYPRESS);
        this.registerEventTypes(CastingEvents.DELETE_KEYPRESS);
        this.registerEventTypes(CastingEvents.REFRESH_CLICKED);
        this.registerEventTypes(CastingEvents.REFRESH_CATEGORIE_CLICKED);
        this.registerEventTypes(CastingEvents.DELETE_COMPOSITION_CLICKED_FROM_CONTEXTMENU);
        this.registerEventTypes(CastingEvents.COMPOSITION_SAVE_SUCCESS);
        this.registerEventTypes(CastingEvents.CATEGORIES_ROOT_LOADED);
        this.registerEventTypes(CastingEvents.AFTER_EDIT);
        this.registerEventTypes(CastingEvents.SHOT_SAVE_SUCCESS);
        this.registerEventTypes(CastingEvents.EXPORT_CASTING_CSV);

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);

        this.registerEventTypes(ExplorerEvents.COLUMN_WIDTH_CHANGED);
        this.registerEventTypes(ExplorerEvents.COLUMN_HIDDEN_CHANGED);
    }
}
