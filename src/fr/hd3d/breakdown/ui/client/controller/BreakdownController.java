package fr.hd3d.breakdown.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.event.CastingEvents;
import fr.hd3d.breakdown.ui.client.model.BreakdownModel;
import fr.hd3d.breakdown.ui.client.view.CastingSequenceView;
import fr.hd3d.breakdown.ui.client.view.CastingView;
import fr.hd3d.breakdown.ui.client.view.IBreakdownView;
import fr.hd3d.breakdown.ui.client.view.IConstituentView;
import fr.hd3d.breakdown.ui.client.view.SequenceView;
import fr.hd3d.breakdown.ui.client.view.ShotView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.parameter.ScriptParams;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.ScriptUtils;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.SheetEditorDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


/**
 * Controller handling Breakdown Tool events.
 */
public class BreakdownController extends MainController
{

    /** Main model handling production view data. */
    private final BreakdownModel model;
    /** Main view, containing main layout and all widgets. */
    private final IBreakdownView view;

    /**
     * Default constructor.
     * 
     * @param view
     *            Production view
     * @param model
     *            Production model
     */
    public BreakdownController(IBreakdownView view, BreakdownModel model)
    {
        super(model, view);

        this.view = view;
        this.model = model;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_TAB_CLICKED)
        {
            this.onConstituentTabClicked();
        }
        else if (type == BreakdownEvents.SHOT_TAB_CLICKED)
        {
            this.onShotTabClicked();
        }
        else if (type == BreakdownEvents.SEQUENCE_TAB_CLICKED)
        {
            this.onSequenceTabClicked();
        }
        else if (type == BreakdownEvents.CASTING_TAB_CLICKED)
        {
            this.onCastingTabClicked();
        }
        else if (type == BreakdownEvents.CASTING_SEQUENCE_TAB_CLICKED)
        {
            this.onCastingSequenceTabClicked();
        }
        else if (type == ExplorerEvents.SELECT_CHANGE)
        {
            this.onExplorerSelectChange(event);
        }
        else if (type == ExplorerEvents.AUTO_SAVE_TOGGLE)
        {
            this.forwardToChildForced(event);
        }
        else if (type == BreakdownEvents.ODS_IMPORT_CLICKED)
        {
            this.onOdsImportClicked();
        }
        else if (type == BreakdownEvents.FILE_SUBMITTED)
        {
            this.onOdsFileSubmitted(event);
        }
        else if (type == BreakdownEvents.UPDATE_ASSET_LIST_CLICKED)
        {
            this.onUpdateAssetListClicked();
        }
        else if (type == BreakdownEvents.UPDATE_TASK_LIST_CLICKED)
        {
            this.onUpdateTaskListClicked();
        }
        else if (type == BreakdownEvents.SCRIPT_EXECUTION_FINISHED)
        {
            this.onScriptExecutionFinished();
        }
        else if (type == BreakdownEvents.ODS_IMPORT_COMPLETE)
        {
            this.onOdsImportFinished();
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
            this.forwardToChild(event);
        }
        else if (type == SheetEditorEvents.NEW_SHEET_CLICKED || type == SheetEditorEvents.EDIT_SHEET_CLICKED)
        {
            this.forwardToChild(event);
            SheetEditorDisplayer.hideValidationButton();
        }
        else if (type == CommonEvents.TASK_TYPE_BUTTON_CLICKED)
        {
            this.onTaskTypeButtonClicked();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When the task type editor button is pressed, display the task type editor.
     */
    private void onTaskTypeButtonClicked()
    {
        this.view.displayTaskTypeEditor();
    }

    /**
     * When user settings are initialized, view is built, loading panel is hidden, controller hierarchy is made then
     * last environment is rebuilt (last selected project since last connection...).
     * 
     * @param event
     *            The setting initialized event.
     */
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        super.onSettingInitialized(event);

        ExcludedField.addExcludedField("activitiesDuration");
        ExcludedField.addExcludedField("boundTasks");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskDurationCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCANCELLEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCLOSEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskOKCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskSTANDBYCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWAITAPPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWIPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TotalNbOfTasksCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskWorkersCollectionQuery");
        ExcludedField.addExcludedField("tasksActivitiesDuration");
        ExcludedField.addExcludedField("tasksEstimation");
        ExcludedField.addExcludedField("tasksStatus");
        ExcludedField.addExcludedField("path");

        ExcludedField.addExcludedField("modeling");
        ExcludedField.addExcludedField("hairs");
        ExcludedField.addExcludedField("setup");
        ExcludedField.addExcludedField("shading");
        ExcludedField.addExcludedField("texturing");
        ExcludedField.addExcludedField("tracking");
        ExcludedField.addExcludedField("rendering");
        ExcludedField.addExcludedField("layout");
        ExcludedField.addExcludedField("compositing");
        ExcludedField.addExcludedField("dressing");
        ExcludedField.addExcludedField("lighting");
        ExcludedField.addExcludedField("mattepainting");
        ExcludedField.addExcludedField("export");
        ExcludedField.addExcludedField("animation");
        ExcludedField.addExcludedField("layout");
        ExcludedField.addExcludedField("design");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery");
        ExcludedField.addExcludedField("tasksDurationGap");
        ExcludedField.addExcludedField("tasksWorkers");
        ExcludedField.addExcludedField("approvalNotes");
        

        ExcludedField.addExcludedField("approvable");
        ExcludedField.addExcludedField("children");
        ExcludedField.addExcludedField("parentIds");
        ExcludedField.addExcludedField("hook");
        ExcludedField.addExcludedField("root");
        ExcludedField.addExcludedField("project");
        ExcludedField.addExcludedField("shots");
        ExcludedField.addExcludedField("shotsApprovalCompletion");
        ExcludedField.addExcludedField("title");
        ExcludedField.addExcludedField("version");

        this.view.initWidgets();
        this.view.setControllers();
        this.rebuildLastEnvironement();
        this.view.rebuildLastEnvironment();
        this.view.hideStartPanel();
    }

    private void rebuildLastEnvironement()
    {
        Boolean isAutoSave = UserSettings.getSettingAsBoolean(ExplorerConfig.SETTING_AUTOSAVE);
        if (isAutoSave != null)
            ExplorerController.autoSave = isAutoSave;
        if (ExplorerController.autoSave)
            this.view.toggleAutoSave();
        this.view.rebuildLastEnvironment();
    }

    /**
     * When explorer selection changes, tag editor is updated with currently selected record tags.
     * 
     * @param event
     *            Explorer selection change event.
     */
    private void onExplorerSelectChange(AppEvent event)
    {
        Hd3dModelData selectedModel = event.getData(ExplorerEvents.EVENT_VAR_MODELDATA);
        if (selectedModel != null)
        {
            if (ConstituentModelData.SIMPLE_CLASS_NAME.equals(this.model.getCurrentEntity()))
            {
                selectedModel.setSimpleClassName(ConstituentModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(ConstituentModelData.CLASS_NAME);
            }
            else
            {
                selectedModel.setSimpleClassName(ShotModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(ShotModelData.CLASS_NAME);
            }
            // refreshTagEditor(selectedModel);
        }
    }

    /**
     * When shot tab is clicked, shot tab is unidled, constituent tab is idled (controller is masked) then if project
     * has changed since last tab display, it refreshes data. Current entity is set to shot. The tab cookie variable
     * value is set to "shot".
     */
    private void onShotTabClicked()
    {
        this.view.saveTabToCookie(ShotView.NAME);
        this.view.idleConstituentView();
        this.view.idleSequenceView();
        this.view.unidleShotView();
        if (this.model.isProjectChanged())
        {
            this.model.setIsProjectChanged(false);
            this.forwardToChild(new AppEvent(CommonEvents.PROJECT_CHANGED, MainModel.currentProject));
        }
        this.view.refreshTagEditor(null);
        this.model.setCurrentEntity(ShotModelData.SIMPLE_CLASS_NAME);
    }

    /**
     * When sequence tab is clicked, sequence tab is unidled, other tabs are idled (controller is masked) then if project
     * has changed since last tab display, it refreshes data. Current entity is set to sequence. The tab cookie variable
     * value is set to "sequence".
     */
    private void onSequenceTabClicked()
    {
        this.view.saveTabToCookie(SequenceView.NAME);
        this.view.idleConstituentView();
        this.view.idleShotView();
        this.view.unidleSequenceView();
        if (this.model.isProjectChanged())
        {
            this.model.setIsProjectChanged(false);
            this.forwardToChild(new AppEvent(CommonEvents.PROJECT_CHANGED, MainModel.currentProject));
        }
        this.model.setCurrentEntity(SequenceModelData.SIMPLE_CLASS_NAME);
    }
    
    
    private void onCastingTabClicked()
    {
        this.view.saveTabToCookie(CastingView.NAME);
    }

    private void onCastingSequenceTabClicked()
    {
        this.view.saveTabToCookie(CastingSequenceView.NAME);
        EventDispatcher.forwardEvent(CastingEvents.REFRESH_CATEGORIE_CLICKED);
    }

    /**
     * When constituent tab is clicked, constituent tab is unidled, shot tab is idled (controller is masked) then if
     * project has changed since last tab display, it refreshes data. Current entity is set to constituent. The tab
     * cookie variable value is set to "constituent".
     */
    private void onConstituentTabClicked()
    {
        this.view.saveTabToCookie(IConstituentView.NAME);
        this.view.idleShotView();
        this.view.idleSequenceView();
        this.view.unidleConstituentView();
        if (this.model.isProjectChanged())
        {
            this.model.setIsProjectChanged(false);
            this.forwardToChild(new AppEvent(CommonEvents.PROJECT_CHANGED, MainModel.currentProject));
        }
        this.view.refreshTagEditor(null);
        this.model.setCurrentEntity(ConstituentModelData.SIMPLE_CLASS_NAME);
    }

    /**
     * When project changes, all data are cleared. Currently selected tab is refreshed. Explorer will display data when
     * a constituent or a shot will be selected.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        MainModel.currentProject = event.getData();
        this.model.setIsProjectChanged(true);

        this.forwardToChild(event);
    }

    // /**
    // * Refresh tag editor depending on selection.
    // *
    // * @param selectedMode
    // * The selected model on which tag editor should displayed attached tags.
    // */
    // private void refreshTagEditor(Hd3dModelData selectedModel)
    // {
    // List<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
    // selection.add(selectedModel);
    // this.view.refreshTagEditor(selection);
    // }

    /**
     * When ODS import button is clicked, the ods file uploader is displayed.
     */
    private void onOdsImportClicked()
    {
        this.view.displayOdsUploader();
    }

    /**
     * When the ODS file is submitted, the ODS import script is called.
     * 
     * Warning : the script is based on key of file revision. If two different ODS file with same key appears, the file
     * will be randomly selected and wrong data could be imported.
     * */
    private void onOdsFileSubmitted(AppEvent event)
    {
        String fileName = event.getData();

        ScriptParams params = new ScriptParams();

        this.view.showOdsImportLongLoading();
        params.set(ScriptUtils.ODS_PATH_PARAMETER, fileName);
        this.model.loadScript(BreakdownConfig.ODS_IMPORT_SCRIPT, BreakdownEvents.ODS_IMPORT_COMPLETE, params);
    }

    /**
     * When ODS import is finished, the loading panel is hidden and project is relaoded.
     */
    private void onOdsImportFinished()
    {
        AppEvent event = new AppEvent(CommonEvents.PROJECT_CHANGED);
        event.setData(MainModel.currentProject);

        this.onScriptExecutionFinished();
        this.onProjectChanged(event);
    }

    /**
     * When update task list is clicked a request is sent to services to call update task list script.
     */
    private void onUpdateTaskListClicked()
    {
        this.view.showTaskLongLoading();

        this.model.launchScript(BreakdownConfig.UPDATE_TASK_LIST_SCRIPT, BreakdownEvents.SCRIPT_EXECUTION_FINISHED);
    }

    /**
     * When update asset list is clicked a request is sent to services to call update asset list script.
     */
    private void onUpdateAssetListClicked()
    {
        this.view.showAssetLongLoading();

        this.model.launchScript(BreakdownConfig.UPDATE_ASSET_LIST_SCRIPT, BreakdownEvents.SCRIPT_EXECUTION_FINISHED);
    }

    /**
     * When a script ends, the loading panel is hidden.
     */
    private void onScriptExecutionFinished()
    {
        this.view.hideLoading();
    }

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_TAB_CLICKED);
        this.registerEventTypes(BreakdownEvents.SHOT_TAB_CLICKED);
        this.registerEventTypes(BreakdownEvents.SEQUENCE_TAB_CLICKED);
        this.registerEventTypes(BreakdownEvents.CASTING_TAB_CLICKED);
        this.registerEventTypes(BreakdownEvents.CASTING_SEQUENCE_TAB_CLICKED);
        this.registerEventTypes(BreakdownEvents.UPDATE_TASK_LIST_CLICKED);
        this.registerEventTypes(BreakdownEvents.UPDATE_ASSET_LIST_CLICKED);
        this.registerEventTypes(BreakdownEvents.ODS_IMPORT_CLICKED);
        this.registerEventTypes(BreakdownEvents.ODS_IMPORT_COMPLETE);
        this.registerEventTypes(BreakdownEvents.SCRIPT_EXECUTION_FINISHED);
        this.registerEventTypes(BreakdownEvents.FILE_SUBMITTED);

        this.registerEventTypes(ExplorerEvents.AUTO_SAVE_TOGGLE);
        this.registerEventTypes(ExplorerEvents.SELECT_CHANGE);

        this.registerEventTypes(CommonEvents.ERROR);
        this.registerEventTypes(CommonEvents.TASK_TYPE_BUTTON_CLICKED);
    }
}
