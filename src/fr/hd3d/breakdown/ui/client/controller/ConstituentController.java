package fr.hd3d.breakdown.ui.client.controller;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.model.CreationData;
import fr.hd3d.breakdown.ui.client.view.ConstituentView;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.model.ConstituentModel;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.workobject.browser.controller.WorkObjectController;


/**
 * Controller handling events raised by or for constituents view.
 * 
 * @author HD3D
 */
public class ConstituentController extends WorkObjectController
{

    /** Model handling constituent view data (explorer and sheet combo stores). */
    private final ConstituentModel model;
    /** Constituent view containing tree, explorer and identity widget for constituents. */
    private final ConstituentView view;

    /**
     * Default constructor.
     * 
     * @param view
     *            Constituent view containing tree, explorer and identity widget for constituents.
     * @param model
     *            Model handling constituent view data (explorer and sheet combo stores).
     */
    public ConstituentController(ConstituentModel model, ConstituentView view)
    {
        super(view, model);

        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == CommonEvents.PROJECT_CHANGED)
        {
            this.onProjectChanged(event);
        }
        else if (type == CommonEvents.CATEGORIES_SELECTED)
        {
            this.onCategorySelected(event);
        }
        else if (type == CommonEvents.CONSTITUENTS_SELECTED)
        {
            this.onConstituentSelected(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_CREATE_CLICKED)
        {
            this.onConstituentCreateClicked(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_CREATE_CATEGORY_CLICKED)
        {
            this.onCategoryCreateClicked(event);
        }
        else if (type == BreakdownEvents.WORK_OBJECT_CREATION_REQUESTED)
        {
            this.onWorkObjectCreationRequested(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_MASS_CREATION_SUCCESS)
        {
            this.onConstituentMassCreationSuccess(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_RENAME_CLICKED)
        {
            this.onConstituentRenameClicked();
        }
        else if (type == BreakdownEvents.CONSTITUENT_RENAMED)
        {
            this.onConstituentRenamed(event);
        }
        else if (type == BreakdownEvents.SELECTION_DROPPED)
        {
            this.onSelectionDropped(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_DELETE_CLICKED)
        {
            this.onConstituentDeleteClicked(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_DELETE_CONFIRMED)
        {
            this.onConstituentDeleteConfirmed(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_CATEGORY_DELETE_CLICKED)
        {
            this.onConstituentCategoryDeleteClicked(event);
        }
        else if (type == BreakdownEvents.CONSTITUENT_CATEGORY_DELETE_CONFIRMED)
        {
            this.onConstituentCategoryDeleteConfirmed(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
    }

	/**
     * When project changes, all data are cleared. Then first sheet of combo box is selected. Explorer filter is updated
     * to retrieve only categories from this project. Project id is save to cookie for reselection at next launching.
     * 
     * @param event
     *            The project changed event.
     */
    private void onProjectChanged(AppEvent event)
    {
        ProjectModelData project = event.getData();
        Constraint constraint = new Constraint(EConstraintOperator.eq, "category.project.id", project.getId());
        this.model.getFilter().setLeftMember(constraint);
        this.model.removeCategoryConstraint();
        this.model.removeConstituentConstraint();

        if (project != null)
        {
            this.view.setExplorerView(null);
            this.view.reloadTree(project);

            this.isFirstSheetLoading = true;

            String cookieKey = BreakdownConfig.SHEET_COOKIE_VAR_PREFIX + this.model.getEntityName() + "-"
                    + MainModel.currentProject.getId();
            this.view.setSheetCookie(cookieKey);
            this.view.reloadSheets(project, ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase());
        }
    }

    /**
     * When a category is selected, explorer filter is updated. If the all category is selected (id = -1) the default
     * filter is set (filter on whole project). If another category is set, all its children are retrieved. Then a new
     * filter is set on explorer, it retrieves only constituents from the selected category and from its children.
     * 
     * @param event
     *            The category selected event.
     */
    private void onCategorySelected(AppEvent event)
    {
        List<CategoryModelData> categories = event.getData();
        if (CollectionUtils.isNotEmpty(categories))
        {
            CategoryModelData category = categories.get(0);

            this.model.removeCategoryConstraint();
            this.model.removeConstituentConstraint();

            if (category.getId() != -1)
            {
                this.model.setConstraintCategory(categories);
            }
            
            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /**
     * Modify explorer store parameters, then reload store data to display only selected constituent inside explorer.
     * 
     * @param event
     *            The constituent selected event.
     */
    private void onConstituentSelected(AppEvent event)
    {
        List<ConstituentModelData> constituents = event.getData();
        if (CollectionUtils.isNotEmpty(constituents))
        {
            this.model.removeCategoryConstraint();
            this.model.removeConstituentConstraint();
            this.model.setConstraintConstituent(constituents);
            this.view.disableTree();
            this.view.refreshWorkObjectList();
        }
    }

    /**
     * When work object creation is requested it analyzes if mass creation or simple creation is requested.<br>
     * If it is simple creation, it creates a work object with name given by user. If it is mass creation, it creates a
     * list of constituents or categories depending on parameters given by user (start index, end index and step).
     * 
     * @param event
     *            Work object creation requested event.
     */
    private void onWorkObjectCreationRequested(AppEvent event)
    {
        CreationData data = event.getData();
        if (Util.isEmptyString(data.getName()))
        {
            EventDispatcher.forwardEvent(CommonEvents.ERROR);
            return;
        }
        final RecordModelData parent = (RecordModelData) CollectionUtils.getFirst(this.view.getTreeSelection());

        if (!data.getMassCreation())
        {
            this.onSimpleCreation(data, parent);
        }
        else
        {
            this.onMassCreation(data, parent);
        }
    }

    /**
     * Creates a list of constituents or categories depending on parameters given by user : start index, end index and
     * step. <br>
     * Ex : If class = Constituent, name = myConstituent, start index = 1, end index = 10 and step = 1. It will create
     * myConstituent 01, myConstituent 02, ... myConstituent 10.
     * 
     * @param data
     *            Data contains parameters : name, simple class name, start index, end index.
     * @param parent
     *            Parent under which instances will be created.
     * 
     */
    private void onMassCreation(CreationData data, RecordModelData parent)
    {
        List<Hd3dModelData> records = new ArrayList<Hd3dModelData>();
        int nbDigits = data.getEndIndex().toString().length();

        if (FieldUtils.isConstituent(data.getSimpleClassName()))
        {
            for (int i = data.getStartIndex(); i <= data.getEndIndex(); i = i + data.getStep())
            {
                String name = data.getName() + FieldUtils.getDigitNumber(i, nbDigits);
                records.add(this.getNewConstituent(name, parent));
            }
        }
        else if (FieldUtils.isCategory(data.getSimpleClassName()))
        {
            for (int i = data.getStartIndex(); i <= data.getEndIndex(); i = i + data.getStep())
            {
                String name = data.getName() + FieldUtils.getDigitNumber(i, nbDigits);
                records.add(this.getNewCategory(name, parent));
            }
        }

        AppEvent event = new AppEvent(BreakdownEvents.CONSTITUENT_MASS_CREATION_SUCCESS);
        event.setData(BreakdownConfig.PARENT_EVENT_VAR_NAME, parent);
        BulkRequests.bulkRangePost(records, event, 50);
    }

    /**
     * On simple creation, a new constituent or category is created with given name under parent node.
     * 
     * @param data
     *            Data contains name and class name for new instance.
     * @param parent
     *            It is the parent inside tree of the future instance.
     */
    private void onSimpleCreation(CreationData data, final RecordModelData parent)
    {
        if (FieldUtils.isConstituent(data.getSimpleClassName()))
        {
            final ConstituentModelData constituent = this.getNewConstituent(data.getName(), parent);

            constituent.save(new PostModelDataCallback(constituent, null) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    this.setNewId(response);
                    model.getTreeStore().add(parent, constituent, false);
                    view.display("Creation succeeds", "Constituent " + constituent.getName() + " created.");
                    view.enableCreationDialog();
                    view.expandParent(parent);
                }
            });
        }
        else if (FieldUtils.isCategory(data.getSimpleClassName()))
        {
            final CategoryModelData category = this.getNewCategory(data.getName(), parent);

            category.save(new PostModelDataCallback(category, null) {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    this.setNewId(response);
                    model.getTreeStore().add(parent, category, false);
                    view.display("Creation succeeds", "Constituent " + category.getName() + " created.");
                    view.enableCreationDialog();
                    view.expandParent(parent);
                }
            });
        }
    }

    /**
     * When create constituent is clicked, the creation dialog is displayed for constituent.
     * 
     * @param event
     *            Constituent Create Clicked.
     */
    private void onConstituentCreateClicked(AppEvent event)
    {
        this.view.displayCreationDialog(ConstituentModelData.SIMPLE_CLASS_NAME, BreakdownEvents.CONSTITUENT_CREATED,
                "Create constituents");
    }

    /**
     * When create category is clicked, the creation dialog is displayed for constituent.
     * 
     * @param event
     *            Category Create Clicked.
     */
    private void onCategoryCreateClicked(AppEvent event)
    {
        this.view.displayCreationDialog(CategoryModelData.SIMPLE_CLASS_NAME, BreakdownEvents.CONSTITUENT_CREATED,
                "Create categories");
    }

    /**
     * When mass creation succeeds, It reloads tree node where creation is occurred. Creation dialog is reenabled.
     * 
     * @param event
     *            The mass creations success event.
     */
    private void onConstituentMassCreationSuccess(AppEvent event)
    {
        RecordModelData parent = event.getData(BreakdownConfig.PARENT_EVENT_VAR_NAME);
        this.model.getTreeStore().getLoader().loadChildren(parent);
        view.display("Creation succeeds", "Mass creation worked correctly");

        this.view.enableCreationDialog();
    }

    /**
     * When constituent rename is clicked, the rename dialog is displayed.
     */
    private void onConstituentRenameClicked()
    {
        List<RecordModelData> constituents = this.view.getTreeSelection();
        if (CollectionUtils.isNotEmpty(constituents))
        {
            this.view.showRenameDialog(constituents.get(0), BreakdownEvents.CONSTITUENT_RENAMED);
        }
    }

    /**
     * When constituent is renamed remotely, the tree is updated to display the new name.
     * 
     * @param event
     *            Constituent renamed event.
     */
    private void onConstituentRenamed(AppEvent event)
    {
        RecordModelData record = event.getData();
        this.model.getTreeStore().update(record);
    }

    /**
     * When a selection is dropped, it updates parent and category fields with id of category on which drop is
     * occurring.
     * 
     * @param event
     *            The selection dropped event.
     */
    private void onSelectionDropped(AppEvent event)
    {
        RecordModelData parent = event.getData(BreakdownConfig.DND_PARENT_EVENT_VAR_NAME);
        List<Hd3dModelData> constituents = event.getData(BreakdownConfig.DND_CONSTITUENT_EVENT_VAR_NAME);
        List<Hd3dModelData> categories = event.getData(BreakdownConfig.DND_CATEGORY_EVENT_VAR_NAME);

        for (Hd3dModelData constituent : constituents)
        {
            constituent.set(ConstituentModelData.CONSTITUENT_CATEGORY, parent.getId());
        }

        for (Hd3dModelData category : categories)
        {
            Long parentId = parent.getId();
            if (parent.getId() == -1L)
                parentId = MainModel.currentProject.getRootCategoryId();
            category.set(CategoryModelData.CATEGORY_PARENT, parentId);
        }

        if (constituents.size() > 0)
            BulkRequests.bulkPut(constituents, CommonEvents.MODEL_DATA_UPDATE_SUCCESS);
        if (categories.size() > 0)
            BulkRequests.bulkPut(categories, CommonEvents.MODEL_DATA_UPDATE_SUCCESS);
    }

    /**
     * When category is deleted it shows a category dialog box.
     * 
     * @param event
     *            Constituent deleted clicked.
     */
    private void onConstituentCategoryDeleteClicked(AppEvent event)
    {
        this.view.showCategoryDeleteConfirmation();
    }

    /**
     * When user confirm category deletion, the category is remotely deleted and removed from the navigation tree.
     * 
     * @param event
     *            Category deletion confirmed.
     */
    private void onConstituentCategoryDeleteConfirmed(AppEvent event)
    {
        List<RecordModelData> categories = this.view.getTreeSelection();
        for (RecordModelData category : categories)
        {
            if (FieldUtils.isCategory(category))
            {
                category.delete();
                this.model.getTreeStore().remove(category);
            }
        }
    }

    /**
     * When constituent is deleted it shows a confirmation dialog box.
     * 
     * @param event
     *            Constituent deleted clicked.
     */
    private void onConstituentDeleteClicked(AppEvent event)
    {
        this.view.showConstituentDeleteConfirmation();
    }

    /**
     * When user confirm constituent deletion, the constituent is remotely deleted and removed from the navigation tree.
     * 
     * @param event
     *            Constituent deleted confirmed.
     */
    private void onConstituentDeleteConfirmed(AppEvent event)
    {
        List<RecordModelData> constituents = this.view.getTreeSelection();
        for (RecordModelData constituent : constituents)
        {
            if (FieldUtils.isConstituent(constituent))
            {
                constituent.delete();
                this.model.getTreeStore().remove(constituent);
            }
        }
    }

    /**
     * If error occurs, saving indicator is hidden and creation dialog is enabled.
     */
    @Override
    protected void onError(AppEvent event)
    {
        super.onError(event);
        this.view.enableTree();
        this.view.enableCreationDialog();
    }

    /**
     * Make a new constituent with correct default path.
     * 
     * @param name
     *            The constituent name.
     * @param parent
     *            The constituent parent inside tree.
     */
    private ConstituentModelData getNewConstituent(String name, RecordModelData parent)
    {
        ConstituentModelData constituent = new ConstituentModelData();
        constituent.setCategory(parent.getId());
        constituent.setLabel(name);
        constituent.setName(name);
        constituent.setHook(name.toLowerCase().replace(" ", "_"));
        constituent.setDefaultPath(MainModel.currentProject.getId(), parent.getId());

        return constituent;
    }

    /**
     * Make a new category with correct default path.
     * 
     * @param name
     *            The category name.
     * @param parent
     *            The category parent inside tree.
     */
    private CategoryModelData getNewCategory(String name, RecordModelData parent)
    {
        CategoryModelData category = new CategoryModelData();
        if (parent != null)
            category.setParent(parent.getId());
        category.setName(name);
        category.setProject(MainModel.currentProject.getId());

        return category;
    }

    @Override
    protected void onExplorerDataLoaded(AppEvent event) 
    {
        super.onExplorerDataLoaded(event);
    	this.view.enableTree();
	}

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.PROJECT_CHANGED);
        this.registerEventTypes(CommonEvents.CONSTITUENTS_SELECTED);
        this.registerEventTypes(CommonEvents.CATEGORIES_SELECTED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_RENAME_CLICKED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_RENAMED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_DELETE_CLICKED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_DELETE_CONFIRMED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_CATEGORY_DELETE_CLICKED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_CATEGORY_DELETE_CONFIRMED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_CREATE_CLICKED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_CREATE_CATEGORY_CLICKED);
        this.registerEventTypes(BreakdownEvents.WORK_OBJECT_CREATION_REQUESTED);
        this.registerEventTypes(BreakdownEvents.CONSTITUENT_MASS_CREATION_SUCCESS);
        this.registerEventTypes(BreakdownEvents.SELECTION_DROPPED);

        this.registerEventTypes(CommonEvents.ERROR);
    }
}
