package fr.hd3d.breakdown.ui.client.model;

public class CreationData
{
    private String name;
    private Boolean massCreation;
    private Integer startIndex;
    private Integer endIndex;
    private Integer step;
    private String simpleClassName;

    public String getName()
    {
        return name;
    }

    public Boolean getMassCreation()
    {
        return massCreation;
    }

    public Integer getStartIndex()
    {
        return startIndex;
    }

    public Integer getEndIndex()
    {
        return endIndex;
    }

    public Integer getStep()
    {
        return step;
    }

    public String getSimpleClassName()
    {
        return simpleClassName;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setMassCreation(Boolean massCreation)
    {
        this.massCreation = massCreation;
    }

    public void setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;
    }

    public void setEndIndex(Integer endIndex)
    {
        this.endIndex = endIndex;
    }

    public void setStep(Integer step)
    {
        this.step = step;
    }

    public void setSimpleClassName(String simpleClassName)
    {
        this.simpleClassName = simpleClassName;
    }

}
