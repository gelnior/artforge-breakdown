package fr.hd3d.breakdown.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.breakdown.ui.client.event.CastingEvents;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.service.store.AllLoadListener;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;


/**
 * Casting model handles casting data. This model containing categories, shots and composition from a project. Its allow
 * to manipulate compositions and castings.
 * 
 * @author HD3D
 * 
 */
public class CastingSequenceModel
{

    /** Store containing shots from a sequence or from a project. */
    private final ServiceStore<ShotModelData> shotStore = new ServiceStore<ShotModelData>(ReaderFactory.getShotReader());
    /** Store containing compositions. */
    private final ServiceStore<CompositionModelData> compositionStore = new ServiceStore<CompositionModelData>(
            ReaderFactory.getCompositionReader());
    /** Store containing casting data. */
    private final CastingStore castingStore = new CastingStore();

    /** LuceneConstraint to reload all shots from list of sequences ( included the sub sequences ). */
    private final LuceneConstraint sequenceConstraintId = new LuceneConstraint(EConstraintOperator.in, "sequences_id");

    /** Shot navigation store tree. */
    private BaseTreeDataModel<ShotModelData, SequenceModelData> shotNavModel;
    /** Constituent navigation store tree. */
    private BaseTreeDataModel<ConstituentModelData, CategoryModelData> constituentNavModel;

    /** Store containing root categories from a project. */
    private final ServiceStore<CategoryModelData> categoriesRootStore = new ServiceStore<CategoryModelData>(
            ReaderFactory.getCategoryReader());

    /** Store containing all categories from a project. */
    private final ServiceStore<CategoryModelData> categoriesStore = new ServiceStore<CategoryModelData>(
            ReaderFactory.getCategoryReader());

    /** Constraint for composition store to retrieve all shots for specific shots. */
    private final Constraint shotIdsConstraint;

    /** Map containing the constituents id sort by shot id. */
    private final FastMap<ArrayList<CompositionModelData>> compositionMapByShotId = new FastMap<ArrayList<CompositionModelData>>();

    /** Containing the current project selected. */
    private ProjectModelData selectedProject = null;
    /** Containing the current sequence selected. */
    private ArrayList<SequenceModelData> selectedSequences = new ArrayList<SequenceModelData>();

    /** Map containing the categories sort by category id. */
    private final FastMap<CategoryModelData> categoriesMapByCategoryId = new FastMap<CategoryModelData>();

    /**
     * Default constructor.
     */
    public CastingSequenceModel()
    {
        categoriesStore.addEventLoadListener(CastingEvents.CATEGORIES_LOADED);
        categoriesRootStore.addEventLoadListener(CastingEvents.CATEGORIES_ROOT_LOADED);

        shotStore.addLoadListener(new AllLoadListener<ShotModelData>(shotStore, CastingEvents.SHOT_LOADED));

        compositionStore.addLoadListener(new AllLoadListener<CompositionModelData>(compositionStore,
                CastingEvents.COMPOSITION_LOADED));

        shotIdsConstraint = new Constraint(EConstraintOperator.in, "shot.id");

        castingStore.groupBy(CastingModelData.SEQUENCES_NAMES);
    }

    /**
     * @return the composition store containing compositions from a specific shots.
     */
    public ServiceStore<CompositionModelData> getCompositionStore()
    {
        return compositionStore;
    }

    /**
     * 
     * @return the root category store containing categories from a specific project.
     */
    public ServiceStore<CategoryModelData> getCategoriesRootStore()
    {
        return categoriesRootStore;
    }

    /**
     * 
     * @return the category store containing categories from a specific project.
     */
    public ServiceStore<CategoryModelData> getCategoriesStore()
    {
        return categoriesStore;
    }

    /**
     * 
     * @return the shot store containing shots from a specific project or a specific sequence.
     */
    public ServiceStore<ShotModelData> getShotStore()
    {
        return shotStore;
    }

    /**
     * 
     * @return the casting store containing constituents linked to the specific shot.
     */
    public BaseStore<CastingModelData> getCastingStore()
    {
        return castingStore;
    }

    /**
     * Reinitialize the data with the new current project.
     * 
     * @param project
     *            the new current project.
     */
    public void initialize(ProjectModelData project)
    {
        if (project == null)
        {
            return;
        }
        selectedProject = project;
        selectedSequences = new ArrayList<SequenceModelData>();

        shotStore.removeAll();
        compositionStore.removeAll();
        castingStore.removeAll();

        shotStore.setPath(project.getDefaultPath() + "/" + ServicesPath.SHOTS);
        compositionStore.setPath(project.getDefaultPath() + "/" + ServicesPath.COMPOSITIONS);

        loadCategories(project);

        if (shotNavModel != null && constituentNavModel != null)
        {
            this.shotNavModel.setCurrentProject(project);
            this.constituentNavModel.setCurrentProject(project);
        }
    }

    public void loadCategories(ProjectModelData project)
    {
        String path = ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.CATEGORIES + ServicesPath.ALL;
        categoriesStore.setPath(path);
        categoriesStore.reload();

        path = ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.CATEGORIES;
        categoriesRootStore.setPath(path);
        categoriesRootStore.reload();
    }

    /**
     * Set the data in the casting store.
     */
    public void setCastingStore()
    {
        castingStore.removeAll();

        if (shotStore.getCount() == 0)
        {
            return;
        }

        ArrayList<CastingModelData> castingList = new ArrayList<CastingModelData>();
        for (ShotModelData shot : shotStore.getModels())
        {
            CastingModelData casting = new CastingModelData(categoriesMapByCategoryId);
            casting.setShot(shot);
            ArrayList<CompositionModelData> compositionList = compositionMapByShotId.get(shot.getId().toString());
            if (compositionList != null)
            {

                casting.initialize(compositionList);

            }
            castingList.add(casting);
        }
        castingStore.add(castingList);
        castingStore.sort(CastingModelData.SHOT_NAME, SortDir.ASC);
    }

    /**
     * Reload the composition store.
     */
    public void reloadCompositionStore()
    {
        ArrayList<Long> shotIdsList = new ArrayList<Long>();
        for (ShotModelData shot : shotStore.getModels())
        {
            shotIdsList.add(shot.getId());
        }
        if (!shotIdsList.isEmpty())
        {
            compositionStore.clearParameters();
            shotIdsConstraint.setLeftMember(shotIdsList);
            compositionStore.addParameter(shotIdsConstraint);
            compositionStore.reload();
        }
    }

    /**
     * Update the composition map with the data store and set casting store.
     */
    public void updateCompositionMap()
    {
        compositionMapByShotId.clear();
        for (CompositionModelData composition : compositionStore.getModels())
        {
            ArrayList<CompositionModelData> list = compositionMapByShotId.get(composition.getShot().toString());
            if (list == null)
            {
                list = new ArrayList<CompositionModelData>();
                compositionMapByShotId.put(composition.getShot().toString(), list);
            }
            list.add(composition);
        }
    }

    /**
     * Register to casting model, the constituent navigation tree model.
     * 
     * @param dataModel
     *            The model to set.
     */
    public void setConstituentNavModel(BaseTreeDataModel<ConstituentModelData, CategoryModelData> dataModel)
    {
        this.constituentNavModel = dataModel;
    }

    /**
     * Register to casting model, the shot navigation tree model.
     * 
     * @param dataModel
     *            The model to set.
     */
    public void setShotNavModel(BaseTreeDataModel<ShotModelData, SequenceModelData> dataModel)
    {
        this.shotNavModel = dataModel;
    }

    /**
     * Set the current selected sequence and reload the data.
     * 
     * @param sequences
     *            the selected sequence.
     */
    public void setSelectedSequences(List<SequenceModelData> sequences)
    {
        selectedSequences.clear();
        selectedSequences.addAll(sequences);
    }

    public void loadShots()
    {
        if (selectedSequences.isEmpty())
        {
            initialize(selectedProject);
        }
        else
        {
            shotStore.removeParameter(sequenceConstraintId);
            ArrayList<String> idList = new ArrayList<String>();
            for (SequenceModelData sequence : selectedSequences)
            {
                idList.add(sequence.getId().toString());
            }
            sequenceConstraintId.setLeftMember(idList);
            shotStore.addParameter(sequenceConstraintId);
            shotStore.reload();
        }
    }

    /**
     * Add compositions to the casting at index position in the casting store.
     * 
     * @param index
     *            the position of casting
     * @param compositionList
     *            the compositions
     */
    public void insert(int index, List<CompositionModelData> compositionList, EventType event)
    {
        CastingModelData casting = castingStore.getAt(index);
        casting.addCompositions(compositionList, event);
    }

    /**
     * Remove all compositions of category to the casting at index position in the casting store.
     * 
     * @param categoryId
     *            the category id
     * @param index
     *            the position of casting
     */
    public boolean remove(String categoryId, int index, EventType event)
    {
        CastingModelData casting = castingStore.getAt(index);
        return casting.removeCompositions(categoryId, event);
    }

    /**
     * Return the casting containing a given composition.
     * 
     * @param composition
     *            the given composition
     * @return the casting null if not found
     */
    public CastingModelData getCasting(CompositionModelData composition)
    {
        if (composition == null)
        {
            return null;
        }

        for (CastingModelData casting : castingStore.getModels())
        {
            if (composition.getShot().longValue() == casting.getShotId().longValue())
            {

                return casting;
            }
        }
        return null;
    }

    /**
     * Update the category map with the data store.
     */
    public void updateCategoriesMap()
    {
        categoriesMapByCategoryId.clear();
        for (CategoryModelData category : categoriesStore.getModels())
        {
            categoriesMapByCategoryId.put(category.getId().toString(), category);
        }
    }

    /**
     * Return the shot with the given id.
     * 
     * @param shotId
     *            the id.
     * @return the shot or null if not find.
     */
    public ShotModelData getShot(Long shotId)
    {
        if (shotId == null)
        {
            return null;
        }
        for (ShotModelData shot : this.shotStore.getModels())
        {
            if (shot.getId().longValue() == shotId.longValue())
            {
                return shot;
            }
        }

        return null;
    }

    /**
     * Return the casting with the given shot id.
     * 
     * @param shotId
     *            the shot id.
     * @return the casting or null if not find.
     */
    public CastingModelData getCasting(Long shotId)
    {
        if (shotId == null)
        {
            return null;
        }
        for (CastingModelData casting : this.castingStore.getModels())
        {
            if (casting.getShotId().longValue() == shotId.longValue())
            {
                return casting;
            }
        }

        return null;
    }

    /**
     * Return the current sequences
     * 
     * @return the sequences
     */
    public List<SequenceModelData> getSelectedSequences()
    {
        return selectedSequences;
    }

    /**
     * Return the current project
     * 
     * @return the project
     */
    public ProjectModelData getSelectedProject()
    {
        return selectedProject;
    }
}
