package fr.hd3d.breakdown.ui.client.model;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;


/**
 * Model handling breakdown view data (main model inheritance). It handles two variables : is project changed to store
 * the fact that project has changed and current entity to always know if the user is working with shots or
 * constituents.
 * 
 * @author HD3D
 */
public class BreakdownModel extends MainModel
{
    /** Name of configuration file needed by sheet editor. */
    private static final String DATA_TYPE_FILE = "config/items_info_breakdown.json";

    /** True if project has changed recently (useful to know if some widgets should be refreshed). */
    private Boolean isProjectChanged = false;
    /** Current entity is used to determine current active tabulation. */
    private String currentEntity = ConstituentModelData.SIMPLE_CLASS_NAME;

    /**
     * Default constructor : configure sheet editor.
     */
    public BreakdownModel()
    {
        SheetEditorModel.addDataFile(DATA_TYPE_FILE);
        SheetEditorModel.setType(ESheetType.BREAKDOWN);
    }

    /**
     * @return if project has changed recently (useful to know if some widgets should be refreshed).
     */
    public Boolean isProjectChanged()
    {
        return isProjectChanged;
    }

    /**
     * Set if project has changed recently.
     * 
     * @param isProjectChanged
     *            The new value for isProjectChanged field.
     */
    public void setIsProjectChanged(Boolean isProjectChanged)
    {
        this.isProjectChanged = isProjectChanged;
    }

    /**
     * @return Currently active tab.
     */
    public String getCurrentEntity()
    {
        return this.currentEntity;
    }

    /**
     * @param currentEntity
     *            Set currently active tab.
     */
    public void setCurrentEntity(String currentEntity)
    {
        this.currentEntity = currentEntity;
    }

}
