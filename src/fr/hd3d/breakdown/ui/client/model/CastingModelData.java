package fr.hd3d.breakdown.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


/**
 * Class to manipulate data of casting to a shot. It adds and removes compositions.
 * 
 * @author HD3D
 * 
 */
public class CastingModelData extends Hd3dModelData
{

    private static final long serialVersionUID = 6784105897633013765L;

    public static String SHOT_NAME = "shot_name";
    public static String SEQUENCES_NAMES = "sequences_names";
    public static String SHOT_ID = "shot_id";
    public static String SHOT_DESCRIPTION = "shot_description";
    public static String SHOT_NBFRAME = "shot_nbframe";
    public static String SHOT_STARTFRAME = "shot_startframe";
    public static String SHOT_ENDFRAME = "shot_endframe";

    public static String CLASS_NAME = "Casting";

    public static String NB_RECORDS_REMAIN = "nb_records_remain";

    /** Shot corresponding to casting. */
    private ShotModelData shot;
    /** Map containing compositions sort by category id and contituent id. */
    private final FastMap<FastMap<CompositionModelData>> compositionMapByConstituentIdByCategoryId = new FastMap<FastMap<CompositionModelData>>();

    /** Map containing category sort by category id. */
    private final FastMap<CategoryModelData> categoriesMapByCategoryId;

    /**
     * Default Constructor.
     * 
     * @param categoriesMapByCategoryId
     *            the map containing all category from a project.
     */
    public CastingModelData(FastMap<CategoryModelData> categoriesMapByCategoryId)
    {
        this.categoriesMapByCategoryId = categoriesMapByCategoryId;
        setSimpleClassName(ShotModelData.SIMPLE_CLASS_NAME);
    }

    /**
     * Set shot id, shot name and sequence name from a specific shot.
     * 
     * @param shot
     *            the specific shot.
     */
    public void setShot(ShotModelData shot)
    {
        if (shot == null)
        {
            return;
        }
        this.shot = shot;
        set(SHOT_ID, shot.getId());
        setId(shot.getId());
        set(SHOT_NAME, shot.getName());
        set(SHOT_DESCRIPTION, shot.getDescription());
        set(SHOT_NBFRAME, shot.getNbFrame());
        set(SHOT_STARTFRAME, shot.getStartFrame());
        set(SHOT_ENDFRAME, shot.getEndFrame());

        set(SEQUENCES_NAMES, shot.getSequencesNames());
    }

    /**
     * Initialize data with a list of compositions.
     * 
     * @param compositionList
     *            the list of compositions.
     */
    public void initialize(ArrayList<CompositionModelData> compositionList)
    {
        if (compositionList == null)
        {
            Logger.log("CastingModelData - initialize : The composition list is null");
            return;
        }

        for (CompositionModelData composition : compositionList)
        {
            addCompositionMap(composition);
        }
    }

    /**
     * Add a composition to the map. If the composition will already exist, incremente the number of occurences.
     * 
     * @param compositionToAdd
     */
    private void addCompositionMap(CompositionModelData compositionToAdd)
    {
        if (compositionToAdd == null)
        {
            Logger.log("CastingModelData - addCompositionMap : The composition is null");
            return;
        }

        String categoryId = getRootCategory(compositionToAdd.getCategoryId().toString());
        if (categoryId == null)
        {
            return;
        }
        compositionToAdd.setCategoryId(Long.parseLong(categoryId));

        FastMap<CompositionModelData> compositionMapByConstituentId = compositionMapByConstituentIdByCategoryId
                .get(categoryId);
        if (compositionMapByConstituentId == null)
        {
            compositionMapByConstituentId = new FastMap<CompositionModelData>();
            compositionMapByConstituentIdByCategoryId.put(categoryId, compositionMapByConstituentId);
        }

        CompositionModelData composition = compositionMapByConstituentId.get(compositionToAdd.getConstituent()
                .toString());
        if (composition == null)
        {
            compositionMapByConstituentId.put(compositionToAdd.getConstituent().toString(), compositionToAdd);
        }
        else
        {
            composition.setOcc(composition.getOcc() + 1);
        }
    }

    /**
     * Add a composition from a constituent. If a composition with the constituent will already exist, increment the
     * number of occurences else create a new composition.
     * 
     * @param constituent
     *            the constituent
     */
    public void addComposition(ConstituentModelData constituent, EventType event)
    {
        if (constituent == null)
        {
            Logger.warn("CastingModelData - getComposition : The constituent is null");
            return;
        }

        String categoryId = getRootCategory(constituent.getCategory().toString());
        if (categoryId == null)
        {
            return;
        }

        CompositionModelData composition = getComposition(categoryId, constituent.getId().toString());
        if (composition == null)
        {
            composition = new CompositionModelData(constituent, shot);
            if (composition != null)
            {
                addCompositionMap(composition);
            }
        }
        else
        {
            composition.setOcc(composition.getOcc() + 1);
        }

        AppEvent appEvent = new AppEvent(event);
        appEvent.setData(CompositionModelData.CLASS_NAME, composition);
        if (composition.getId() == null)
            composition.setDefaultPath(constituent.getDefaultPath() + "/" + ServicesPath.COMPOSITIONS);
        composition.save(appEvent);
    }

    /**
     * Add the list of compositions. If the composition will already exist, addition the number of occurence else copy
     * the composition.
     * 
     * @param compositionsCopy
     *            the list of composition to add
     */
    public void addCompositions(List<CompositionModelData> compositionsCopy, EventType event)
    {
        if (compositionsCopy == null)
        {
            Logger.warn("CastingModelData - addCompositions : the data to copy is null");
            return;
        }

        int size = compositionsCopy.size();
        for (CompositionModelData compositionCopy : compositionsCopy)
        {
            CompositionModelData composition = getComposition(compositionCopy);
            if (composition == null)
            {
                composition = CompositionModelData.copyFrom(compositionCopy, shot);
                if (composition == null)
                {
                    return;
                }

                addCompositionMap(composition);
            }
            else
            {
                composition.setOcc(composition.getOcc() + compositionCopy.getOcc());
            }

            AppEvent appEvent = new AppEvent(event);
            appEvent.setData(CompositionModelData.CLASS_NAME, composition);
            appEvent.setData(CastingModelData.NB_RECORDS_REMAIN, --size);
            composition.save(appEvent);
        }
    }

    /**
     * Return the composition if the casting will contain this composition.
     * 
     * @param composition
     *            the composition
     * @return the composition
     */
    private CompositionModelData getComposition(CompositionModelData composition)
    {
        if (composition == null)
        {
            Logger.log("CastingModelData - getComposition : The composition is null");
            return null;
        }

        return getComposition(composition.getCategoryId().toString(), composition.getConstituent().toString());
    }

    /**
     * Return the composition if the casting will contain this composition.
     * 
     * @param categoryId
     *            the category id
     * @param constituentId
     *            the constituent id
     * @return the composition
     */
    private CompositionModelData getComposition(String categoryId, String constituentId)
    {
        categoryId = getRootCategory(categoryId);
        if (categoryId == null)
        {
            return null;
        }
        FastMap<CompositionModelData> compositionMapByConstituentId = compositionMapByConstituentIdByCategoryId
                .get(categoryId);
        if (compositionMapByConstituentId == null)
        {
            return null;
        }

        return compositionMapByConstituentId.get(constituentId);
    }

    /**
     * Return the list of compositions of category id. The category id have to a category of first level.
     * 
     * @param categoryId
     *            the category id
     * @return the list of compositions
     */
    public List<CompositionModelData> getCompositions(String categoryId)
    {
        FastMap<CompositionModelData> compositionMap = compositionMapByConstituentIdByCategoryId.get(categoryId);
        if (compositionMap == null)
        {
            return new ArrayList<CompositionModelData>();
        }

        ArrayList<CompositionModelData> compositionList = new ArrayList<CompositionModelData>();
        for (CompositionModelData composition : compositionMap.values())
        {
            compositionList.add(composition);
        }

        return compositionList;
    }

    /**
     * Remove the compositions from category id.
     * 
     * @param categoryId
     *            the category id.
     * @return if remove something.
     */
    public boolean removeCompositions(String categoryId, EventType event)
    {
        List<CompositionModelData> compositionList = getCompositions(categoryId);
        if (compositionList == null || compositionList.isEmpty())
        {
            Logger.log("CastingModelData - removeCompositions : the category don't contain compositions");
            return false;
        }

        int size = compositionList.size();
        for (CompositionModelData composition : compositionList)
        {
            AppEvent appEvent = new AppEvent(event);
            appEvent.setData(CompositionModelData.CLASS_NAME, composition);
            appEvent.setData(CastingModelData.NB_RECORDS_REMAIN, --size);
            composition.delete(appEvent);
        }
        compositionMapByConstituentIdByCategoryId.remove(categoryId);

        return true;
    }

    /**
     * Return the shot id.
     * 
     * @return the shot id.
     */
    public Long getShotId()
    {
        return (Long) get(SHOT_ID);
    }

    /**
     * Remove a composition from a composition.
     * 
     * @param compositionToRemove
     *            the composition to remove.
     */
    public void removeComposition(CompositionModelData compositionToRemove)
    {
        FastMap<CompositionModelData> compositionMap = compositionMapByConstituentIdByCategoryId
                .get(compositionToRemove.getCategoryId().toString());
        if (compositionMap == null)
        {
            return;
        }

        CompositionModelData composition = compositionMap.get(compositionToRemove.getConstituent().toString());
        if (composition == null)
        {
            return;
        }

        compositionMap.remove(compositionToRemove.getConstituent().toString());
        composition.delete();
    }

    /**
     * Return the category id of first level.
     * 
     * @param categoryId
     *            category id of inferior level.
     * @return the category id
     */
    private String getRootCategory(String categoryId)
    {
        CategoryModelData category = null;
        String id = categoryId;
        String categoryRoot = null;
        while ((category = categoriesMapByCategoryId.get(id)) != null)
        {
            if (category.getParent() != null && category.getParent().longValue() != category.getId().longValue())
            {
                // garde l'id d'avant car category root non reel
                categoryRoot = id;
                id = category.getParent().toString();
            }
            else
            {
                break;
            }
        }
        return categoryRoot;
    }

    /**
     * Return the description of the shot.
     * 
     * @return the description
     */
    public String getShotDescription()
    {
        return get(SHOT_DESCRIPTION);
    }

    /**
     * Set the description of shot.
     * 
     * @param description
     *            the shot description.
     */
    public void setShotDescriptin(String description)
    {
        set(SHOT_DESCRIPTION, description);
    }

    /**
     * Return the number of frames of the shot.
     * 
     * @return the number of frames
     */
    public Integer getShotNbFrame()
    {
        return get(SHOT_NBFRAME);
    }

    /**
     * Set the number of frames.
     * 
     * @param nbframe
     *            the number of frames.
     */
    public void setShotNbFrame(Integer nbframe)
    {
        set(SHOT_NBFRAME, nbframe);
    }

    /**
     * Set the start frame.
     * 
     * @param startframe
     *            the start frame.
     */
    public void setShotStartFrame(Integer startframe)
    {
        set(SHOT_STARTFRAME, startframe);
    }

    /**
     * Set the end frame.
     * 
     * @param endframe
     *            the end frame.
     */
    public void setShotEndFrame(Integer endframe)
    {
        set(SHOT_ENDFRAME, endframe);
    }

    /**
     * Return the start frame of the shot.
     * 
     * @return the start frame
     */
    public Integer getShotStartFrame()
    {
        return get(SHOT_STARTFRAME);
    }

    /**
     * Return the end frame of the shot.
     * 
     * @return the end frame
     */
    public Integer getShotEndFrame()
    {
        return get(SHOT_ENDFRAME);
    }

    /**
     * Return the name of the shot.
     * 
     * @return the shot name
     */
    public String getShotName()
    {
        return get(SHOT_NAME);
    }

    /**
     * Return the name of the sequence of the shot.
     * 
     * @return the sequence name
     */
    public String getSequenceName()
    {
        return get(SEQUENCES_NAMES);
    }
}
