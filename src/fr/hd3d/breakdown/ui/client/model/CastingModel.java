package fr.hd3d.breakdown.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.breakdown.ui.client.enums.ECastingMode;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.CompositionReader;
import fr.hd3d.common.ui.client.modeldata.reader.ConstituentReader;
import fr.hd3d.common.ui.client.modeldata.reader.ShotReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;


/**
 * Casting model handle casting data.
 * 
 * @author HD3D
 */
public class CastingModel
{
    /** Store containing constituent linked to a specific shot. */
    private final ServiceStore<ConstituentModelData> constituentStore = new ServiceStore<ConstituentModelData>(
            new ConstituentReader());
    /** Store containing shot linked to a specific constituent. */
    private final ServiceStore<ShotModelData> shotStore = new ServiceStore<ShotModelData>(new ShotReader());
    /** Store containing compositions. */
    private final ServiceStore<CompositionModelData> compoStore = new ServiceStore<CompositionModelData>(
            new CompositionReader());

    /** Constraint for composition store to retrieve all compositions for a specific shot. */
    private final EqConstraint shotConstraint = new EqConstraint(CompositionModelData.SHOT_ID_FIELD);
    /** Constraint for composition store to retrieve all compositions for a specific constituent. */
    private final EqConstraint constituentConstraint = new EqConstraint(CompositionModelData.CONSTITUENT_ID_FIELD);

    /** Map containing store used by grids. */
    private final FastMap<ServiceStore<?>> storeMap = new FastMap<ServiceStore<?>>();

    /** Shot navigation store tree. */
    private BaseTreeDataModel<ShotModelData, SequenceModelData> shotNavModel;
    /** Constituent navigation store tree. */
    private BaseTreeDataModel<ConstituentModelData, CategoryModelData> constituentNavModel;

    /** Last selected shot. */
    private ShotModelData selectedShot;
    /** Last selected constituent. */
    private ConstituentModelData selectedConsituent;

    /** Tell in which mode the casting is : shots on which constituent appears or constituent that appears on shot. */
    private ECastingMode currentMode = ECastingMode.SHOT;

    /** Type of data loaded. */
    private String loadingEntity;

    /** Listener used to load constituent or shot when compositions are loaded. */
    private final LoadListener loadListener = new LoadListener() {
        @Override
        public void loaderLoad(LoadEvent le)
        {
            onCompositionsLoaded();
        }
    };

    /**
     * Constructor : configure stores.
     */
    public CastingModel()
    {
        storeMap.put(ShotModelData.SIMPLE_CLASS_NAME, shotStore);
        storeMap.put(ConstituentModelData.SIMPLE_CLASS_NAME, constituentStore);

        shotStore.groupBy(ShotModelData.SHOT_SEQUENCESNAMES);
        constituentStore.groupBy(ConstituentModelData.CONSTITUENT_CATEGORIESNAMES);

        compoStore.addLoadListener(loadListener);
    }

    /** @return in which mode the casting is : shots on which constituent appears or constituent that appears on shot. */
    public ECastingMode getCurrentMode()
    {
        return this.currentMode;
    }

    /**
     * Set in which mode the casting is : shots on which constituent appears or constituent that appears on shot.
     * 
     * @param mode
     *            The mode set.
     */
    public void setCurrentMode(ECastingMode mode)
    {
        this.currentMode = mode;
    }

    /**
     * @return Store containing constituent linked to a specific shot.
     */
    public BaseStore<ConstituentModelData> getConstituentStore()
    {
        return constituentStore;
    }

    /**
     * @return Store containing shot linked to a specific constituent.
     */
    public BaseStore<ShotModelData> getShotStore()
    {
        return shotStore;
    }

    /**
     * @return Last selected shot.
     */
    public ShotModelData getSelectedShot()
    {
        return this.selectedShot;
    }

    /**
     * Set last selected shot.
     * 
     * @param shot
     *            the shot to set.
     */
    public void setSelectedShot(ShotModelData shot)
    {
        this.selectedShot = shot;
    }

    /**
     * @return Last selected constituent.
     */
    public ConstituentModelData getSelectedConstituent()
    {
        return this.selectedConsituent;
    }

    /**
     * Set last selected constituent.
     * 
     * @param constituent
     *            the constituent to set.
     */
    public void setSelectedConstituent(ConstituentModelData constituent)
    {
        this.selectedConsituent = constituent;
    }

    /**
     * Set shots depending on current project.
     * 
     * @param project
     *            Project used to configure stores.
     */
    public void configureStores(ProjectModelData project)
    {
        this.shotStore.setPath(ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.SHOTS);
        this.constituentStore.setPath(ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.CONSTITUENTS);
    }

    /**
     * Register to casting model, the shot navigation tree model.
     * 
     * @param dataModel
     *            The model to set.
     */
    public void setShotNavModel(BaseTreeDataModel<ShotModelData, SequenceModelData> dataModel)
    {
        this.shotNavModel = dataModel;
    }

    /**
     * Register to casting model, the constituent navigation tree model.
     * 
     * @param dataModel
     *            The model to set.
     */
    public void setConstituentNavModel(BaseTreeDataModel<ConstituentModelData, CategoryModelData> dataModel)
    {
        this.constituentNavModel = dataModel;
    }

    /** Reload data displayed by navigation trees. */
    public void reloadTrees(ProjectModelData project)
    {
        this.shotNavModel.setCurrentProject(project);
        this.constituentNavModel.setCurrentProject(project);
    }

    /**
     * Refresh constituent store data based on shot given in parameter. First it reloads composition store, then with
     * composition retrieved it set a new filter on constituent store and reload it.
     * 
     * @param shot
     *            The shot of which casting should be retrieved.
     */
    public void refreshConstituentStore(ShotModelData shot)
    {
        // this.shotConstraint.setLeftMember(shot.getId());
        this.compoStore.clearParameters();
        this.compoStore.setPath(shot.getDefaultPath() + "/" + ServicesPath.COMPOSITIONS);
        this.loadingEntity = ConstituentModelData.SIMPLE_CLASS_NAME;

        this.compoStore.reload();
    }

    /**
     * Refresh shot store data based on constituent given in parameter. First it reloads composition store, then with
     * composition retrieved it set a new filter on shot store and reload it.
     * 
     * @param constituent
     *            The constituent of which casting should be retrieved.
     */
    public void refreshShotStore(ConstituentModelData constituent)
    {
        this.constituentConstraint.setLeftMember(constituent);
        this.compoStore.clearParameters();
        this.compoStore.setPath(constituent.getDefaultPath() + "/" + ServicesPath.COMPOSITIONS);
        this.loadingEntity = ShotModelData.SIMPLE_CLASS_NAME;

        this.compoStore.reload();
    }

    /**
     * When compositions are loaded.
     * 
     * @param shot
     *            The shot of which casting should be retrieved.
     */
    private void onCompositionsLoaded()
    {
        ServiceStore<?> store = storeMap.get(loadingEntity);
        List<Long> ids = new ArrayList<Long>();
        InConstraint constraint = new InConstraint(Hd3dModelData.ID_FIELD, ids);

        if (compoStore.getCount() > 0)
        {
            for (CompositionModelData composition : compoStore.getModels())
            {
                if (ShotModelData.SIMPLE_CLASS_NAME.equals(loadingEntity))
                {
                    ids.add(composition.getShot());
                }
                else
                {
                    ids.add(composition.getConstituent());
                }
            }

            store.clearParameters();
            store.addParameter(constraint);
            store.reload();
        }
        else
        {
            ids.add(-1L); // Needed to set a constraint that will return no result if user refreshes the grid ant that
            // there is no composition.

            store.clearParameters();
            store.addParameter(constraint);
            store.removeAll();
        }
    }

    public void deleteConstituentCompositions(List<ConstituentModelData> constituents)
    {
        List<CompositionModelData> compositions = new ArrayList<CompositionModelData>();
        for (ConstituentModelData constituent : constituents)
        {
            CompositionModelData composition = this.compoStore.findModel(CompositionModelData.CONSTITUENT_ID_FIELD,
                    constituent.getId());
            compositions.add(composition);
        }

        for (CompositionModelData composition : compositions)
        {
            composition.delete();
        }
    }

    public void deleteShotCompositions(List<ShotModelData> shots)
    {
        List<CompositionModelData> compositions = new ArrayList<CompositionModelData>();
        for (ShotModelData shot : shots)
        {
            CompositionModelData composition = this.compoStore.findModel(CompositionModelData.SHOT_ID_FIELD,
                    shot.getId());
            compositions.add(composition);
        }

        for (CompositionModelData composition : compositions)
        {
            composition.delete();
        }
    }

}
