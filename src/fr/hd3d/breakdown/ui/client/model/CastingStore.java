package fr.hd3d.breakdown.ui.client.model;

import fr.hd3d.breakdown.ui.client.event.CastingEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Casting store allow to manage the CastingModelData.
 * 
 * @author Hd3d
 * 
 */
public class CastingStore extends ServiceStore<CastingModelData>
{

    public CastingStore()
    {
        super(null);
    }

    @Override
    public void reload()
    {
        EventDispatcher.forwardEvent(CastingEvents.REFRESH_CLICKED);
    }
}
