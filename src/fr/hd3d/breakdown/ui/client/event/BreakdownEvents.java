package fr.hd3d.breakdown.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by breakdown application.
 * 
 * @author HD3D
 */
public class BreakdownEvents
{
    public static final EventType CONSTITUENT_TAB_CLICKED = new EventType();
    public static final EventType SHOT_TAB_CLICKED = new EventType();
    public static final EventType SEQUENCE_TAB_CLICKED = new EventType();
    public static final EventType CASTING_TAB_CLICKED = new EventType();

    public static final EventType UPDATE_TASK_LIST_CLICKED = new EventType();
    public static final EventType UPDATE_ASSET_LIST_CLICKED = new EventType();
    public static final EventType ODS_IMPORT_CLICKED = new EventType();
    public static final EventType FILE_SUBMITTED = new EventType();
    public static final EventType SCRIPT_EXECUTION_FINISHED = new EventType();

    public static final EventType CONSTITUENT_MODE_CLICKED = new EventType();
    public static final EventType SHOT_MODE_CLICKED = new EventType();
    public static final EventType CASTING_DRAG_START = new EventType();
    public static final EventType CASTING_CONSTITUENT_DROPPED = new EventType();
    public static final EventType CASTING_SHOT_DROPPED = new EventType();
    public static final EventType ODS_IMPORT_COMPLETE = new EventType();

    public static final EventType CONSTITUENT_CREATE_CLICKED = new EventType();
    public static final EventType CONSTITUENT_CREATED = new EventType();
    public static final EventType CONSTITUENT_CREATE_CATEGORY_CLICKED = new EventType();
    public static final EventType CONSTITUENT_DELETE_CLICKED = new EventType();
    public static final EventType CONSTITUENT_DELETE_CONFIRMED = new EventType();
    public static final EventType CONSTITUENT_CATEGORY_DELETE_CLICKED = new EventType();
    public static final EventType CONSTITUENT_CATEGORY_DELETE_CONFIRMED = new EventType();
    public static final EventType CONSTITUENT_RENAME_CLICKED = new EventType();
    public static final EventType CONSTITUENT_RENAMED = new EventType();
    public static final EventType SELECTION_DROPPED = new EventType();

    public static final EventType SHOT_CREATE_CLICKED = new EventType();
    public static final EventType SHOT_CREATE_SEQUENCE_CLICKED = new EventType();
    public static final EventType SHOT_CREATED = new EventType();
    public static final EventType SHOT_MASS_CREATION_SUCCESS = new EventType();
    public static final EventType SHOT_DELETE_CLICKED = new EventType();
    public static final EventType SHOT_SEQUENCE_DELETE_CLICKED = new EventType();
    public static final EventType SHOT_DELETE_CONFIRMED = new EventType();
    public static final EventType SHOT_SEQUENCE_DELETE_CONFIRMED = new EventType();
    public static final EventType SHOT_RENAME_CLICKED = new EventType();
    public static final EventType SHOT_RENAMED = new EventType();
    public static final EventType SHOT_DROPPED = new EventType();

    public static final EventType WORK_OBJECT_CREATION_REQUESTED = new EventType();
    public static final EventType CONSTITUENT_MASS_CREATION_SUCCESS = new EventType();
    public static final EventType CASTING_REMOVE_CLICKED = new EventType();
    public static final EventType CASTING_REFRESH_CLICKED = new EventType();

    // public static final EventType TASK_TYPE_BUTTON_CLICKED = new EventType();
    public static final EventType CASTING_SEQUENCE_TAB_CLICKED = new EventType();
}
