package fr.hd3d.breakdown.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


public class CastingEvents
{
    public static final EventType COMPOSITION_LOADED = new EventType();
    public static final EventType SHOT_LOADED = new EventType();
    public static final EventType CONSTITUENT_LOADED = new EventType();
    public static final EventType CATEGORIES_LOADED = new EventType();
    public static final EventType SEQUENCE_ALL_CHILDREN_LOADED = new EventType();
    public static final EventType CASTING_LOADED = new EventType();
    public static final EventType CONSTITUENT_DROPPED = new EventType();
    public static final EventType COPY_KEYPRESS = new EventType();
    public static final EventType PASTE_KEYPRESS = new EventType();
    public static final EventType DELETE_KEYPRESS = new EventType();
    public static final EventType REFRESH_CLICKED = new EventType();
    public static final EventType DELETE_COMPOSITION_CLICKED_FROM_CONTEXTMENU = new EventType();
    public static final EventType COMPOSITION_SAVE_SUCCESS = new EventType();
    public static final EventType CATEGORIES_ROOT_LOADED = new EventType();
    public static final EventType AFTER_EDIT = new EventType();
    public static final EventType SHOT_SAVE_SUCCESS = new EventType();
    public static final EventType REFRESH_CATEGORIE_CLICKED = new EventType();
    public static final EventType EXPORT_CASTING_CSV = new EventType();
}
