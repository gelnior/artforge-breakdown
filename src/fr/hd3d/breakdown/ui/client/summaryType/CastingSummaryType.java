package fr.hd3d.breakdown.ui.client.summaryType;

import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;

import fr.hd3d.breakdown.ui.client.model.CastingModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;


public abstract class CastingSummaryType extends SummaryType<Long>
{

    // public static final SummaryType<Long> SUM_CASTING = new SummaryType<Long>() {
    // @Override
    // public Long render(Object v, ModelData m, String field, Map<String, Object> data)
    // {
    // System.out.println(v + ", " + m + ", " + field + ", " + data);
    // return null;
    // }
    // };

    public static final SummaryType<Long> COUNT_CASTING = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            CastingModelData casting = (CastingModelData) m;
            List<CompositionModelData> compositionList = casting.getCompositions(field);
            if (compositionList == null || compositionList.isEmpty())
            {
                return (Long) v;
            }
            v = (Long) v + 1;

            return (Long) v;
        }
    };

    public static final SummaryType<Long> SUM_CASTING = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            CastingModelData casting = (CastingModelData) m;
            List<CompositionModelData> compositionList = casting.getCompositions(field);
            if (compositionList == null || compositionList.isEmpty())
            {
                return (Long) v;
            }
            for (CompositionModelData composition : compositionList)
            {
                v = (Long) v + composition.getOcc();
            }
            return (Long) v;
        }
    };

    public static final SummaryType<Long> DIFFERENT_CASTING = new SummaryType<Long>() {

        @Override
        public Long render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            if (v == null)
            {
                v = 0L;
            }
            CastingModelData casting = (CastingModelData) m;
            List<CompositionModelData> compositionList = casting.getCompositions(field);
            if (compositionList == null || compositionList.isEmpty())
            {
                return (Long) v;
            }

            for (CompositionModelData composition : compositionList)
            {
                data.put(composition.getConstituent().toString(), composition);
            }
            v = (long) data.size();
            return (Long) v;
        }
    };

    public static final SummaryType<Double> AVERAGE_CASTING = new SummaryType<Double>() {

        @Override
        public Double render(Object v, ModelData m, String field, Map<String, Object> data)
        {
            Double nbshot;
            if ((nbshot = (Double) data.get("NBSHOT")) == null)
            {
                nbshot = 0d;
            }
            nbshot = nbshot + 1;
            data.put("NBSHOT", nbshot);

            Double sum;
            if ((sum = (Double) data.get("SUM")) == null)
            {
                sum = 0d;
            }

            CastingModelData casting = (CastingModelData) m;
            List<CompositionModelData> compositionList = casting.getCompositions(field);
            if (compositionList == null)
            {
                return (sum / nbshot);
            }

            for (CompositionModelData composition : compositionList)
            {
                sum = sum + composition.getOcc();
            }
            v = (sum / nbshot);

            data.put("SUM", sum);
            return (Double) v;
        }
    };
}
