package fr.hd3d.breakdown.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.breakdown.ui.client.view.BreakdownView;
import fr.hd3d.breakdown.ui.client.view.IBreakdownView;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Production Tool entry point.
 * 
 * @author HD3D
 */
public class Breakdown implements EntryPoint
{
    /**
     * This is the entry point method. It sets up the viewport and start application configuration
     */
    public void onModuleLoad()
    {
        // PermissionUtil.setDebugOn();

        IBreakdownView mainView = new BreakdownView();
        mainView.init();

        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
