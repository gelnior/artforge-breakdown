package fr.hd3d.breakdown.ui.client.enums;

/**
 * Enumeration used to know if casting is in shot mode (user see constituents on shots and edit them) or in consituent
 * mode (the opposite).
 * 
 * @author HD3D
 */
public enum ECastingMode
{
    CONSTITUENT, SHOT;
}
