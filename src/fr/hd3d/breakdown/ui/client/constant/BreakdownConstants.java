package fr.hd3d.breakdown.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * '/home/frank.rousseau/workspace/suiviDeProduction/src/fr/hd3d/production/ui/client/constant/ProductionConstants.prope
 * r t i e s ' .
 */
public interface BreakdownConstants extends com.google.gwt.i18n.client.Constants
{

    /**
     * Translated "Add note".
     * 
     * @return translated "Add note"
     */
    @DefaultStringValue("Add note")
    @Key("AddNote")
    String AddNote();

    /**
     * Translated "Approval Editor".
     * 
     * @return translated "Approval Editor"
     */
    @DefaultStringValue("Approval Editor")
    @Key("ApprovalEditor")
    String ApprovalEditor();

    /**
     * Translated "Approval History".
     * 
     * @return translated "Approval History"
     */
    @DefaultStringValue("Approval History")
    @Key("ApprovalHistory")
    String ApprovalHistory();

    /**
     * Translated "Approval Note".
     * 
     * @return translated "Approval Note"
     */
    @DefaultStringValue("Approval Note")
    @Key("ApprovalNote")
    String ApprovalNote();

    /**
     * Translated "Approval note history".
     * 
     * @return translated "Approval note history"
     */
    @DefaultStringValue("Approval note history")
    @Key("ApprovalNoteHistory")
    String ApprovalNoteHistory();

    /**
     * Translated "Delete note".
     * 
     * @return translated "Delete note"
     */
    @DefaultStringValue("Delete note")
    @Key("DeleteNote")
    String DeleteNote();

    /**
     * Translated "Edit last note".
     * 
     * @return translated "Edit last note"
     */
    @DefaultStringValue("Edit last note")
    @Key("EditLastNote")
    String EditLastNote();

    /**
     * Translated "Edit note".
     * 
     * @return translated "Edit note"
     */
    @DefaultStringValue("Edit note")
    @Key("EditNote")
    String EditNote();

    /**
     * Translated "No Information".
     * 
     * @return translated "No Information"
     */
    @DefaultStringValue("No Information")
    @Key("NoInformation")
    String NoInformation();

    /**
     * Translated "Suivi de production".
     * 
     * @return translated "Suivi de production"
     */
    @DefaultStringValue("Suivi de production")
    @Key("ProductionTool")
    String ProductionTool();

    /**
     * Translated "Retake".
     * 
     * @return translated "Retake"
     */
    @DefaultStringValue("Retake")
    @Key("Retake")
    String Retake();

    /**
     * Translated "Show history".
     * 
     * @return translated "Show history"
     */
    @DefaultStringValue("Show history")
    @Key("ShowHistory")
    String ShowHistory();

    /**
     * Translated "Validate".
     * 
     * @return translated "Validate"
     */
    @DefaultStringValue("Validate")
    @Key("Validate")
    String Validate();

    /**
     * Translated "Validate with a comment".
     * 
     * @return translated "Validate with a comment"
     */
    @DefaultStringValue("Validate with a comment")
    @Key("ValidateComment")
    String ValidateComment();

    /**
     * Translated "Waiting for approval".
     * 
     * @return translated "Waiting for approval"
     */
    @DefaultStringValue("Waiting for approval")
    @Key("WaitingForApproval")
    String WaitingForApproval();

    /**
     * Translated "Delete Checked Constituents".
     * 
     * @return translated "Delete Checked Constituents"
     */
    @DefaultStringValue("Delete Checked Constituents")
    @Key("DeleteCheckedConstituents")
    String DeleteCheckedConstituents();

    /**
     * Translated "Copy Checked Constituents".
     * 
     * @return translated "Copy Checked Constituents"
     */
    @DefaultStringValue("Copy Checked Constituents")
    @Key("CopyCheckedConstituents")
    String CopyCheckedConstituents();

    /**
     * Translated "Create Constituent".
     * 
     * @return translated "Create Constituent"
     */
    @DefaultStringValue("Create Constituent")
    @Key("CreateConstituent")
    String CreateConstituent();

    /**
     * Translated "Delete Constituents".
     * 
     * @return translated "Delete Constituents"
     */
    @DefaultStringValue("Delete Constituents")
    @Key("DeleteConstituents")
    String DeleteConstituents();

    /**
     * Translated "Constituents Deletion".
     * 
     * @return translated "Constituents Deletion"
     */
    @DefaultStringValue("Constituents Deletion")
    @Key("ConstituentsDeletion")
    String ConstituentsDeletion();

    /**
     * Translated "AreYouSureDeleteConstituents".
     * 
     * @return translated "AreYouSureDeleteConstituents"
     */
    @DefaultStringValue("Are you sure you want to delete selected constituents ?")
    @Key("AreYouSureDeleteConstituents")
    String AreYouSureDeleteConstituents();

    /**
     * Translated "create Shot".
     * 
     * @return translated "create Shot"
     */
    @DefaultStringValue("create Shot")
    @Key("CreateShot")
    String CreateShot();

    /**
     * Translated "Create Sequence".
     * 
     * @return translated "Create Sequence"
     */
    @DefaultStringValue("Create Sequence")
    @Key("CreateSequence")
    String CreateSequence();

    /**
     * Translated "Delete Shots".
     * 
     * @return translated "Delete Shots"
     */
    @DefaultStringValue("Delete Shots")
    @Key("DeleteShots")
    String DeleteShots();

    /**
     * Translated "Delete Sequences".
     * 
     * @return translated "Delete Sequences"
     */
    @DefaultStringValue("Delete Sequences")
    @Key("DeleteSequences")
    String DeleteSequences();

    @DefaultStringValue("Constituents and shots creation...")
    @Key("ConstituentsShotsCreation")
    String ConstituentsShotsCreation();

    @DefaultStringValue("Nb frame")
    @Key("NbFrame")
    String NbFrame();

    @DefaultStringValue("Number of shots")
    @Key("NumberOfShots")
    String NumberOfShots();

    @DefaultStringValue("Average per shot")
    @Key("AveragePerShot")
    String AveragePerShot();

    @DefaultStringValue("Shot deletion")
    @Key("ShotDeletion")
    String ShotDeletion();

    @DefaultStringValue("Are you sure you want to delete selected shots ?")
    @Key("AreYouSureDeleteShots")
    String AreYouSureDeleteShots();

    @DefaultStringValue("Sequences deletion")
    @Key("SequencesDeletion")
    String SequencesDeletion();

    @DefaultStringValue("Are you sure you want to delete selected sequences ?")
    @Key("AreYouSureDeleteSequences")
    String AreYouSureDeleteSequences();

    @DefaultStringValue("Shots")
    @Key("Shots")
    String Shots();

}
