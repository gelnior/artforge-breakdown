package fr.hd3d.breakdown.ui.client.config;

/**
 * Application constants.
 * 
 * @author HD3D
 */
public class BreakdownConfig
{
    public static final String TAB_COOKIE_VAR = "breakdown-tab";
    public static final String SHEET_COOKIE_VAR_PREFIX = "breakdown-sheet-";
    public static final String UPDATE_TASK_LIST_SCRIPT = "updateTaskList";
    public static final String UPDATE_ASSET_LIST_SCRIPT = "updateAssetList";

    public static final String DNDEVENT_EVENT_VAR_NAME = "dnd-event";
    public static final String FORMODE_EVENT_VAR_NAME = "for-mode";
    public static final String ODS_IMPORT_SCRIPT = "initializeWithOds";
    public static final String DND_PARENT_EVENT_VAR_NAME = "dnd-parent";
    public static final String DND_CONSTITUENT_EVENT_VAR_NAME = "dnd-constituent";
    public static final String DND_CATEGORY_EVENT_VAR_NAME = "dnd-category";
    public static final String DND_SHOT_EVENT_VAR_NAME = "dnd-shot";
    public static final String DND_SEQUENCE_EVENT_VAR_NAME = "dnd-sequence";
    public static final String PARENT_EVENT_VAR_NAME = "parent";
}
