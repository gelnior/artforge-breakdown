package fr.hd3d.breakdown.ui.client.config;

import com.extjs.gxt.ui.client.util.IconHelper;
import com.google.gwt.user.client.ui.AbstractImagePrototype;


/**
 * Images specifically used by Breakdown application.
 * 
 * @author HD3D
 */
public class BreakdownImages
{

    public static AbstractImagePrototype getCastingIcon()
    {
        return IconHelper.create("images/user_orange.png");
    }

    public static AbstractImagePrototype getOdsImportIcon()
    {
        return IconHelper.create("images/ods_import.png");
    }

    public static AbstractImagePrototype getUpdateTaskIcon()
    {
        return IconHelper.create("images/sync_task.png", 24, 24);
    }

    public static AbstractImagePrototype getUpdateAssetIcon()
    {
        return IconHelper.create("images/sync_asset.png", 24, 24);
    }

}
