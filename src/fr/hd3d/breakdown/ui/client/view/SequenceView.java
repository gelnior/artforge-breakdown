package fr.hd3d.breakdown.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.google.gwt.core.client.GWT;

import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.controller.SequenceController;
import fr.hd3d.breakdown.ui.client.controller.ShotController;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.view.column.BreakdownColumnInfoProvider;
import fr.hd3d.breakdown.ui.client.view.column.BreakdownEditorProvider;
import fr.hd3d.breakdown.ui.client.view.contextmenu.ShotMenu;
import fr.hd3d.breakdown.ui.client.view.dnd.ShotBreakDropTarget;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.model.SequenceModel;
import fr.hd3d.common.ui.client.model.ShotModel;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.widget.FilterStoreField;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.workobject.browser.events.WorkObjectEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.SequenceTree;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;


/**
 * Shot view contains shot browsing widgets : temporal navigation tree, shot explorer and shot identity sheet.
 * 
 * @author HD3D
 */
public class SequenceView extends BreakWorkObjectView
{
    /** View name needed to register active tab. */
    public static final String NAME = "sequence";

    /** Constant strings to display : dialog messages, button label... */
    public static BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);

    /** Temporal navigation tree. */
    private final SequenceTree tree = new SequenceTree();
    /** Model handling shot view data. */
    private final SequenceModel model = new SequenceModel();
    /** Model handling shot view data. */
    private final SequenceController controller = new SequenceController(model, this);


    /**
     * Default constructor : initialize widgets then register controllers.
     */
    public SequenceView()
    {
        super();

        this.model.setTreeStore(this.tree.getStore());

        this.setSheetCombo();
        this.setTree();
        this.configureExplorer();

        this.controller.addChild(this.explorer.getController());
    }

    /** @return Shot view controller. */
    public MaskableController getController()
    {
        return this.controller;
    }

    /** Mask shot view controller. */
    @Override
    public void idle()
    {
        super.idle();
        this.controller.mask();
    }

    /** Unmask shot view controller. */
    @Override
    public void unidle()
    {
        super.unidle();
        this.controller.unMask();
    }

    /** Reload navigation tree for selected project. */
    public void reloadTree(ProjectModelData project)
    {
        this.tree.setCurrentProject(project);
    }

    /**
     * @return Elements selected in navigation tree.
     */
    public List<RecordModelData> getTreeSelection()
    {
        return this.tree.getSelection();
    }

    /**
     * Displays a shot delete confirmation message box.
     */
    public void showShotDeleteConfirmation()
    {
//        ConfirmationDisplayer.display(shotDeletionString, areYouSureDeleteShotString,
//                BreakdownEvents.SHOT_DELETE_CONFIRMED);
    }

    /**
     * Displays a sequence delete confirmation message box.
     */
    public void showSequenceDeleteConfirmation()
    {
//        ConfirmationDisplayer.display(sequencesDeletionString, areYouSureDeleteSequenceString,
//                BreakdownEvents.SHOT_SEQUENCE_DELETE_CONFIRMED);
    }

    /**
     * Expand parent given in parameter.
     * 
     * @param parent
     *            The parent to expand.
     */
    public void expandParent(RecordModelData parent)
    {
        this.tree.expand(parent);
    }

    /**
     * Set and configure sheet combo for retrieving only shot sheet for breakdown application and registers its store to
     * model.
     */
    private void setSheetCombo()
    {
        this.model.setSheetStore(this.sheetCombo.getServiceStore());

        this.sheetCombo.setType(ESheetType.BREAKDOWN);
        this.sheetCombo.addAvailableEntity(SequenceModelData.SIMPLE_CLASS_NAME);
        this.sheetCombo.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                WorkObjectEvents.SHEET_CHANGED));
    }

    /**
     * Set navigation tree to west.
     */
    private void setTree()
    {
        this.tree.setBodyBorder(false);
        this.tree.setBorders(false);
        this.addToWestPanel(tree);
    }
    
    /**
     * Configure explorer with right field providers and configure sheet editor for shots only.
     */
    private void configureExplorer()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();
        EntityModelData entity = new EntityModelData();
        String entityName = SequenceModelData.SIMPLE_CLASS_NAME;
        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        entities.add(entity);

        this.explorer.setColumnInfoProvider(new BreakdownColumnInfoProvider());
        this.explorer.setConstraintEditorProvider(new BreakdownEditorProvider());
        this.model.setExplorerStore(explorer.getStore(), SequenceModelData.SIMPLE_CLASS_NAME.toLowerCase());
        this.model.setEntities(entities);
        this.explorer.setApplicationFilter(this.model.getFilter());
    }

	/**
     * Enable navigation tree.
     */
	public void enableTree() 
	{
		this.tree.enable();
	}
	
    /**
     * Disable navigation tree.
     */
	public void disableTree() 
	{
		this.tree.disable();
	}
}
