package fr.hd3d.breakdown.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.widget.form.StoreFilterField;
import com.google.gwt.core.client.GWT;

import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.controller.ConstituentController;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.view.column.BreakdownColumnInfoProvider;
import fr.hd3d.breakdown.ui.client.view.contextmenu.ConstituentMenu;
import fr.hd3d.breakdown.ui.client.view.dnd.ConstituentBreakDropTarget;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.model.ConstituentModel;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.workobject.browser.events.WorkObjectEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;


/**
 * ConstituentView displays constituent data for validation.
 * 
 * @author HD3D
 */
public class ConstituentView extends BreakWorkObjectView implements IConstituentView
{
    private static final BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);

    private static final String constituentsDeletion = CONSTANTS.ConstituentsDeletion();
    private static final String areYouSureDeleteConstituents = CONSTANTS.AreYouSureDeleteConstituents();
    private static final String categoriesDeletion = "Categories deletion";
    private static final String areYouSureDeleteCategories = "Are you sure you want to delete selected categories ?";

    /** Model that handles displayed data. */
    private final ConstituentModel model = new ConstituentModel();
    /** Controller that handles constituent view related events. */
    private final ConstituentController controller = new ConstituentController(model, this);

    /** Constituent navigation tree. */
    private final ConstituentTree constituentTree = new ConstituentTree();

    /**
     * Default constructor : initialize widgets then register controllers.
     */
    public ConstituentView()
    {
        super();

        this.model.setTreeStore(this.constituentTree.getStore());

        this.setSheetCombo();
        this.setTree();
        this.configureExplorer();
        this.setTreeDnd();

        // this.controller.addChild(this.identityPortal.getController());
        this.controller.addChild(this.explorer.getController());
        this.setTreeFilter();
    }

    public MaskableController getController()
    {
        return this.controller;
    }

    public void reloadTree(ProjectModelData project)
    {
        this.constituentTree.setCurrentProject(project);
    }

    @Override
    public void idle()
    {
        super.idle();
        this.controller.mask();
    }

    @Override
    public void unidle()
    {
        super.unidle();
        this.controller.unMask();
    }

    public List<RecordModelData> getTreeSelection()
    {
        return this.constituentTree.getSelection();
    }

    public void showConstituentDeleteConfirmation()
    {
        ConfirmationDisplayer.display(constituentsDeletion, areYouSureDeleteConstituents,
                BreakdownEvents.CONSTITUENT_DELETE_CONFIRMED);
    }

    public void showCategoryDeleteConfirmation()
    {
        ConfirmationDisplayer.display(categoriesDeletion, areYouSureDeleteCategories,
                BreakdownEvents.CONSTITUENT_CATEGORY_DELETE_CONFIRMED);
    }

    public void expandParent(RecordModelData parent)
    {
        this.constituentTree.expand(parent);
    }

    /** Configure sheet combo box and register its store to model. */
    private void setSheetCombo()
    {
        this.model.setSheetStore(this.sheetCombo.getServiceStore());
        this.sheetCombo.setType(ESheetType.BREAKDOWN);
        this.sheetCombo.addAvailableEntity(ConstituentModelData.SIMPLE_CLASS_NAME);
        this.sheetCombo.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                WorkObjectEvents.SHEET_CHANGED));
    }

    /**
     * Add navigation tree to west panel.
     */
    private void setTree()
    {
        this.constituentTree.setBodyBorder(false);
        this.constituentTree.setBorders(false);
        this.constituentTree.getTree().setContextMenu(new ConstituentMenu(constituentTree));
        this.addToWestPanel(constituentTree);
    }

    // Actually it does not work, to implements later.
    private void setTreeFilter()
    {
        StoreFilterField<RecordModelData> storeFilter = new StoreFilterField<RecordModelData>() {
            @Override
            protected boolean doSelect(Store<RecordModelData> store, RecordModelData parent, RecordModelData record,
                    String property, String filter)
            {
                if (constituentTree.getStore().getLoader().hasChildren(record))
                {
                    return true;
                }
                String name = record.get(ConstituentModelData.CONSTITUENT_LABEL);
                name = name.toLowerCase();
                if (name.startsWith(filter.toLowerCase()))
                {
                    return true;
                }
                return false;
            }
        };
        storeFilter.bind(constituentTree.getStore());
        storeFilter.setWidth(45);
        // this.westPanel.getHeader().addTool(storeFilter);
    }

    /** Configure explorer and sheet editor for constituent browsing. */
    private void configureExplorer()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();
        EntityModelData entity = new EntityModelData();
        String entityName = ConstituentModelData.SIMPLE_CLASS_NAME;
        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        entities.add(entity);

        this.explorer.setColumnInfoProvider(new BreakdownColumnInfoProvider());
        this.model.setExplorerStore(explorer.getStore(), ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase());
        this.model.setEntities(entities);
        this.explorer.setApplicationFilter(this.model.getFilter());
    }

    /**
     * Allow drag and drop between tree nodes and leaves.
     */
    private void setTreeDnd()
    {
        new TreePanelDragSource(constituentTree.getTree());

        ConstituentBreakDropTarget target = new ConstituentBreakDropTarget(constituentTree);
        target.setAllowSelfAsSource(true);
        target.setAllowDropOnLeaf(false);
        target.setOperation(Operation.MOVE);
    }

	/**
     * Enable navigation tree.
     */
	public void enableTree() 
	{
		this.constituentTree.enable();
	}
	
    /**
     * Disable navigation tree.
     */
	public void disableTree() 
	{
		this.constituentTree.disable();
	}


    // public Record getTreeConstituentRecord(Long id)
    // {
    // RecordModelData constituent = this.constituentTree.getStore().findModel(Hd3dModelData.ID_FIELD, id);
    // return this.constituentTree.getStore().getRecord(constituent);
    // }
    //
    // public void updateTreeConstituentRecord(Record constituent)
    // {
    // this.constituentTree.getStore().update((RecordModelData) constituent.getModel());
    // }
}
