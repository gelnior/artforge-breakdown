package fr.hd3d.breakdown.ui.client.view.column;

import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.IdentitySheetEditorProvider;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.StepField;


/**
 * Editor provider provides edition fields for identity sheet widget.
 * 
 * @author HD3D
 */
public class BreakdownEditorProvider extends IdentitySheetEditorProvider
{
    /**
     * @param name
     *            The editor name.
     * @return Field corresponding to <i>name</i>.
     */
    public Field<?> getEditor(String name, Object value, ItemModelData itemModelData)
    {
        if (Renderer.ACTIVITIES_DURATION.equals(name))
        {
            DurationField field = new DurationField();
            if (value != null)
                field.setValue(((Number) value));
            return field;
        }
        if (Renderer.STEP.equals(name))
        {
            StepField field = new StepField();

            if (value != null)
            {
                if (value instanceof Long)
                {
                    field.setLongValue((Long) value);
                }
                else if (value instanceof StepModelData)
                {
                    field.setStepValue((StepModelData) value);
                }
            }
            return field;
        }
        return super.getEditor(name, value, itemModelData);
    }
}
