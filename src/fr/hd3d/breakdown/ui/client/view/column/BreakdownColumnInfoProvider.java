package fr.hd3d.breakdown.ui.client.view.column;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.explorer.model.BaseColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.HoursRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.RatingCellRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.SimpleApprovalRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ThumbnailRenderer;
import fr.hd3d.common.ui.client.widget.grid.editor.StepDurationEditor;


public class BreakdownColumnInfoProvider extends BaseColumnInfoProvider
{
    public BreakdownColumnInfoProvider()
    {
        super();
    }

    @Override
    public CellEditor getEditor(String name, IColumn column)
    {
        CellEditor editor = super.getEditor(name, column);
        if (name.equals("step"))
        {
            // return new StepEditor(StepField.getPossibleValues());
            return new StepDurationEditor(new DurationField());
        }
        return editor;
    }

    @Override
    public GridCellRenderer<Hd3dModelData> getRenderer(String name)
    {
        if (name == null)
        {
            return null;
        }
        else if (name.equals(Renderer.STEP))
        {
            return new StepRenderer();
        }
        else if (name.equals(Renderer.DURATION))
        {
            return new HoursRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.RATING))
        {
            return new RatingCellRenderer();
        }
        else if (name.equals(Renderer.THUMBNAIL))
        {
            return new ThumbnailRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.APPROVAL))
        {
            return new SimpleApprovalRenderer();
        }
        else
        {
            return super.getRenderer(name);
        }
    }
}
