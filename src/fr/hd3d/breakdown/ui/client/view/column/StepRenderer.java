package fr.hd3d.breakdown.ui.client.view.column;

import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IAggregationRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.RecordModelDataRenderer;


/**
 * Step renderer renders long as string if step value is of type long. It happens when value was just set from an empty
 * value. If step has been set before, it extracts from step object the estimated duration then display it as a string.
 * 
 * @author HD3D
 */
public class StepRenderer extends RecordModelDataRenderer implements IAggregationRenderer
{
    /**
     * @return estimated duration for current step (can handle duration set in Long format or duration set inside step
     *         object).
     */
    @Override
    protected String getValueFromObject(Object obj)
    {
        if (obj == null)
        {
            return this.getStepDiv(0L);
        }
        if (obj instanceof Long)
        {
            return this.getStepDiv((Long) obj);
        }
        if (obj instanceof Double)
        {
            return this.getStepDiv(((Double) obj).longValue());
        }
        if (obj instanceof StepModelData)
        {
            StepModelData step = (StepModelData) obj;
            return this.getStepDiv(step.getEstimatedDuration());
        }

        return this.getStepDiv(0L);
    }

    private String getStepDiv(Long duration)
    {
        // FieldModelData selectedField = StepField.getPossibleValues().get(duration);
        String displayedDuration = "0";
        // if (selectedField != null)
        displayedDuration = DurationField.getStringFromLong(duration);
        String divText = "<div style=\"text-align: center; font-weight: bold; font-size: 16px;\">" + displayedDuration
                + "</div>";

        return divText;
    }

    public Object renderAggregate(Object value)
    {
        return getValueFromObject(value);
    }
}
