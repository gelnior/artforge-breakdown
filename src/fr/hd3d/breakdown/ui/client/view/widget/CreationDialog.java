package fr.hd3d.breakdown.ui.client.view.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormData;

import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.model.CreationData;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;


/**
 * 
 * @author HD3D
 */
public class CreationDialog extends FormDialog
{
    /** Name of the object to create. */
    private final TextField<String> nameField = new TextField<String>();

    private final CheckBox massCreationCheckBox = new CheckBox();

    private final NumberField startIndexField = new NumberField();
    private final NumberField endIndexField = new NumberField();
    private final NumberField stepField = new NumberField();

    private final Button okCloseButton = new Button("Create & close");

    private String simpleClassName;

    private boolean toHideWhenSaveEnds = false;

    public CreationDialog(EventType okEvent, String header)
    {
        super(okEvent, header);

        this.setFields();
        this.resetForm();
        this.setOtherListeners();
        this.setStyles();

        this.okButton.disable();
        this.okCloseButton.disable();
    }

    public boolean isToHideWhenSaveEnds()
    {
        return toHideWhenSaveEnds;
    }

    public void resetForm()
    {
        this.nameField.setValue(null);
        this.massCreationCheckBox.setValue(Boolean.FALSE);
        this.startIndexField.setValue(1);
        this.endIndexField.setValue(10);
        this.stepField.setValue(1);

        this.nameField.focus();
    }

    @Override
    protected void onOkClicked()
    {
        CreationData data = new CreationData();
        data.setName(nameField.getValue());
        data.setMassCreation(massCreationCheckBox.getValue());
        if (data.getMassCreation())
        {
            Integer startIndex = startIndexField.getValue().intValue();
            data.setStartIndex(startIndex);
            Integer endIndex = endIndexField.getValue().intValue();
            data.setEndIndex(endIndex);
            Integer step = stepField.getValue().intValue();
            data.setStep(step);
        }
        data.setSimpleClassName(simpleClassName);

        this.showSaving();
        this.okButton.disable();
        this.okCloseButton.disable();
        this.panel.disable();

        EventDispatcher.forwardEvent(BreakdownEvents.WORK_OBJECT_CREATION_REQUESTED, data);
    }

    public void enableForm()
    {
        this.panel.enable();
        this.resetForm();
    }

    public void show(String simpleClassName)
    {
        super.show();
        this.nameField.clear();
        this.nameField.focus();
        this.simpleClassName = simpleClassName;
        this.startIndexField.hide();
        this.endIndexField.hide();
        this.stepField.hide();
        this.setHeight(150);
        this.toHideWhenSaveEnds = false;
    }

    public boolean isValid()
    {
        boolean IsMassFormValid = this.startIndexField.getValue() != null && this.endIndexField.getValue() != null
                && this.stepField.getValue() != null;
        if (this.nameField.getValue() == null && (!this.massCreationCheckBox.getValue() || IsMassFormValid))
            return false;

        if (this.massCreationCheckBox.getValue())
        {
            int startIndex = startIndexField.getValue().intValue();
            if (startIndex <= 0)
                return false;

            int endIndex = endIndexField.getValue().intValue();
            if (endIndex <= 0 || endIndex - startIndex <= 0)
                return false;

            int step = stepField.getValue().intValue();
            if (step <= 0 || step > 100 || ((endIndex - startIndex) / step) > 100)
                return false;
        }

        if (this.nameField.getValue().length() == 0)
            return false;
        return true;
    }

    @Override
    protected void onCancelClicked()
    {
        this.hide();
    }

    protected void onOkCloseClicked()
    {
        this.toHideWhenSaveEnds = true;
        this.onOkClicked();
    }

    protected void setOtherListeners()
    {
        this.massCreationCheckBox.addListener(Events.Change, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onCheckBoxChanged();
            }
        });
        this.okCloseButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                onOkCloseClicked();
            }
        });

        this.nameField.addKeyListener(enterListener);
        this.endIndexField.addKeyListener(enterListener);
        this.startIndexField.addKeyListener(enterListener);
        this.stepField.addKeyListener(enterListener);
        this.massCreationCheckBox.addKeyListener(enterListener);
    }

    protected void onCheckBoxChanged()
    {
        if (this.massCreationCheckBox.getValue())
        {
            this.startIndexField.show();
            this.endIndexField.show();
            this.stepField.show();

            this.setHeight(230);
        }
        else
        {
            this.startIndexField.hide();
            this.endIndexField.hide();
            this.stepField.hide();
            this.setHeight(150);
        }
        this.layout();
    }

    private void setFields()
    {
        FormData data = new FormData();
        data.setWidth(30);

        this.nameField.setFieldLabel("Name");
        this.addKeyField(nameField, new FormData(255, 20));

        this.massCreationCheckBox.setFieldLabel("Mass creation");
        this.addField(massCreationCheckBox, new FormData(255, 20));

        this.startIndexField.setFieldLabel("Start index");
        this.addKeyField(startIndexField, data);

        this.endIndexField.setFieldLabel("End index");
        this.addKeyField(endIndexField, data);

        this.stepField.setFieldLabel("Step");
        this.addKeyField(stepField, data);
    }

    /**
     * When a registered field changes, this method is called.
     */
    @Override
    protected void onFieldChanged()
    {
        this.okButton.setEnabled(isValid());
        this.okCloseButton.setEnabled(isValid());
    }

    private void setStyles()
    {
        this.setModal(true);
        this.setWidth(370);
        this.setHeight(150);
        this.setHideOnButtonClick(false);

        this.startIndexField.setWidth(30);
        this.endIndexField.setWidth(30);
        this.stepField.setWidth(30);
        this.startIndexField.hide();
        this.endIndexField.hide();
        this.stepField.hide();

        this.okButton.setText("Create");
        this.getButtonBar().insert(okCloseButton, 2);
    }

}
