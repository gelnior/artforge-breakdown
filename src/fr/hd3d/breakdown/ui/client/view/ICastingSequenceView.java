package fr.hd3d.breakdown.ui.client.view;

import java.util.List;

import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel.Cell;

import fr.hd3d.breakdown.ui.client.model.CastingModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;


public interface ICastingSequenceView
{

    void maskLoading();

    void unmaskLoading();

    void reconfigureGrid();

    ColumnModel getCastingColumnModel();

    List<CompositionModelData> getCompositionSelectedGrid();

    List<Cell> getCellsSelected();

    List<CastingModelData> getCastingSelected();

    void updateRow(int index);

    void showMessageCopyError();

    void showMessageDeleteError();

    String getColumnIdSelected();

    void deselectedRows();

    boolean isMultiSelectionGrid();

    void infoDisplay(String title, String message);

    void infoProgress(String title, String message);

    void changeProgress(Double value);

    void openCsvExport(String path);
}
