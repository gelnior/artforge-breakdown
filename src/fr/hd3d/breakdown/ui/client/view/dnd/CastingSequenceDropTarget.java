package fr.hd3d.breakdown.ui.client.view.dnd;

import com.extjs.gxt.ui.client.dnd.GridDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.breakdown.ui.client.model.CastingModelData;


/**
 * Drop target to casting model data grid.
 * 
 * @author HD3D
 * 
 */
public class CastingSequenceDropTarget extends GridDropTarget
{

    /**
     * Default constructor.
     * 
     * @param grid
     *            the grid to drop target.
     */
    public CastingSequenceDropTarget(Grid<CastingModelData> grid)
    {
        super(grid);

    }

    @Override
    protected void onDragDrop(DNDEvent e)
    {
    // empty : cancel the interaction from parent
    // to avoid the copy
    }

    /**
     * Return the active item index when mouse over.
     * 
     * @param e
     *            dnd event.
     * @return the index of item mouse over.
     */
    public int getActiveItemIndex(DNDEvent e)
    {
        int index = grid.getView().findRowIndex(e.getTarget());
        return index;
    }

    @Override
    protected void onDragMove(DNDEvent event)
    {
        int index = getActiveItemIndex(event);
        grid.getSelectionModel().select(index, false);
        if (index < 0)
        {
            event.getStatus().setStatus(false);
        }
        else
        {
            event.getStatus().setStatus(true);
        }
    }

}
