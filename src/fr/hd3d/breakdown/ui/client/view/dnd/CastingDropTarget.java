package fr.hd3d.breakdown.ui.client.view.dnd;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.GridDropTarget;
import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


public class CastingDropTarget extends GridDropTarget
{
    public CastingDropTarget(Grid<?> grid)
    {
        super(grid);
    }

    @Override
    protected void onDragDrop(DNDEvent e)
    {
        Object data = e.getData();
        List<ModelData> models = prepareDropData(data, true);

        List<RecordModelData> workObjects = new ArrayList<RecordModelData>();
        if (models.size() > 0)
        {
            if (feedback == Feedback.APPEND)
            {
                for (ModelData model : models)
                {
                    RecordModelData record = (RecordModelData) model;
                    if (FieldUtils.isConstituent(record) || FieldUtils.isShot(record))
                    {
                        if (this.grid.getStore().findModel(RecordModelData.ID_FIELD, record.getId()) == null)
                            workObjects.add(record);
                    }
                }
                grid.getStore().add(workObjects);
            }
            else
            {
                grid.getStore().insert(models, insertIndex);
            }
        }
        insertIndex = -1;
        activeItem = null;
    }
}
