package fr.hd3d.breakdown.ui.client.view.dnd;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.Insert;
import com.extjs.gxt.ui.client.dnd.TreePanelDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;


public class ConstituentBreakDropTarget extends TreePanelDropTarget
{
    protected ConstituentTree constituentTree;

    public ConstituentBreakDropTarget(ConstituentTree tree)
    {
        super(tree.getTree());

        this.constituentTree = tree;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void showFeedback(DNDEvent event)
    {
        final TreeNode overItem = tree.findNode(event.getTarget());
        if (overItem == null)
        {
            event.getStatus().setStatus(false);
            return;
        }
        TreePanel<?> source = (TreePanel<?>) event.getDragSource().getComponent();
        List<RecordModelData> list = (List<RecordModelData>) source.getSelectionModel().getSelection();

        ModelData overModel = overItem.getModel();

        if (overModel.get(Hd3dModelData.ID_FIELD) == null
                || ((Long) overModel.get(Hd3dModelData.ID_FIELD)).longValue() == -1L)
        {
            boolean isContainsConstituent = false;
            for (RecordModelData record : list)
            {
                if (FieldUtils.isConstituent(record))
                {
                    isContainsConstituent = true;
                    continue;
                }
            }

            if (isContainsConstituent)
            {
                status = -1;
                activeItem = null;
                appendItem = null;
                Insert.get().hide();
                event.getStatus().setStatus(false);
            }
            else
            {
                super.showFeedback(event);
            }
        }
        else
        {
            super.showFeedback(event);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onDragDrop(DNDEvent event)
    {
        super.onDragDrop(event);

        final TreeNode overItem = tree.findNode(event.getTarget());

        RecordModelData overModel = (RecordModelData) overItem.getModel();
        List<ModelData> list = prepareDropData(event.getData(), false);

        List<RecordModelData> constituents = new ArrayList<RecordModelData>();
        List<RecordModelData> categories = new ArrayList<RecordModelData>();

        for (ModelData record : list)
        {
            if (FieldUtils.isConstituent((Hd3dModelData) record.get("model")))
            {
                constituents.add((RecordModelData) record.get("model"));
            }
            else
            {
                categories.add((RecordModelData) record.get("model"));
            }
        }

        AppEvent mvcEvent = new AppEvent(BreakdownEvents.SELECTION_DROPPED);
        mvcEvent.setData(BreakdownConfig.DND_PARENT_EVENT_VAR_NAME, overModel);
        mvcEvent.setData(BreakdownConfig.DND_CONSTITUENT_EVENT_VAR_NAME, constituents);
        mvcEvent.setData(BreakdownConfig.DND_CATEGORY_EVENT_VAR_NAME, categories);

        EventDispatcher.forwardEvent(mvcEvent);
    }
}
