package fr.hd3d.breakdown.ui.client.view.dnd;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.Insert;
import com.extjs.gxt.ui.client.dnd.TreePanelDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;


public class ShotBreakDropTarget extends TreePanelDropTarget
{
    protected ShotTree shotTree;

    public ShotBreakDropTarget(ShotTree tree)
    {
        super(tree.getTree());

        this.shotTree = tree;
    }

    @Override
    protected void showFeedback(DNDEvent event)
    {

        @SuppressWarnings("rawtypes")
        final TreeNode overItem = tree.findNode(event.getTarget());
        TreePanel<?> source = (TreePanel<?>) event.getDragSource().getComponent();
        @SuppressWarnings("unchecked")
        List<RecordModelData> list = (List<RecordModelData>) source.getSelectionModel().getSelection();
        ModelData overModel = overItem.getModel();

        if (overModel.get(Hd3dModelData.ID_FIELD) == null
                || ((Long) overModel.get(Hd3dModelData.ID_FIELD)).longValue() == -1L)
        {
            boolean isContainsShot = false;
            for (RecordModelData record : list)
            {
                if (FieldUtils.isShot(record))
                {
                    isContainsShot = true;
                    continue;
                }
            }

            if (isContainsShot)
            {
                status = -1;
                activeItem = null;
                appendItem = null;
                Insert.get().hide();
                event.getStatus().setStatus(false);
            }
            else
            {
                super.showFeedback(event);
            }
        }
        else
        {
            super.showFeedback(event);
        }
    }

    @Override
    protected void onDragDrop(DNDEvent event)
    {
        super.onDragDrop(event);

        @SuppressWarnings("rawtypes")
        final TreeNode overItem = tree.findNode(event.getTarget());

        RecordModelData overModel = (RecordModelData) overItem.getModel();
        List<ModelData> list = prepareDropData(event.getData(), false);

        List<RecordModelData> shots = new ArrayList<RecordModelData>();
        List<RecordModelData> sequences = new ArrayList<RecordModelData>();

        for (ModelData record : list)
        {
            if (FieldUtils.isShot((Hd3dModelData) record.get("model")))
            {
                shots.add((RecordModelData) record.get("model"));
            }
            else
            {
                sequences.add((RecordModelData) record.get("model"));
            }
        }

        AppEvent mvcEvent = new AppEvent(BreakdownEvents.SELECTION_DROPPED);
        mvcEvent.setData(BreakdownConfig.DND_PARENT_EVENT_VAR_NAME, overModel);
        mvcEvent.setData(BreakdownConfig.DND_SHOT_EVENT_VAR_NAME, shots);
        mvcEvent.setData(BreakdownConfig.DND_SEQUENCE_EVENT_VAR_NAME, sequences);

        EventDispatcher.forwardEvent(mvcEvent);
    }
}
