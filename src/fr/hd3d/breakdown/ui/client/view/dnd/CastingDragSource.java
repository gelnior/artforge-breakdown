package fr.hd3d.breakdown.ui.client.view.dnd;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.DND.TreeSource;
import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Format;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.enums.ECastingMode;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Casting drag source cancel event if the edition mode is not correct and drag only shot and constituents with no
 * sequences and categories.
 * 
 * @author HD3D
 */
public class CastingDragSource extends TreePanelDragSource
{
    /** Mode for which this drag source is working. */
    private final ECastingMode mode;

    /***
     * 
     * @param treePanel
     * @param mode
     */
    public CastingDragSource(TreePanel<RecordModelData> treePanel, ECastingMode mode)
    {
        super(treePanel);

        this.mode = mode;
    }

    @Override
    @SuppressWarnings("rawtypes")
    protected void onDragStart(DNDEvent e)
    {
        this.forwardDragStartEvent(e);

        TreeNode n = tree.findNode(e.getTarget());
        if (n == null)
        {
            e.setCancelled(true);
            return;
        }
        ModelData m = n.getModel();
        if (!tree.getView().isSelectableTarget(m, e.getTarget()))
        {
            e.setCancelled(true);
        }

        if (e.isCancelled())
            return;

        boolean leaf = treeSource == TreeSource.LEAF || treeSource == TreeSource.BOTH;
        boolean node = treeSource == TreeSource.NODE || treeSource == TreeSource.BOTH;

        List<ModelData> sel = tree.getSelectionModel().getSelectedItems();
        if (sel.size() > 0)
        {
            boolean ok = true;
            for (ModelData mi : sel)
            {
                if ((leaf && tree.isLeaf(mi)) || (node && !tree.isLeaf(mi)))
                {
                    continue;
                }
                ok = false;
                break;
            }
            if (ok)
            {
                this.setWorkObjectListToDrag(e, sel);
            }
            else
            {
                e.setCancelled(true);
            }
        }
        else
        {
            e.setCancelled(true);
        }
    }

    /**
     * This forward DND event to casting controller to ensure that it is a valid drag'n'drop depending on current.
     * 
     * @param e
     *            The DND event.
     */
    protected void forwardDragStartEvent(DNDEvent e)
    {
        AppEvent mvcEvent = new AppEvent(BreakdownEvents.CASTING_DRAG_START);
        mvcEvent.setData(BreakdownConfig.DNDEVENT_EVENT_VAR_NAME, e);
        mvcEvent.setData(BreakdownConfig.FORMODE_EVENT_VAR_NAME, mode);
        EventDispatcher.forwardEvent(mvcEvent);
    }

    /**
     * Take only leaves for dragging. Categories and sequences could not appear in casting.
     * 
     * @param e
     *            the drag and drop event.
     * @param sel
     *            The selection to drag.
     */
    protected void setWorkObjectListToDrag(DNDEvent e, List<ModelData> sel)
    {
        List<ModelData> models = new ArrayList<ModelData>();
        for (ModelData mi : sel)
        {
            if (tree.isLeaf(mi))
                models.add(treeStoreState ? tree.getStore().getModelState(mi) : mi);
        }
        e.setData(models);
        e.setCancelled(false);
        e.getStatus().update(Format.substitute(getStatusText(), models.size()));
    }
}
