package fr.hd3d.breakdown.ui.client.view.dnd;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.breakdown.ui.client.enums.ECastingMode;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


/**
 * Cancel drag events if is not in Constituent mode. When drag ends, it sends only selected shots.
 * 
 * @author HD3D
 */
public class ShotTreeDragSource extends CastingDragSource
{
    /***
     * Constructor.
     * 
     * @param treePanel
     *            The panel from where the drag start.
     */
    public ShotTreeDragSource(TreePanel<RecordModelData> treePanel)
    {
        super(treePanel, ECastingMode.CONSTITUENT);
    }

    /**
     * When the item is dropped,
     * 
     * @param event
     *            The drop event.
     * 
     * @see com.extjs.gxt.ui.client.dnd.TreePanelDragSource#onDragDrop(com.extjs.gxt.ui.client.event.DNDEvent)
     */
    @Override
    protected void onDragDrop(DNDEvent event)
    {
        List<ModelData> models = this.tree.getSelectionModel().getSelectedItems();
        List<ShotModelData> selectedShots = new ArrayList<ShotModelData>();

        for (ModelData model : models)
        {
            if (FieldUtils.isShot((Hd3dModelData) model))
                selectedShots.add((ShotModelData) model);
        }
        AppEvent mvcEvent = new AppEvent(BreakdownEvents.CASTING_SHOT_DROPPED);
        mvcEvent.setData(selectedShots);

        EventDispatcher.forwardEvent(mvcEvent);
    }
}
