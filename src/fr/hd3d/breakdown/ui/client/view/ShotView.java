package fr.hd3d.breakdown.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.google.gwt.core.client.GWT;

import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.controller.ShotController;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.view.column.BreakdownColumnInfoProvider;
import fr.hd3d.breakdown.ui.client.view.column.BreakdownEditorProvider;
import fr.hd3d.breakdown.ui.client.view.contextmenu.ShotMenu;
import fr.hd3d.breakdown.ui.client.view.dnd.ShotBreakDropTarget;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.model.ShotModel;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.widget.FilterStoreField;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.workobject.browser.events.WorkObjectEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;


/**
 * Shot view contains shot browsing widgets : temporal navigation tree, shot explorer and shot identity sheet.
 * 
 * @author HD3D
 */
public class ShotView extends BreakWorkObjectView
{
    /** View name needed to register active tab. */
    public static final String NAME = "shot";

    /** Constant strings to display : dialog messages, button label... */
    public static BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);

    private static final String shotDeletionString = CONSTANTS.ShotDeletion();
    private static final String areYouSureDeleteShotString = CONSTANTS.AreYouSureDeleteShots();
    private static final String sequencesDeletionString = CONSTANTS.SequencesDeletion();
    private static final String areYouSureDeleteSequenceString = CONSTANTS.AreYouSureDeleteSequences();

    /** Temporal navigation tree. */
    private final ShotTree shotTree = new ShotTree();
    /** Model handling shot view data. */
    private final ShotModel model = new ShotModel();
    /** Model handling shot view data. */
    private final ShotController controller = new ShotController(model, this);

    /**
     * Default constructor : initialize widgets then register controllers.
     */
    public ShotView()
    {
        super();

        this.model.setTreeStore(this.shotTree.getStore());

        this.setSheetCombo();
        this.setTree();
        this.setTreeDnd();
        this.setTreeFilter();
        this.configureExplorer();

        // this.controller.addChild(this.identityPortal.getController());
        this.controller.addChild(this.explorer.getController());
    }

    /** @return Shot view controller. */
    public MaskableController getController()
    {
        return this.controller;
    }

    /** Mask shot view controller. */
    @Override
    public void idle()
    {
        super.idle();
        this.controller.mask();
    }

    /** Unmask shot view controller. */
    @Override
    public void unidle()
    {
        super.unidle();
        this.controller.unMask();
    }

    /** Reload navigation tree for selected project. */
    public void reloadTree(ProjectModelData project)
    {
        this.shotTree.setCurrentProject(project);
    }

    /**
     * @return Elements selected in navigation tree.
     */
    public List<RecordModelData> getTreeSelection()
    {
        return this.shotTree.getSelection();
    }

    /**
     * Displays a shot delete confirmation message box.
     */
    public void showShotDeleteConfirmation()
    {
        ConfirmationDisplayer.display(shotDeletionString, areYouSureDeleteShotString,
                BreakdownEvents.SHOT_DELETE_CONFIRMED);
    }

    /**
     * Displays a sequence delete confirmation message box.
     */
    public void showSequenceDeleteConfirmation()
    {
        ConfirmationDisplayer.display(sequencesDeletionString, areYouSureDeleteSequenceString,
                BreakdownEvents.SHOT_SEQUENCE_DELETE_CONFIRMED);
    }

    /**
     * Expand parent given in parameter.
     * 
     * @param parent
     *            The parent to expand.
     */
    public void expandParent(RecordModelData parent)
    {
        this.shotTree.expand(parent);
    }

    /**
     * Set and configure sheet combo for retrieving only shot sheet for breakdown application and registers its store to
     * model.
     */
    private void setSheetCombo()
    {
        this.model.setSheetStore(this.sheetCombo.getServiceStore());

        this.sheetCombo.setType(ESheetType.BREAKDOWN);
        this.sheetCombo.addAvailableEntity(ShotModelData.SIMPLE_CLASS_NAME);
        this.sheetCombo.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                WorkObjectEvents.SHEET_CHANGED));
    }

    /**
     * Set navigation tree to west.
     */
    private void setTree()
    {
        this.shotTree.setBodyBorder(false);
        this.shotTree.setBorders(false);
        this.shotTree.getTree().setContextMenu(new ShotMenu(shotTree));
        this.addToWestPanel(shotTree);
    }

    // Actually it does not work. To implements later.
    private void setTreeFilter()
    {
        FilterStoreField<RecordModelData> filter = new FilterStoreField<RecordModelData>(shotTree.getStore());
        filter.setFieldLabel(ShotModelData.SHOT_LABEL);
        filter.setWidth(45);
        // this.explorerBar.add(new FillToolItem());
        // this.explorerBar.add(filter);
    }

    /**
     * Configure explorer with right field providers and configure sheet editor for shots only.
     */
    private void configureExplorer()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();
        EntityModelData entity = new EntityModelData();
        String entityName = ShotModelData.SIMPLE_CLASS_NAME;
        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        entities.add(entity);

        this.explorer.setColumnInfoProvider(new BreakdownColumnInfoProvider());
        this.explorer.setConstraintEditorProvider(new BreakdownEditorProvider());
        this.model.setExplorerStore(explorer.getStore(), ShotModelData.SIMPLE_CLASS_NAME.toLowerCase());
        this.model.setEntities(entities);
        this.explorer.setApplicationFilter(this.model.getFilter());
    }

    /**
     * Allow drag and drop between tree nodes and leaves.
     */
    private void setTreeDnd()
    {
        new TreePanelDragSource(shotTree.getTree());

        ShotBreakDropTarget target = new ShotBreakDropTarget(shotTree);
        target.setAllowSelfAsSource(true);
        target.setAllowDropOnLeaf(false);
        target.setOperation(Operation.MOVE);
    }

	/**
     * Enable navigation tree.
     */
	public void enableTree() 
	{
		this.shotTree.enable();
	}
	
    /**
     * Disable navigation tree.
     */
	public void disableTree() 
	{
		this.shotTree.disable();
	}
}
