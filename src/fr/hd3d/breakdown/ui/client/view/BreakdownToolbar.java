package fr.hd3d.breakdown.ui.client.view;

import java.util.Arrays;
import java.util.List;

import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.breakdown.ui.client.config.BreakdownImages;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.ProjectCombobox;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.AutoSaveButton;
import fr.hd3d.common.ui.client.widget.tageditor.TagEditor;
import fr.hd3d.common.ui.client.widget.tasktype.TaskTypeToolItem;


/**
 * Production view toolbar.
 */
public class BreakdownToolbar extends ToolBar
{

    /** Combo box needed to select project. */
    // private final ModelDataComboBox<ProjectModelData> projectCombo;
    private final ProjectCombobox projectCombo;
    /** Tools needed to set tags on shots and constituents. */
    private final TagEditor tagEditor = new TagEditor();

    /** Explorer auto-save button. */
    private final AutoSaveButton autoSaveButton = new AutoSaveButton();

    /** ods import button. */
    private final ToolBarButton odsImportButton = new ToolBarButton(BreakdownImages.getOdsImportIcon(),
            "Import data from ODS file", BreakdownEvents.ODS_IMPORT_CLICKED);

    /** Task type editor button. */
    private final TaskTypeToolItem tasktypeButton = new TaskTypeToolItem();

    /**
     * Default Constructor
     */
    public BreakdownToolbar(List<Button> buttons)
    {

        projectCombo = new ProjectCombobox();

        this.add(projectCombo);
        // this.add(new SeparatorToolItem());
        // this.add(tasktypeButton);
        this.add(new SeparatorToolItem());
//        this.add(odsImportButton);
//        this.add(new SeparatorToolItem());
        this.add(autoSaveButton);

        if (CollectionUtils.isNotEmpty(buttons))
        {
            this.add(new SeparatorToolItem());
            for (Button button : buttons)
            {
                this.add(button);
            }
        }

        this.add(new FillToolItem());
        this.add(tagEditor);
        tagEditor.hide();

        this.setStyles();
        this.registerListeners();
        this.configureProjectComboBox();
    }

    /**
     * Refresh tag editor with tag corresponding to selection. If selection contains more than one element, no tag is
     * displayed.
     * 
     * @param selection
     *            Explorer current selection.
     */
    public void refreshTagEditor(List<Hd3dModelData> selection)
    {
        this.tagEditor.refreshSelection(selection);
    }

    /**
     * Reload project combo box then select last selected project.
     */
    public void selectLastSelectedProject()
    {
        this.projectCombo.setData();
    }

    /** Set toolbar styles such as padding. */
    private void setStyles()
    {
        this.projectCombo.setWidth(200);
        this.setStyleAttribute("padding", "5px");
    }

    /** Register widget listeners. */
    private void registerListeners()
    {
        EventDispatcher.get().addController(tagEditor.getController());

        projectCombo.addSelectionChangedListener(new EventSelectionChangedListener<ProjectModelData>(
                CommonEvents.PROJECT_CHANGED));
    }

    /** Change combo box filtering to allow selection of project of which status is set to "waiting". */
    private void configureProjectComboBox()
    {
        this.projectCombo.getServiceStore().clearParameters();
        this.projectCombo.getServiceStore().addParameter(new OrderBy(ProjectModelData.NAME_FIELD));
        this.projectCombo.getServiceStore().addParameter(
                new InConstraint(ProjectModelData.PROJECT_STATUS, Arrays.asList(EProjectStatus.OPEN.toString(),
                        EProjectStatus.WAITING.toString())));
    }

    /**
     * Press auto save toggle button if it is not pressed.
     */
    public void toggleAutoSave()
    {
        this.autoSaveButton.toggle();
    }
}
