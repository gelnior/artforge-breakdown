package fr.hd3d.breakdown.ui.client.view.contextmenu;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;


public abstract class WorkObjectMenu extends EasyMenu
{
    protected MenuItem createLeaf;
    protected MenuItem createParent;
    protected MenuItem removeLeaf;
    protected MenuItem removeParent;
    protected MenuItem rename;
    protected final MenuItem refresh;

    /** Tree on which menu is set. */
    protected final BaseTree<?, ?> constituentTree;
    protected String leafClass;
    protected String parentClass;

    public WorkObjectMenu(final BaseTree<?, ?> tree)
    {
        this.constituentTree = tree;

        this.setMenuItems();

        this.refresh = new MenuItem();
        refresh.setText("Refresh");
        refresh.setIcon(Hd3dImages.getRefreshIcon());
        refresh.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                tree.refreshSelection();
            }
        });
    }

    protected abstract void setMenuItems();

    /**
     * Dynamically updates the menu items depending on elements selected.
     */
    @Override
    protected void onBeforeShow(BaseEvent be)
    {
        List<RecordModelData> selection = constituentTree.getSelection();

        removeAll();
        if (selection.size() == 1)
        {
            RecordModelData record = selection.get(0);

            if (this.leafClass.equals(record.getClassName()))
            {
                if (record.getUserCanUpdate())
                    add(rename);
                if (record.getUserCanDelete())
                    add(removeLeaf);
            }
            else if (record.getId() != null & record.getId().longValue() == -1L)
            {
                add(createParent);
                add(refresh);
            }
            else
            {
                add(createLeaf);
                add(createParent);
                if (record.getUserCanUpdate())
                    add(rename);
                if (record.getUserCanDelete())
                    add(removeParent);
                add(refresh);
            }
        }
        else if (selection.size() > 1)
        {
            boolean isOnlyCategories = true;
            for (RecordModelData record : selection)
            {
                if (this.leafClass.equals(record.getClassName()))
                {
                    isOnlyCategories = false;
                }
            }

            if (isOnlyCategories)
            {
                add(removeParent);
            }
            else
            {
                add(removeLeaf);
            }
        }

    }
}
