package fr.hd3d.breakdown.ui.client.view.contextmenu;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.layout.TableLayout;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.event.CastingEvents;
import fr.hd3d.common.ui.client.clipboard.ClipBoard;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ConfirmMessageBoxCallback;


/**
 * Casting context menu show all compositions from a list and it could remove a composition, change the number of
 * occurence of a composition, copy the selected composition or paste the clipboard.
 * 
 * @author HD3D
 * 
 */
public class CastingContextMenu extends EasyMenu
{

    /** List of composition panels. */
    private final ArrayList<CastingContextMenuPanel> panelList = new ArrayList<CastingContextMenuPanel>();

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    public static BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);

    private static final String DeleteChekedString = CONSTANTS.DeleteCheckedConstituents();
    private static final String CopyChekedString = CONSTANTS.CopyCheckedConstituents();

    private static final String DeleteAllSelectionString = "Delete all selection";

    private static final String RefreshAllDataString = "Refresh all data";

    private static final String pasteString = "Paste";

    private static final String NoDataToCopy = "No data to copy.";

    /** if multiple selection is true. */
    public final boolean isMultiSelection;

    /**
     * Default Constructor
     * 
     * @param isMultiSelection
     */
    public CastingContextMenu(boolean isMultiSelection)
    {
        showSeparator = false;
        this.isMultiSelection = isMultiSelection;
    }

    /**
     * Default Constructor
     * 
     */
    public CastingContextMenu()
    {
        this(false);
    }

    /**
     * Add the compositions list to the menu. It adds a casting Menu panel allowing to remove, change, copy or paste
     * compositions.
     * 
     * @param compositionList
     *            list of compositions
     */
    public void add(List<CompositionModelData> compositionList)
    {
        if (compositionList != null && !isMultiSelection)
        {
            addConstituents(compositionList);
        }

        this.add(new SeparatorMenuItem());

        boolean isListError = false;
        if (compositionList == null || compositionList.isEmpty())
        {
            isListError = true;
        }

        if (!isMultiSelection)
        {
            createCopyItem(isListError);
            createPasteItem(false);
            createDeleteItem(isListError);
            createRefreshItem();
        }
        else
        {
            createCopyItem(true);
            createPasteItem(false);
            createDeleteMulti(false);
        }

    }

    private void createDeleteMulti(boolean b)
    {
        this.addItem(DeleteAllSelectionString, CastingEvents.DELETE_KEYPRESS, Hd3dImages.getDeleteIcon());
    }

    private void createRefreshItem()
    {
        this.addItem(RefreshAllDataString, CastingEvents.REFRESH_CLICKED, Hd3dImages.getRefreshIcon());
    }

    private void createDeleteItem(boolean disabled)
    {
        MenuItem deleteItem = new MenuItem(DeleteChekedString, Hd3dImages.getDeleteIcon());
        deleteItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                ArrayList<CompositionModelData> compositionList = new ArrayList<CompositionModelData>();
                for (CastingContextMenuPanel panel : panelList)
                {
                    if (panel.getCheckBox().getValue())
                    {
                        compositionList.add(panel.getComposition());
                    }
                }
                MessageBox.confirm(COMMON_CONSTANTS.warning(), COMMON_CONSTANTS.YouAreGoingToDeleteData(),
                        new ConfirmMessageBoxCallback(CastingEvents.DELETE_COMPOSITION_CLICKED_FROM_CONTEXTMENU, null,
                                compositionList));
            }
        });
        this.add(deleteItem);

        if (disabled)
        {
            deleteItem.disable();
        }
    }

    private void createPasteItem(boolean disabled)
    {
        MenuItem paste = new MenuItem(pasteString, new SelectionListener<MenuEvent>() {

            @Override
            public void componentSelected(MenuEvent ce)
            {
                EventDispatcher.forwardEvent(CastingEvents.PASTE_KEYPRESS);
                hide();
            }

        });
        this.add(paste);
        if (ClipBoard.get().paste() == null)
        {
            paste.disable();
            paste.setTitle(NoDataToCopy);
        }
        else
        {
            paste.setTitle("");
        }
        this.add(paste);

        if (disabled)
        {
            paste.disable();
        }
    }

    private void createCopyItem(boolean disabled)
    {
        MenuItem copy = new MenuItem(CopyChekedString);
        copy.addSelectionListener(new SelectionListener<MenuEvent>() {

            @Override
            public void componentSelected(MenuEvent ce)
            {
                ArrayList<CompositionModelData> compositionList = new ArrayList<CompositionModelData>();
                for (CastingContextMenuPanel panel : panelList)
                {
                    if (panel.getCheckBox().getValue())
                    {
                        compositionList.add(panel.getComposition());
                    }
                }

                ClipBoard.get().copy(compositionList);

            }

        });
        this.add(copy);
        if (disabled)
        {
            copy.disable();
        }
    }

    private void addConstituents(List<CompositionModelData> compositionList)
    {
        ContentPanel panel = new ContentPanel();
        panel.setScrollMode(Scroll.AUTOY);
        panel.setAutoHeight(true);
        panel.setHeaderVisible(false);
        panel.setBodyBorder(false);
        panel.setBodyStyle("background-color:#F0F0F0;max-height:400px");

        TableLayout tableLayout = new TableLayout();
        tableLayout.setColumns(5);
        tableLayout.setCellSpacing(10);
        tableLayout.setInsertSpacer(true);

        panel.setLayout(tableLayout);

        this.add(new SeparatorMenuItem());

        for (CompositionModelData composition : compositionList)
        {
            CastingContextMenuPanel contextMenu = new CastingContextMenuPanel(composition);
            panel.add(contextMenu.getCheckBox());
            panel.add(contextMenu.getCompositionNameText());
            panel.add(contextMenu.getNbOccField());
            panel.add(contextMenu.getSaveButton());
            panel.add(contextMenu.getDelButton());
            panelList.add(contextMenu);
        }

        this.add(panel);
    }
}
