package fr.hd3d.breakdown.ui.client.view.contextmenu;

import com.google.gwt.core.client.GWT;

import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;


public class ConstituentMenu extends WorkObjectMenu
{

    /** Constant strings to display : dialog messages, button label... */
    public static BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);

    private static final String createConstituentString = CONSTANTS.CreateConstituent();
    private static final String createCategoryString = "Create category";
    private static final String deleteConstituentsString = CONSTANTS.DeleteConstituents();
    private static final String deleteCategoriesString = "Delete categories";

    public ConstituentMenu(ConstituentTree constituentTree)
    {
        super(constituentTree);

        this.leafClass = ConstituentModelData.CLASS_NAME;
        this.parentClass = CategoryModelData.CLASS_NAME;
    }

    @Override
    protected void setMenuItems()
    {
        this.createLeaf = makeNewItem(createConstituentString, BreakdownEvents.CONSTITUENT_CREATE_CLICKED, Hd3dImages
                .getAddIcon());
        this.createParent = makeNewItem(createCategoryString, BreakdownEvents.CONSTITUENT_CREATE_CATEGORY_CLICKED,
                Hd3dImages.getAddIcon());
        this.removeLeaf = makeNewItem(deleteConstituentsString, BreakdownEvents.CONSTITUENT_DELETE_CLICKED, Hd3dImages
                .getDeleteIcon());
        this.removeParent = makeNewItem(deleteCategoriesString, BreakdownEvents.CONSTITUENT_CATEGORY_DELETE_CLICKED,
                Hd3dImages.getDeleteIcon());

        this.rename = makeNewItem("Rename", BreakdownEvents.CONSTITUENT_RENAME_CLICKED, Hd3dImages.getEditIcon());
    }
}
