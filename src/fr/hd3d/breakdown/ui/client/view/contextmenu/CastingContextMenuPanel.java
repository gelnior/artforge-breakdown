package fr.hd3d.breakdown.ui.client.view.contextmenu;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.google.gwt.core.client.GWT;

import fr.hd3d.breakdown.ui.client.event.CastingEvents;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ConfirmMessageBoxCallback;


/**
 * Casting panel allows to remove, change, copy or paste a composition. It contains a check box, a label, a number
 * field, a save button and a remove button.
 * 
 * @author HD3D
 * 
 */
public class CastingContextMenuPanel
{

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Check box to selected the composition. */
    private final CheckBox check = new CheckBox();
    /** Number field containing the number of occurence. */
    private final NumberField nbOccField;
    /** Text containing the constituent name. */
    private final Text compositionNameText;
    /** Button to remove the composition. */
    private final Button delButton;
    /** Button to save the composition */
    private final Button saveButton;
    /** Composition represented by this panel. */
    private final CompositionModelData composition;

    /**
     * Default Constructor.
     * 
     * @param composition
     *            the composition represented by the panel.
     */
    public CastingContextMenuPanel(CompositionModelData composition)
    {
        this.composition = composition;

        compositionNameText = new Text(composition.getConstituentName());

        nbOccField = new NumberField();
        nbOccField.setWidth(40);
        nbOccField.setMinLength(1);
        nbOccField.setAllowBlank(false);
        nbOccField.setAllowDecimals(false);
        nbOccField.setAllowNegative(false);
        nbOccField.setValue(composition.getOcc());

        saveButton = new Button("", Hd3dImages.getSaveIcon());
        delButton = new Button("", Hd3dImages.getDeleteIcon());

        setListeners();
    }

    /**
     * Return the check box.
     * 
     * @return the check box.
     */
    public CheckBox getCheckBox()
    {
        return check;
    }

    /**
     * Return the number field.
     * 
     * @return the number field.
     */
    public NumberField getNbOccField()
    {
        return nbOccField;
    }

    /**
     * Return the composition name Text
     * 
     * @return the text
     */
    public Text getCompositionNameText()
    {
        return compositionNameText;
    }

    /**
     * Return the delete button.
     * 
     * @return the delete button.
     */
    public Button getDelButton()
    {
        return delButton;
    }

    /**
     * Return the save button.
     * 
     * @return the save button.
     */
    public Button getSaveButton()
    {
        return saveButton;
    }

    /**
     * Return the composition.
     * 
     * @return the composition.
     */
    public CompositionModelData getComposition()
    {
        return composition;
    }

    /**
     * Set listeners.
     */
    private void setListeners()
    {
        delButton.addSelectionListener(new SelectionListener<ButtonEvent>() {

            @Override
            public void componentSelected(ButtonEvent ce)
            {
                ArrayList<CompositionModelData> compositionList = new ArrayList<CompositionModelData>();
                compositionList.add(composition);
                MessageBox.confirm(COMMON_CONSTANTS.warning(), "Do you confirm data suppression ?",
                        new ConfirmMessageBoxCallback(CastingEvents.DELETE_COMPOSITION_CLICKED_FROM_CONTEXTMENU, null,
                                compositionList));
            }

        });

        saveButton.addSelectionListener(new SelectionListener<ButtonEvent>() {

            @Override
            public void componentSelected(ButtonEvent ce)
            {
                Number value = nbOccField.getValue();
                if (value == null)
                {
                    return;
                }
                composition.setOcc(value.intValue());
                AppEvent event = new AppEvent(CastingEvents.COMPOSITION_SAVE_SUCCESS);
                event.setData(CompositionModelData.CLASS_NAME, composition);
                composition.save(event);
            }

        });
    }
}
