package fr.hd3d.breakdown.ui.client.view.contextmenu;

import com.google.gwt.core.client.GWT;

import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;


public class ShotMenu extends WorkObjectMenu
{
    private static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    private static BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);

    private static final String createShotString = "Create shot";// CONSTANTS.CreateShot();
    private static final String createSequenceString = "Create sequence";// CONSTANTS.CreateSequence();
    private static final String deleteShotsString = "Delete shots";// CONSTANTS.DeleteShots();
    private static final String deleteSequenceString = "Delete sequences";// CONSTANTS.DeleteSequences();
    private static final String renameString = COMMON_CONSTANTS.Rename();

    public ShotMenu(ShotTree shotTree)
    {
        super(shotTree);

        this.leafClass = ShotModelData.CLASS_NAME;
        this.parentClass = SequenceModelData.CLASS_NAME;
    }

    @Override
    protected void setMenuItems()
    {
        this.createLeaf = makeNewItem(createShotString, BreakdownEvents.SHOT_CREATE_CLICKED, Hd3dImages.getAddIcon());
        this.createParent = makeNewItem(createSequenceString, BreakdownEvents.SHOT_CREATE_SEQUENCE_CLICKED,
                Hd3dImages.getAddIcon());
        this.removeLeaf = makeNewItem(deleteShotsString, BreakdownEvents.SHOT_DELETE_CLICKED,
                Hd3dImages.getDeleteIcon());
        this.removeParent = makeNewItem(deleteSequenceString, BreakdownEvents.SHOT_SEQUENCE_DELETE_CLICKED,
                Hd3dImages.getDeleteIcon());

        this.rename = makeNewItem(renameString, BreakdownEvents.SHOT_RENAME_CLICKED, Hd3dImages.getEditIcon());
    }

}
