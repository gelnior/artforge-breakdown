package fr.hd3d.breakdown.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.breakdown.ui.client.config.BreakdownConfig;
import fr.hd3d.breakdown.ui.client.config.BreakdownImages;
import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.constant.BreakdownMessages;
import fr.hd3d.breakdown.ui.client.controller.BreakdownController;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.model.BreakdownModel;
import fr.hd3d.breakdown.ui.client.view.column.BreakdownEditorProvider;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerModel;
import fr.hd3d.common.ui.client.widget.identitysheet.IdentitySheetWidget;
import fr.hd3d.common.ui.client.widget.mainview.CustomButtonInfos;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.common.ui.client.widget.tasktype.TaskTypeEditor;
import fr.hd3d.common.ui.client.widget.uploader.ODSUploader;


/**
 * BreakdownView organizes and connects breakdown main widgets.
 * 
 * @author HD3D
 */
public class BreakdownView extends MainView implements IBreakdownView
{

    /** Constant strings to display : dialog messages, button label... */
    public static BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static BreakdownMessages MESSAGES = GWT.create(BreakdownMessages.class);

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    private static final String ConstituentsShotsCreationString = CONSTANTS.ConstituentsShotsCreation();
    private static final String ODSbreakdownImportString = "ODS breakdown import";
    private static final String updatingAssetsString = "Updating assets";
    private static final String synchronizingAssetWithStepsString = "Synchronizing assets with steps...";
    private static final String updatingTasksString = "Updating tasks";
    private static final String synchronizingTaskWithStepsString = "Synchronizing tasks with steps...";

    /** Controller that handles main events. */
    private BreakdownController controller;
    /** Model handling application data. */
    private final BreakdownModel model = new BreakdownModel();

    /** Panel used for making tabs (constituents, shots...). */
    private final TabPanel tabPanel = new TabPanel();

    /** The panel hosting the application. */
    private final ContentPanel panel = new ContentPanel();
    /** Application main toolbar : project combobo box, auto save button... */
    private BreakdownToolbar toolbar;

    /** The constituent view composed of a navigation tree, an explorer and an identity sheet. */
    private ConstituentView constituentView;
    /** Tab containing constituent view. */
    private final TabItem constituentTabItem = new TabItem(COMMON_CONSTANTS.Constituents());
    /** The shot view composed of a navigation tree, an explorer and an identity sheet. */
    private ShotView shotView;
    /** Tab containing shot view. */
    private final TabItem shotTabItem = new TabItem(CONSTANTS.Shots());
    /** The shot view composed of a navigation tree, an explorer and an identity sheet. */
    private SequenceView sequenceView;
    /** Tab containing shot view. */
    private final TabItem sequenceTabItem = new TabItem("Sequences");

    /** Casting view that displays constituent linked to shots. */
    private CastingView castingView;
    /** Tab containing casting view. */
    private final TabItem castingTabItem = new TabItem("Casting");

    private ODSUploader odsUploader;

    /** Casting view that displays constituent linked to shots. */
    private CastingSequenceView castingSequenceView;
    /** Tab containing casting view. */

    private final TabItem castingSequenceTabItem = new TabItem(COMMON_CONSTANTS.SequenceCasting());

    private final TaskTypeEditor taskTypeEditor = new TaskTypeEditor();

    /**
     * Default constructor
     */
    public BreakdownView()
    {
        super("Breakdown");
    }

    @Override
    public void init()
    {
        controller = new BreakdownController(this, model);
        controller.addChild(taskTypeEditor.getController());

        EventDispatcher.get().addController(controller);
    }

    public void initWidgets()
    {
        this.toolbar = new BreakdownToolbar(this.getCustomButtons());
        this.panel.setTopComponent(toolbar);

        this.constituentView = new ConstituentView();
        this.constituentTabItem.setLayout(new FitLayout());
        this.constituentTabItem.add(constituentView);
        this.constituentTabItem.setIcon(Hd3dImages.getConsituentIcon());
        this.constituentTabItem.addListener(Events.Select, new EventBaseListener(
                BreakdownEvents.CONSTITUENT_TAB_CLICKED));

        this.shotView = new ShotView();
        this.shotTabItem.setLayout(new FitLayout());
        this.shotTabItem.add(shotView);
        this.shotTabItem.setIcon(Hd3dImages.getShotIcon());
        this.shotTabItem.addListener(Events.Select, new EventBaseListener(BreakdownEvents.SHOT_TAB_CLICKED));
        this.shotView.idle();
        
//        this.sequenceView = new SequenceView();
//        this.sequenceTabItem.setLayout(new FitLayout());
//        this.sequenceTabItem.add(sequenceView);
//        this.sequenceTabItem.setIcon(Hd3dImages.getSequenceIcon());
//        this.sequenceTabItem.addListener(Events.Select, new EventBaseListener(BreakdownEvents.SEQUENCE_TAB_CLICKED));
//        this.sequenceView.idle();

        this.castingView = new CastingView();
        this.castingTabItem.setLayout(new FitLayout());
        this.castingTabItem.add(castingView);
        this.castingTabItem.setIcon(BreakdownImages.getCastingIcon());
        this.castingTabItem.addListener(Events.Select, new EventBaseListener(BreakdownEvents.CASTING_TAB_CLICKED));
        // castingView.idle();

        this.castingSequenceView = new CastingSequenceView();
        this.castingSequenceTabItem.setLayout(new FitLayout());
        this.castingSequenceTabItem.add(castingSequenceView);
        this.castingSequenceTabItem.setIcon(BreakdownImages.getCastingIcon());
        this.castingSequenceTabItem.addListener(Events.Select, new EventBaseListener(
                BreakdownEvents.CASTING_SEQUENCE_TAB_CLICKED));

        this.tabPanel.add(constituentTabItem);
        this.tabPanel.add(shotTabItem);
//        this.tabPanel.add(sequenceTabItem);
        this.tabPanel.add(castingTabItem);
        this.tabPanel.add(castingSequenceTabItem);

        this.panel.setLayout(new FitLayout());
        this.panel.setHeaderVisible(false);
        this.panel.setBorders(false);
        this.panel.setBodyBorder(false);
        this.panel.add(tabPanel);
        this.addToViewport(panel);

        // Identity sheet configuration for Breakdown application.
        IdentitySheetWidget.itemInfoProvider = new BreakdownEditorProvider();
        ExplorerModel.setIsIdentityDialogEnabled(true);

        RootPanel.get().add(loadingPanel);
        odsUploader = new ODSUploader(BreakdownEvents.FILE_SUBMITTED);
    }

    public void setControllers()
    {
        controller.addChild(constituentView.getController());
        controller.addChild(shotView.getController());
//        controller.addChild(sequenceView.getController());
        controller.addChild(castingView.getController());
        controller.addChild(castingSequenceView.getController());
    }

    public void rebuildLastEnvironment()
    {
        String cookieTab = FavoriteCookie.getFavParameterValue(BreakdownConfig.TAB_COOKIE_VAR);
        if (ShotView.NAME.equals(cookieTab))
        {
            tabPanel.setSelection(shotTabItem);
        }
        else if (SequenceView.NAME.equals(cookieTab))
        {
            tabPanel.setSelection(sequenceTabItem);
        }
        else if (CastingView.NAME.equals(cookieTab))
        {
            tabPanel.setSelection(castingTabItem);
        }
        else if (CastingSequenceView.NAME.equals(cookieTab))
        {
            tabPanel.setSelection(castingSequenceTabItem);
        }

        this.selectLastProject();
    }

    public void refreshTagEditor(List<Hd3dModelData> selection)
    {
        this.toolbar.refreshTagEditor(selection);
    }

    public void selectLastProject()
    {
        this.toolbar.selectLastSelectedProject();
    }

    public void idleConstituentView()
    {
        this.constituentView.idle();
    }

    public void unidleConstituentView()
    {
        this.constituentView.unidle();
    }

    public void idleShotView()
    {
        this.shotView.idle();
    }

    public void unidleShotView()
    {
        this.shotView.unidle();
    }
    
    public void idleSequenceView()
    {
//        this.sequenceView.idle();
    }

    public void unidleSequenceView()
    {
        this.sequenceView.unidle();
    }

    public void saveTabToCookie(String tabName)
    {
        FavoriteCookie.putFavValue(BreakdownConfig.TAB_COOKIE_VAR, tabName);
    }

    public void showTaskLongLoading()
    {
        this.loadingPanel.setText(updatingTasksString, synchronizingTaskWithStepsString);
        this.loadingPanel.show();
    }

    public void showAssetLongLoading()
    {
        this.loadingPanel.setText(updatingAssetsString, synchronizingAssetWithStepsString);
        this.loadingPanel.show();
    }

    public void showOdsImportLongLoading()
    {
        this.loadingPanel.setText(ODSbreakdownImportString, ConstituentsShotsCreationString);
        this.loadingPanel.show();
    }

    public void hideLoading()
    {
        this.loadingPanel.hide();
    }

    public void displayOdsUploader()
    {
        this.odsUploader.show();
    }

    public void toggleAutoSave()
    {
        this.toolbar.toggleAutoSave();
    }

    public void displayTaskTypeEditor()
    {
        this.taskTypeEditor.show();
    }

    private List<Button> getCustomButtons()
    {
        List<CustomButtonInfos> customButtonInfos = this.model.getCustomButtons();
        List<Button> customButtons = new ArrayList<Button>();
        for (CustomButtonInfos info : customButtonInfos)
        {
            customButtons.add(info.asButton());
        }
        return customButtons;
    }

}
