package fr.hd3d.breakdown.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.CardLayout;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.breakdown.ui.client.controller.CastingController;
import fr.hd3d.breakdown.ui.client.event.BreakdownEvents;
import fr.hd3d.breakdown.ui.client.model.CastingModel;
import fr.hd3d.breakdown.ui.client.view.dnd.CastingDropTarget;
import fr.hd3d.breakdown.ui.client.view.dnd.ConstituentTreeDragSource;
import fr.hd3d.breakdown.ui.client.view.dnd.ShotTreeDragSource;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.ToolBarToggleButton;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.RatingCellRenderer;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;


/**
 * Casting view displays list of constituent casts into a shot. It displays also shot in which a constituent appears. To
 * displays lists, user browses navigation trees. For editing casting, user drag and drop data from navigation trees. To
 * avoid errors two mode are available : shot mode and constituent mode.
 * 
 * @author HD3D
 */
public class CastingView extends BorderedPanel
{
    public static final String NAME = "casting";

    /** Model handling casting data. */
    private final CastingModel model = new CastingModel();
    /** Controller handling casting events. */
    private final CastingController controller = new CastingController(this, model);

    /** Constituent navigation tree. */
    private final ConstituentTree constituentTree = new ConstituentTree();
    /** Shot navigation tree. */
    private final ShotTree shotTree = new ShotTree();

    /** Grid displaying constituents linked to selected shot. */
    private final BaseGrid<ConstituentModelData> constituentGrid = new BaseGrid<ConstituentModelData>(
            this.model.getConstituentStore(), this.getConstituentColumnModel());
    /** Grid displaying shots linked to selected shot. */
    private final BaseGrid<ShotModelData> shotGrid = new BaseGrid<ShotModelData>(this.model.getShotStore(),
            this.getShotColumnModel());
    /** Card Layout used to switch between constituent grid and shot grid. */
    private final CardLayout gridLayout = new CardLayout();

    /** View toolbar. */
    private final ToolBar gridToolBar = new ToolBar();

    /** Constituent button to pass in constituent mode. */
    private final ToolBarToggleButton constituentButton = new ToolBarToggleButton(Hd3dImages.getConstituentIcon(),
            "Constituent mode", BreakdownEvents.CONSTITUENT_MODE_CLICKED);
    /** Shot button to pass in shot mode. */
    private final ToolBarToggleButton shotButton = new ToolBarToggleButton(Hd3dImages.getShotIcon(), "Shot mode",
            BreakdownEvents.SHOT_MODE_CLICKED);

    private final Text workObjectNameText = new Text();

    /** @return view controller. Useful for hierarchical registering. */
    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Constructor : set layout.
     */
    public CastingView()
    {
        this.setGrids();
        this.setTrees();
        this.setGridToolBar();
        this.setDragNDrop();
    }

    /** Freeze the view. */
    public void idle()
    {
        this.controller.mask();
    }

    /** Unfreeze the view. */
    public void unidle()
    {
        this.controller.unMask();
    }

    /** Display constituent list and hide shot list. */
    public void displayConstituentList()
    {
        this.gridLayout.setActiveItem(constituentGrid);
    }

    /** Display shot list and hide constituent list. */
    public void displayShotList()
    {
        this.gridLayout.setActiveItem(shotGrid);
    }

    /** Disable constituent mode button, then press shot button. */
    public void disableConstituentModeButton()
    {
        this.constituentButton.disable();
        this.shotButton.enable();
        this.shotButton.toggle();
    }

    /** Disable shot mode button, then press constituent button. */
    public void disableShotModeButton()
    {
        this.shotButton.disable();
        this.constituentButton.enable();
        this.constituentButton.toggle();
    }

    /**
     * Change displayed current work object name.
     * 
     * @param name
     *            The name to set.
     * @param nbFrame
     */
    public void setWorkObjectName(String name, Integer nbFrame)
    {
        if (nbFrame != null)
        {
            name += " (" + nbFrame + " frame";
            if (nbFrame > 1)
                name += "s";
            name += ")";
        }
        this.workObjectNameText.setText(name);
    }

    /** Set drag and drop between navigation trees and casting grids. */
    private void setDragNDrop()
    {
        new ConstituentTreeDragSource(this.constituentTree.getTree());

        CastingDropTarget target = new CastingDropTarget(this.constituentGrid);
        target.setFeedback(Feedback.APPEND);
        target.setOperation(Operation.COPY);

        new ShotTreeDragSource(this.shotTree.getTree());

        CastingDropTarget shotTarget = new CastingDropTarget(this.shotGrid);
        shotTarget.setFeedback(Feedback.APPEND);
        shotTarget.setOperation(Operation.COPY);
    }

    /** Set toolbar upon casting grid to handle mode selection and displays current work object name. */
    private void setGridToolBar()
    {
        this.gridToolBar.add(constituentButton);
        this.gridToolBar.add(shotButton);
        this.gridToolBar.add(new SeparatorToolItem());
        this.gridToolBar.add(workObjectNameText);

        this.workObjectNameText.setStyleAttribute("font-weight", "bold");
        this.workObjectNameText.setStyleAttribute("font-size", "20px");

        this.shotButton.toggle();
        this.shotButton.disable();
    }

    /**
     * Set data grid to the center of the view.
     */
    private void setGrids()
    {
        ContentPanel gridPanel = new ContentPanel();
        gridPanel.setLayout(gridLayout);

        gridPanel.add(constituentGrid);
        gridPanel.add(shotGrid);
        gridPanel.setHeaderVisible(false);
        gridPanel.setTopComponent(gridToolBar);

        this.constituentGrid.setAutoWidth(true);
        this.constituentGrid.setGroupingOn();

        EasyMenu menu = this.constituentGrid.getMenu();
        menu.removeAll();
        menu.addItem("Remove", BreakdownEvents.CASTING_REMOVE_CLICKED, Hd3dImages.getDeleteIcon());
        menu.addItem("Refresh", BreakdownEvents.CASTING_REFRESH_CLICKED, Hd3dImages.getRefreshIcon());

        this.shotGrid.setAutoWidth(true);
        this.shotGrid.setGroupingOn();

        menu = this.shotGrid.getMenu();
        menu.removeAll();
        menu.addItem("Remove", BreakdownEvents.CASTING_REMOVE_CLICKED, Hd3dImages.getDeleteIcon());
        menu.addItem("Refresh", BreakdownEvents.CASTING_REFRESH_CLICKED, Hd3dImages.getRefreshIcon());

        this.addCenter(gridPanel);
        this.gridLayout.setActiveItem(constituentGrid);
    }

    /**
     * Set navigation trees to the west of the view.
     */
    private void setTrees()
    {
        BorderedPanel treePanel = new BorderedPanel();
        treePanel.addNorth(constituentTree, 300, 0, 0, 0, 0);
        treePanel.addCenter(shotTree, 5, 0, 0, 0);

        this.addWest(treePanel, 200);

        this.constituentTree.setLocalEventsOn();
        this.constituentTree.addController(this.controller);
        this.shotTree.setLocalEventsOn();
        this.shotTree.addController(this.controller);

        this.model.setShotNavModel(this.shotTree.getTreeModel());
        this.model.setConstituentNavModel(this.constituentTree.getTreeModel());
    }

    /**
     * @return Constituent grid column list.
     */
    private ColumnModel getConstituentColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        RatingCellRenderer ratingRenderer = new RatingCellRenderer();
        ratingRenderer.setEditable(false);

        GridUtils.addColumnConfig(columns, ConstituentModelData.CONSTITUENT_CATEGORIESNAMES, "Category", 150);
        GridUtils.addColumnConfig(columns, ConstituentModelData.CONSTITUENT_LABEL, "Name", 150);
        GridUtils.addThumbnailColumn(columns);
        GridUtils.addColumnConfig(columns, ConstituentModelData.CONSTITUENT_DESCRIPTION, "Description", 400);

        return new ColumnModel(columns);
    }

    /**
     * @return Constituent grid column list.
     */
    private ColumnModel getShotColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        RatingCellRenderer ratingRenderer = new RatingCellRenderer();
        ratingRenderer.setEditable(false);

        GridUtils.addColumnConfig(columns, ShotModelData.SHOT_SEQUENCESNAMES, "Sequence", 200);
        GridUtils.addColumnConfig(columns, ShotModelData.SHOT_LABEL, "Name", 150);
        GridUtils.addColumnConfig(columns, ShotModelData.SHOT_NBFRAME, "Frames", 50);
        GridUtils.addThumbnailColumn(columns);
        GridUtils.addColumnConfig(columns, ShotModelData.SHOT_DESCRIPTION, "Description", 400);

        return new ColumnModel(columns);
    }

    public List<ConstituentModelData> getConstituentSelection()
    {
        return this.constituentGrid.getSelectionModel().getSelectedItems();
    }

    public List<ShotModelData> getShotSelection()
    {
        return this.shotGrid.getSelectionModel().getSelectedItems();
    }
}
