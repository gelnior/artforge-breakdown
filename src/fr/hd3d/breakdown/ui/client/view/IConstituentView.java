package fr.hd3d.breakdown.ui.client.view;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


public interface IConstituentView
{

    /** View name to tab distinction purpose. */
    public static final String NAME = "constituent";

    /**
     * @return view controller
     */
    public abstract MaskableController getController();

    /**
     * Refresh navigation tree for given project.
     * 
     * @param project
     *            The project used to set data inside tree.
     */
    public abstract void reloadTree(ProjectModelData project);

    /**
     * Idle every widget contains inside view and mask view controller.
     */
    public abstract void idle();

    /**
     * Unidle every widget contains inside view and unmask view controller.
     */
    public abstract void unidle();

    /**
     * @return Tree whole constituent tree selection.
     */
    public abstract List<RecordModelData> getTreeSelection();

    /**
     * Show delete constituents delete confirmation box.
     */
    public abstract void showConstituentDeleteConfirmation();

    /**
     * Show delete categories delete confirmation box.
     */
    public abstract void showCategoryDeleteConfirmation();

    /**
     * Expand parent given in parameter.
     * 
     * @param parent
     *            The parent to expand.
     */
    public abstract void expandParent(RecordModelData parent);

}
