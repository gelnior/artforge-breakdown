package fr.hd3d.breakdown.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.GXT;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.dnd.DropTarget;
import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.HeaderGroupConfig;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel.Cell;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;

import fr.hd3d.breakdown.ui.client.constant.BreakdownConstants;
import fr.hd3d.breakdown.ui.client.controller.CastingSequenceController;
import fr.hd3d.breakdown.ui.client.event.CastingEvents;
import fr.hd3d.breakdown.ui.client.model.CastingModelData;
import fr.hd3d.breakdown.ui.client.model.CastingSequenceModel;
import fr.hd3d.breakdown.ui.client.summaryType.CastingSummaryType;
import fr.hd3d.breakdown.ui.client.view.contextmenu.CastingContextMenu;
import fr.hd3d.breakdown.ui.client.view.dnd.CastingSequenceDropTarget;
import fr.hd3d.breakdown.ui.client.view.renderer.CastingCellRenderer;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.util.hotkeys.HotKeysUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.ColumnHiddenChangeListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.ColumnWidthChangeListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ThumbnailRenderer;
import fr.hd3d.common.ui.client.widget.grid.BaseEditorGrid;
import fr.hd3d.common.ui.client.widget.grid.MultiLineCellSelectionModel;
import fr.hd3d.common.ui.client.widget.grid.SelectableGridView;
import fr.hd3d.common.ui.client.widget.grid.editor.TextAreaEditor;
import fr.hd3d.common.ui.client.widget.grid.renderer.LongTextRenderer;
import fr.hd3d.common.ui.client.widget.workobjecttree.ConstituentTree;
import fr.hd3d.common.ui.client.widget.workobjecttree.ShotTree;


/**
 * Casting view displays list of constituent casts into a shot. It displays also constituents which appears in a shot.
 * To displays lists, user browses shot navigation tree. For editing casting, user drag and drop data from constituent
 * navigation tree or use the context menu.
 * 
 * @author HD3D
 */
public class CastingSequenceView extends BorderedPanel implements ICastingSequenceView
{
    public static final String NAME = "castingSequence";

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Constant strings to display : dialog messages, button label... */
    public static BreakdownConstants CONSTANTS = GWT.create(BreakdownConstants.class);

    private static final String descriptionString = "description";
    private static final String sequenceString = COMMON_CONSTANTS.Sequence();
    private static final String shotString = COMMON_CONSTANTS.Shot();
    private static final String nbFrameString = CONSTANTS.NbFrame();
    private static final String numbersOfShotsString = CONSTANTS.NumberOfShots();
    private static final String totalString = "Total";
    private static final String totalDistinctItemsString = "Total of distinct items";
    private static final String averagePerShotString = CONSTANTS.AveragePerShot();
    private static final String categoryString = "Category";
    private static final String impossibleToCopyString = "Impossible to copy in the copyBoard.";
    private static final String impossibleToDeleteString = "Impossible to delete.";

    /** Model handling casting data. */
    private final CastingSequenceModel model = new CastingSequenceModel();
    /** Controller handling casting events. */
    private final CastingSequenceController controller = new CastingSequenceController(this, model);

    /** Constituent navigation tree. */
    private final ConstituentTree constituentTree = new ConstituentTree();
    /** Shot navigation tree. */
    private final ShotTree shotTree = new ShotTree("");

    private final ToolBarButton exportCastingCsvButton = new ToolBarButton(Hd3dImages.getCsvExportIcon(),
            "Export casting to csv file", CastingEvents.EXPORT_CASTING_CSV);

    /** Grid containing casting data. */
    private final BaseEditorGrid<CastingModelData> castingGrid = new BaseEditorGrid<CastingModelData>(this.model
            .getCastingStore(), this.getCastingColumnModel());

    /** Progress bar while saving or deleting compositions. */
    private MessageBox progress;

    /**
     * Constructor : set layout.
     */
    public CastingSequenceView()
    {
        this.setGrids();
        this.setTrees();
        this.setListener();
        this.setDragNDrop();
        this.setHotKeys();
    }

    /**
     * Set listener to the grid.
     */
    private void setListener()
    {
        this.castingGrid.addListener(Events.ContextMenu, new Listener<GridEvent<CastingModelData>>() {
            public void handleEvent(GridEvent<CastingModelData> ge)
            {
                Menu menu = getContextMenu(ge.getModel(), ge.getColIndex(), isMultiSelectionGrid());

                if (menu == null)
                {
                    return;
                }

                menu.showAt(ge.getClientX(), ge.getClientY());
                ge.setCancelled(true);
            }
        });
        this.castingGrid.addListener(Events.AfterEdit, new Listener<GridEvent<CastingModelData>>() {
            public void handleEvent(GridEvent<CastingModelData> be)
            {
                if (be.getRowIndex() == -1)
                {
                    return;
                }

                CastingModelData casting = model.getCastingStore().getAt(be.getRowIndex());
                if (casting == null)
                {
                    return;
                }
                EventDispatcher.forwardEvent(CastingEvents.AFTER_EDIT, casting);
            }
        });
        this.castingGrid.setClicksToEdit(ClicksToEdit.TWO);
        this.castingGrid.getView().setEmptyText("Select a sequence with shots...");
    }

    /**
     * Return the context menu depending to casting and selected column.
     * 
     * @param casting
     *            the selected casting
     * @param colIndex
     *            the selected column
     * @param b
     * @return
     */
    private Menu getContextMenu(CastingModelData casting, int colIndex, boolean isMultiSelection)
    {
        if (colIndex < 0 || casting == null)
        {
            Logger.log("CastingView - getContextMenu() : The casting or the colIndex is not correct : casting = "
                    + casting + ", colIndex = " + colIndex);
            return null;
        }
        ColumnConfig columnConfig = castingGrid.getColumnModel().getColumn(colIndex);
        if (columnConfig == null)
        {
            Logger.log("CastingView - getContextMenu() : There are not column at this index : " + colIndex);
            return null;
        }
        List<CompositionModelData> constituentList = casting.getCompositions(columnConfig.getId().toString());

        CastingContextMenu menu = new CastingContextMenu(isMultiSelection);
        menu.add(constituentList);

        return menu;
    }

    /**
     * Set the hot keys.
     */
    private void setHotKeys()
    {
        HotKeysUtils.addHotKeys(HotKeysUtils.CTRL_C, CastingEvents.COPY_KEYPRESS);
        HotKeysUtils.addHotKeys(HotKeysUtils.CTRL_V, CastingEvents.PASTE_KEYPRESS);
        HotKeysUtils.addHotKeys(HotKeysUtils.SUPPR, CastingEvents.DELETE_KEYPRESS);
    }

    /**
     * Set the drag source on the constituent tree and the drop target on the casting grid.
     */
    private void setDragNDrop()
    {
        new TreePanelDragSource(constituentTree.getTree());

        DropTarget target = new CastingSequenceDropTarget(castingGrid);
        target.addDNDListener(controller.getDNDListener());
        target.setOperation(Operation.COPY);
    }

    /**
     * Set data grid to the center of the view.
     */
    private void setGrids()
    {
        this.castingGrid.setView(new SelectableGridView());

        MultiLineCellSelectionModel<CastingModelData> cellSelection = new MultiLineCellSelectionModel<CastingModelData>(
                (SelectableGridView) this.castingGrid.getView(), null);

        this.castingGrid.setSelectionModel(cellSelection);

        BorderedPanel centerPanel = new BorderedPanel();
        centerPanel.setToolBar();
        centerPanel.addToToolBar(exportCastingCsvButton);
        centerPanel.addCenter(this.castingGrid);
        centerPanel.setHeaderVisible(false);
        centerPanel.setLayout(new FitLayout());

        centerPanel.setBorders(true);
        this.addCenter(centerPanel);
    }

    /**
     * Set navigation trees to the west of the view.
     */
    private void setTrees()
    {
        BorderedPanel treePanel = new BorderedPanel();
        treePanel.addNorth(constituentTree, 300, 0, 0, 0, 0);
        treePanel.addCenter(shotTree, 5, 0, 0, 0);

        this.addWest(treePanel, 200);

        this.constituentTree.setLocalEventsOn();
        this.constituentTree.addController(this.controller);
        this.shotTree.setLocalEventsOn();
        this.shotTree.addController(this.controller);

        this.model.setShotNavModel(this.shotTree.getTreeModel());
        this.model.setConstituentNavModel(this.constituentTree.getTreeModel());
    }

    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Reconfigure the casting grid depending the categories.
     */
    public void reconfigureGrid()
    {
        castingGrid.reconfigure(this.model.getCastingStore(), getCastingColumnModel());
    }

    /**
     * Return the casting grid column model depending the categories of constituents.
     * 
     * @return Casting grid column list.
     */
    public ColumnModel getCastingColumnModel()
    {
        ArrayList<ColumnConfig> columnList = new ArrayList<ColumnConfig>();
        GridUtils.addColumnConfig(columnList, CastingModelData.SEQUENCES_NAMES, sequenceString);
        ColumnConfig shotColumn = GridUtils.addColumnConfig(columnList, CastingModelData.SHOT_NAME, shotString, 120);
        shotColumn.setGroupable(false);

        ColumnConfig thumbnailColumn = GridUtils.addColumnConfig(columnList, CastingModelData.CLASS_NAME, "Thumbnail",
                80);
        thumbnailColumn.setRenderer(new ThumbnailRenderer<ShotModelData>());
        thumbnailColumn.setGroupable(false);

        ColumnConfig descriptColumn = GridUtils.addColumnConfig(columnList, CastingModelData.SHOT_DESCRIPTION,
                descriptionString, 80);
        descriptColumn.setEditor(new TextAreaEditor());
        descriptColumn.setRenderer(new LongTextRenderer<CastingModelData>());
        descriptColumn.setGroupable(false);

        ColumnConfig nbFrameColumn = GridUtils.addColumnConfig(columnList, CastingModelData.SHOT_NBFRAME,
                nbFrameString, 70);
        NumberField field = new NumberField();
        field.setPropertyEditorType(Integer.class);
        nbFrameColumn.setEditor(new CellEditor(field));
        nbFrameColumn.setAlignment(HorizontalAlignment.CENTER);
        nbFrameColumn.setGroupable(false);

        ColumnConfig startFrameColumn = GridUtils.addColumnConfig(columnList, CastingModelData.SHOT_STARTFRAME,
                "Start frame", 70);
        field = new NumberField();
        field.setPropertyEditorType(Integer.class);
        startFrameColumn.setEditor(new CellEditor(field));
        startFrameColumn.setAlignment(HorizontalAlignment.CENTER);
        startFrameColumn.setGroupable(false);

        ColumnConfig endFrameColumn = GridUtils.addColumnConfig(columnList, CastingModelData.SHOT_ENDFRAME,
                "End frame", 70);
        field = new NumberField();
        field.setPropertyEditorType(Integer.class);
        endFrameColumn.setEditor(new CellEditor(field));
        endFrameColumn.setAlignment(HorizontalAlignment.CENTER);
        endFrameColumn.setGroupable(false);

        for (CategoryModelData category : this.model.getCategoriesRootStore().getModels())
        {
            ColumnConfig categoryColumn = GridUtils.addColumnConfig(columnList, category.getId().toString(), category
                    .getName(), 200);
            categoryColumn.setAlignment(HorizontalAlignment.CENTER);
            categoryColumn.setRenderer(new CastingCellRenderer());
            categoryColumn.setGroupable(false);
            categoryColumn.setSortable(false);
        }
        ColumnModel columnModel = new ColumnModel(columnList);

        AggregationRowConfig<CastingModelData> nbShot = new AggregationRowConfig<CastingModelData>();
        nbShot.setHtml(CastingModelData.SHOT_NAME, numbersOfShotsString);
        nbShot.setSummaryType(CastingModelData.SHOT_NBFRAME, SummaryType.COUNT);
        nbShot.setSummaryFormat(CastingModelData.SHOT_NBFRAME, NumberFormat.getDecimalFormat());
        columnModel.addAggregationRow(nbShot);

        AggregationRowConfig<CastingModelData> somme = new AggregationRowConfig<CastingModelData>();
        somme.setHtml(CastingModelData.SHOT_NAME, totalString);
        somme.setSummaryType(CastingModelData.SHOT_NBFRAME, SummaryType.SUM);
        somme.setSummaryFormat(CastingModelData.SHOT_NBFRAME, NumberFormat.getDecimalFormat());
        columnModel.addAggregationRow(somme);

        AggregationRowConfig<CastingModelData> different = new AggregationRowConfig<CastingModelData>();
        different.setHtml(CastingModelData.SHOT_NAME, totalDistinctItemsString);
        different.setSummaryFormat(CastingModelData.SHOT_NBFRAME, NumberFormat.getDecimalFormat());
        columnModel.addAggregationRow(different);

        AggregationRowConfig<CastingModelData> moyenne = new AggregationRowConfig<CastingModelData>();
        moyenne.setHtml(CastingModelData.SHOT_NAME, averagePerShotString);
        moyenne.setSummaryType(CastingModelData.SHOT_NBFRAME, SummaryType.AVG);
        moyenne.setSummaryFormat(CastingModelData.SHOT_NBFRAME, NumberFormat.getDecimalFormat());
        columnModel.addAggregationRow(moyenne);

        for (CategoryModelData category : this.model.getCategoriesRootStore().getModels())
        {
            nbShot.setSummaryType(category.getId().toString(), CastingSummaryType.COUNT_CASTING);
            nbShot.setSummaryFormat(category.getId().toString(), NumberFormat.getDecimalFormat());

            somme.setSummaryType(category.getId().toString(), CastingSummaryType.SUM_CASTING);
            somme.setSummaryFormat(category.getId().toString(), NumberFormat.getDecimalFormat());

            different.setSummaryType(category.getId().toString(), CastingSummaryType.DIFFERENT_CASTING);
            different.setSummaryFormat(category.getId().toString(), NumberFormat.getDecimalFormat());

            moyenne.setSummaryType(category.getId().toString(), CastingSummaryType.AVERAGE_CASTING);
            moyenne.setSummaryFormat(category.getId().toString(), NumberFormat.getDecimalFormat());
        }

        columnModel.addHeaderGroup(0, 0, new HeaderGroupConfig(shotString, 1, 7));
        columnModel.addHeaderGroup(0, 7, new HeaderGroupConfig(categoryString, 1, this.model.getCategoriesRootStore()
                .getCount()));

        setUserSetting(columnModel);
        setColumnModelListener(columnModel);
        return columnModel;
    }

    /**
     * Set the user settings for each column.
     * 
     * @param columnModel
     *            the column model.
     */
    private void setUserSetting(ColumnModel columnModel)
    {
        if (this.model.getSelectedProject() == null)
        {
            return;
        }
        String beginKey = "breakdown.casting.project." + this.model.getSelectedProject().getId() + ".item.";
        String columnWidth = ":width";
        String columnHidden = ":hidden";
        for (ColumnConfig config : columnModel.getColumns())
        {
            String width = UserSettings.getSetting(beginKey + config.getId() + columnWidth);
            if (width != null)
            {
                config.setWidth(Integer.parseInt(width));
            }
            String hidden = UserSettings.getSetting(beginKey + config.getId() + columnHidden);
            if (hidden != null)
            {
                config.setHidden(Boolean.parseBoolean(hidden));
            }
        }
    }

    /**
     * Set the listener on the column model to save the user settings
     * 
     * @param columnModel
     */
    private void setColumnModelListener(ColumnModel columnModel)
    {
        columnModel.addListener(Events.WidthChange, new ColumnWidthChangeListener());
        columnModel.addListener(Events.HiddenChange, new ColumnHiddenChangeListener());
    }

    /**
     * Mask with the loading panel.
     */
    public void maskLoading()
    {
        this.castingGrid.mask(GXT.MESSAGES.loadMask_msg());
    }

    /**
     * Unmask with the loading panel.
     */
    public void unmaskLoading()
    {
        this.castingGrid.unmask();
    }

    /**
     * Return the compositions which is selected.
     * 
     * @return the compositions. Return null, if no selected row.
     */
    public List<CompositionModelData> getCompositionSelectedGrid()
    {

        MultiLineCellSelectionModel<CastingModelData> selection = (MultiLineCellSelectionModel<CastingModelData>) this.castingGrid
                .getSelectionModel();

        List<Cell> cellList = selection.getSeletedCells();
        if (cellList == null || cellList.isEmpty())
        {
            return null;
        }
        ArrayList<CompositionModelData> compositionList = new ArrayList<CompositionModelData>();
        for (Cell cell : cellList)
        {
            ColumnConfig columnConfig = castingGrid.getColumnModel().getColumn(cell.cell);
            CastingModelData casting = this.model.getCastingStore().getAt(cell.row);
            if (casting == null)
            {
                continue;
            }
            compositionList.addAll(casting.getCompositions(columnConfig.getId().toString()));
        }
        return compositionList;
    }

    /**
     * Return the selected cells.
     * 
     * @return the selected cells. Return null, if no selected row.
     */
    public List<Cell> getCellsSelected()
    {
        MultiLineCellSelectionModel<CastingModelData> selection = (MultiLineCellSelectionModel<CastingModelData>) this.castingGrid
                .getSelectionModel();
        List<Cell> cellList = selection.getSeletedCells();
        return cellList;
    }

    /**
     * Return the selected castings.
     * 
     * @return the selected casting. Return null, if no selected row.
     */
    public List<CastingModelData> getCastingSelected()
    {
        MultiLineCellSelectionModel<CastingModelData> selection = (MultiLineCellSelectionModel<CastingModelData>) this.castingGrid
                .getSelectionModel();
        List<Cell> cellList = selection.getSeletedCells();
        if (cellList == null || cellList.isEmpty())
        {
            return null;
        }
        ArrayList<CastingModelData> castings = new ArrayList<CastingModelData>();
        for (Cell cell : cellList)
        {
            CastingModelData casting = this.model.getCastingStore().getAt(cell.row);
            if (casting == null)
            {
                continue;
            }
            castings.add(casting);
        }
        return castings;
    }

    /**
     * Update the row.
     * 
     * @param index
     *            the row index.
     */
    public void updateRow(int index)
    {
        this.castingGrid.getStore().update(this.castingGrid.getStore().getAt(index));
    }

    /**
     * Show the message box when a error get while the copy.
     */
    public void showMessageCopyError()
    {
        MessageBox.alert(COMMON_CONSTANTS.warning(), impossibleToCopyString, null);
    }

    /**
     * Show the message box when a error get while the delete.
     */
    public void showMessageDeleteError()
    {
        MessageBox.alert(COMMON_CONSTANTS.warning(), impossibleToDeleteString, null);
    }

    /**
     * Return the column id of the column selected.
     * 
     * @return the column id. Return null, if no selected row
     */
    public String getColumnIdSelected()
    {
        MultiLineCellSelectionModel<CastingModelData> selection = (MultiLineCellSelectionModel<CastingModelData>) this.castingGrid
                .getSelectionModel();
        List<Cell> cellList = selection.getSeletedCells();
        if (cellList == null || cellList.isEmpty())
        {
            return null;
        }
        Cell cell = cellList.get(0);
        if (cell.cell == -1)
        {
            return null;
        }

        ColumnConfig columnConfig = castingGrid.getColumnModel().getColumn(cell.cell);
        return columnConfig.getId();
    }

    public void deselectedRows()
    {
        MultiLineCellSelectionModel<CastingModelData> selection = (MultiLineCellSelectionModel<CastingModelData>) this.castingGrid
                .getSelectionModel();
        selection.deselectAll();
    }

    public boolean isMultiSelectionGrid()
    {
        GridSelectionModel<CastingModelData> selection = this.castingGrid.getSelectionModel();
        return selection.getSelectedItems().size() > 1;
    }

    public void infoDisplay(String title, String message)
    {
        Info.display(title, message);
    }

    public void infoProgress(String title, String message)
    {
        progress = MessageBox.progress(title, message, "");
    }

    public void changeProgress(Double value)
    {
        if (value == null || progress == null)
        {
            return;
        }
        progress.updateProgress(value, "");
        if (value == 1)
        {
            progress.close();
            progress = null;
        }
    }

    public void openCsvExport(String path)
    {
        Window.open(path, "Export Casting", "");
    }
}
