package fr.hd3d.breakdown.ui.client.view.renderer;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;

import fr.hd3d.breakdown.ui.client.model.CastingModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;


/**
 * Renderer to display the list of constituent to a specific shot and specific category.
 * 
 * @author HD3D
 * 
 */
public class CastingCellRenderer implements GridCellRenderer<CastingModelData>
{

    public Object render(CastingModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<CastingModelData> store, Grid<CastingModelData> grid)
    {
        List<CompositionModelData> compositionList = model.getCompositions(property);
        if (compositionList == null)
        {
            return "";
        }
        StringBuilder text = new StringBuilder("");
        for (CompositionModelData composition : compositionList)
        {
            String constituentName = getConstituentName(composition);
            if (text.length() == 0)
            {
                text.append(constituentName);
            }
            else
            {
                text.append(", ").append(constituentName);
            }
        }

        Element div = DOM.createDiv();
        if (text != null)
        {
            div.setInnerHTML(text.toString());
        }
        div.addClassName(" cell-long-text");
        return div.getString();
    }

    /**
     * Return the constituent name concerning the composition.
     * 
     * @param composition
     * @return the constituent name
     */
    private String getConstituentName(CompositionModelData composition)
    {

        if (composition == null)
            return "";

        String constituentName;
        if (composition.getOcc() > 1)
        {
            constituentName = composition.getConstituentName() + " x " + composition.getOcc();
        }
        else
        {
            constituentName = composition.getConstituentName();
        }

        return constituentName;
    }

}
