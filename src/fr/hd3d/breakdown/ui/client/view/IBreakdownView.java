package fr.hd3d.breakdown.ui.client.view;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.mainview.IMainView;


public interface IBreakdownView extends IMainView
{

    /** Registers all controller. */
    public abstract void init();

    /** Initialize breakdown widgets and add them to viewport. Selects automatically last selected tab. */
    public abstract void initWidgets();

    /** Set tab controller as child of Production controller. */
    public abstract void setControllers();

    /** Reload last selected project, and select last selected tab. */
    public abstract void rebuildLastEnvironment();

    /** Refresh tag displayed inside tag editor. */
    public abstract void refreshTagEditor(List<Hd3dModelData> selection);

    /**
     * Select last selected project. The last selected project is the one of which id is stored in the favorite cookie.
     */
    public abstract void selectLastProject();

    /**
     * Make the constituent view inactive (to not react to event from similar view such as shot view).
     */
    public abstract void idleConstituentView();

    /**
     * Make the constituent view active.
     */
    public abstract void unidleConstituentView();

    /**
     * Make the shot view inactive (to not react to event from similar view such as constituent view).
     */
    public abstract void idleShotView();

    /**
     * Make the shot view active.
     */
    public abstract void unidleShotView();

    /**
     * Make the sequence view inactive (to not react to event from similar view such as sequence view).
     */
    public abstract void idleSequenceView();

    /**
     * Make the sequence view active.
     */
    public abstract void unidleSequenceView();

    /**
     * Save selected tab name to favorite cookie.
     * 
     * @param tabName
     *            The clicked tab name.
     */
    public abstract void saveTabToCookie(String tabName);

    /**
     * Show loading panel for update task script call.
     */
    public abstract void showTaskLongLoading();

    /**
     * Show loading panel for update asset script call.
     */
    public abstract void showAssetLongLoading();

    /**
     * Show loading panel for ODS import script call.
     */
    public abstract void showOdsImportLongLoading();

    /**
     * Hide loading panel.
     */
    public abstract void hideLoading();

    /**
     * Display ODS file uplaod dialog.
     */
    public abstract void displayOdsUploader();

    /**
     * Press auto save toggle button if it is not pressed.
     */
    public abstract void toggleAutoSave();

    public abstract void displayTaskTypeEditor();

}
