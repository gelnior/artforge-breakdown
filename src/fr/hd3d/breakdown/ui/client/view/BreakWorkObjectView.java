package fr.hd3d.breakdown.ui.client.view;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.breakdown.ui.client.view.widget.CreationDialog;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.dialog.RenameDialog;
import fr.hd3d.common.ui.client.widget.workobject.browser.view.WorkObjectView;


/**
 * BreakWorkObjectView contains utilities common to constituent and shot view.
 * 
 * @author HD3D
 */
public class BreakWorkObjectView extends WorkObjectView
{

    /** Rename dialog used for category and constituent renaming. */
    protected RenameDialog renameDialog;
    /** Creation dialog used for category and constituent creation. */
    protected CreationDialog creationDialog;

    /**
     * Display creation dialog for specified class (it will create instances for this class.
     * 
     * @param simpleClassName
     *            The class name to use;
     */
    public void displayCreationDialog(String simpleClassName, EventType event, String title)
    {
        if (creationDialog == null)
        {
            creationDialog = new CreationDialog(event, title);
        }
        creationDialog.setHeading(title);
        creationDialog.show(simpleClassName);
    }

    public void showRenameDialog(RecordModelData record, EventType event)
    {
        if (renameDialog == null)
        {
            renameDialog = new RenameDialog("");
            renameDialog.setEventType(event);
        }
        renameDialog.setHeading("Renaming " + record.getName());
        renameDialog.show(record);
    }

    public void display(String title, String text)
    {
        Info.display(title, text);
    }

    public void enableCreationDialog()
    {
        if (this.creationDialog != null)
        {
            this.creationDialog.enableForm();
            this.creationDialog.hideSaving();
            if (creationDialog.isToHideWhenSaveEnds())
                this.creationDialog.hide();
        }
    }
}
